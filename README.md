## Ссылка на верстку

[Главная](http://rekollegiya.simonboyko.tk/)

[Главная (2-ой вариант первого блока)](http://rekollegiya.simonboyko.tk/index-alt.html)

[Базовая внутренняя страница](http://rekollegiya.simonboyko.tk/base.html)

[Лауреаты](http://redkollegiya.simonboyko.tk/laureates.html)

[Претенденты за месяц](http://redkollegiya.simonboyko.tk/applicants-month.html)

[Претенденты за год](http://redkollegiya.simonboyko.tk/applicants-year.html)

[События](http://redkollegiya.simonboyko.tk/events.html)

[Событие](http://redkollegiya.simonboyko.tk/event.html)

[О нас](http://redkollegiya.simonboyko.tk/about.html)

[Интервью](http://redkollegiya.simonboyko.tk/about.html)

[Интервью](http://redkollegiya.simonboyko.tk/interview.html)

[Правила](http://redkollegiya.simonboyko.tk/rules.html)

[404](http://redkollegiya.simonboyko.tk/404.html)
