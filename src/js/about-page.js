(function (d) {
  const dev = true;

  if (!d.querySelector('.aboutPage')) return;

  if (dev) {
    console.log('INIT about-page');
  }

  const items = d.querySelectorAll('.aboutPage__body');
  try {
    items.forEach(i => Stickyfill.add(i));
    Stickyfill.forceSticky();
  } catch (e) {
    console.log(e);
  }



})(document);
