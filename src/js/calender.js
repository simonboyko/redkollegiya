(function (d) {
  // const dev = true;

  if (!d.querySelector('.calender')) return;

  if (dev) {
    console.log('INIT calender');
  }

  class CalenderFull {
    constructor(el) {
      this.refs = {
        el: el,
        month: el.querySelectorAll('.calender__month'),
        allForY: el.querySelector('.calender__all'),
        pagerCurrent: el.querySelector('.calender__currentY'),
        prevY: el.querySelector('.calender__arrow--prev'),
        nextY: el.querySelector('.calender__arrow--next'),
      };
      this.state = {
        curM: null,
        curY: null,
        curS: null,
        pager: null,
        minDate: new Date('2016-09'),
        baseUrl: location.pathname,
      };
      this.today = new Date();

      this.init();
    }

    init() {
      const q = new URLSearchParams(location.search);
      const now = this.today;
      this.state.curY = +q.get('y');
      this.state.curM = +q.get('m');
      this.state.curS = q.get('sort');
      if (!this.state.curY && !this.state.curM) {
        this.state.curY = now.getFullYear();
        this.state.curM = now.getMonth() + 1;
      }

      this.state.pager = this.state.curY;

      this.addListeners();
      this.updateMonth();
      this.updatePager();

      if (dev) {
        console.log(this.state);
      }
    }

    addListeners() {
      this.refs.prevY.addEventListener('click', () => this.actionPager(-1));
      this.refs.nextY.addEventListener('click', () => this.actionPager(1));
    }

    updateMonth() {
      // clean classes
      this.refs.month.forEach(m => {
        m.classList.remove('calender__month--dis');
        m.classList.remove('calender__month--active');
      });
      this.refs.allForY.classList.remove('calender__all--active');

      // set url and classes
      const y = this.state.pager;
      const m = this.state.curM;
      const s = this.state.curS;
      const path = this.state.baseUrl;
      const curY = this.state.curY;

      let q = new URLSearchParams();
      q.append('y', y);
      if (s) {
        q.append('y', y);
      }

      this.refs.allForY.href = `${path}?${q}`;
      this.refs.month.forEach((month, index) => {
        const curM = index + 1;
        q.set('m', curM);
        month.href = `${path}?${q}`;

        const now = this.today;
        if (y === curY && m === curM) {
          month.classList.add('calender__month--active');
        }
        const minDate = this.state.minDate;
        const maxY = now.getFullYear();
        const minY = minDate.getFullYear();
        if (
          (y === maxY && curM > Number(now.getMonth() + 1) || y > maxY)
          || (y === minY && curM < Number(minDate.getMonth() + 1) || y < minY)
        ) {
          month.classList.add('calender__month--dis');
        }
      });

      if (!m) {
        this.refs.allForY.classList.add('calender__all--active');
      }

    }

    updatePager() {
      const pagerY = this.state.pager;
      this.refs.pagerCurrent.innerHTML = pagerY;

      const now = this.today;
      if (pagerY >= now.getFullYear()) {
        this.refs.nextY.classList.add('calender__arrow--dis');
      } else {
        this.refs.nextY.classList.remove('calender__arrow--dis');
      }
      if (pagerY <= this.state.minDate.getFullYear()) {
        this.refs.prevY.classList.add('calender__arrow--dis');
      } else {
        this.refs.prevY.classList.remove('calender__arrow--dis');
      }
    }


    // actions
    actionPager(shift) {
      if (dev) {
        console.log('f: actionPager');
      }
      this.state.pager += shift;

      this.updatePager();
      this.updateMonth();
    }
  }

  class CalenderYears {
    constructor(el) {
      this.refs = {
        el: el,
        list: el.querySelector('.calender__list')
      };
      this.state = {
        curY: null,
        minYear: 2016,
        baseUrl: location.pathname,
      };
      this.today = new Date();

      this.init();
    }

    init() {
      const q = new URLSearchParams(location.search);
      const now = this.today;
      this.state.curY = +q.get('y') || now.getFullYear();

      this.createYears();

      if (dev) {
        console.log(this.state);
      }
    }

    createYears() {
      if (dev) {
        console.log('f: createYears');
      }
      let html = '';
      const now = this.today;
      const minY = this.state.minYear;
      const maxY = now.getFullYear();
      const curY = this.state.curY;

      let i = minY;
      while (i <= maxY) {
        const url = `${this.state.baseUrl}?y=${i}`;
        let classes = 'calender__month';
        if (i === curY) {
          classes += ' calender__month--active';
        }
        html = html + `<a href="${url}" class="${classes}">${i}</a>`;
        i++;
      }
      this.refs.list.innerHTML = html;
    }
  }

  try {
    new CalenderFull(d.querySelector('.calender:not(.calender--years)'));
  } catch (e) {
    if (dev) {
      console.log(e);
    }
  }

  try {
    new CalenderYears(d.querySelector('.calender--years'));
  } catch (e) {
    if (dev) {
      console.log(e);
    }
  }



})(document);
