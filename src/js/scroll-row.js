(function (d) {
  // const dev = true;

  if (!d.querySelector('.scrollRow')) return;

  if (dev) {
    console.log('INIT scroll-row');
  }


  class ScrollRow {
    constructor(el) {
      this.el = el;
      this.inner = el.querySelector('.scrollRow__inner');
      this.innerFistChild = this.inner.firstElementChild;
      this.cursor = d.querySelector('.dragCursor');
      this.arrowPrev = el.querySelector('.scrollRow__arrowPrev');
      this.arrowNext = el.querySelector('.scrollRow__arrowNext');
      this.state = {
        width: 0,
        offset: 0,
        minOffset: 0,
        nodrag: false,
        dragged: false,
        hideLeft: false,
        isActive: true,
        arrows: false,
      };
      this.step = 100;

      this.changeState = this.changeState.bind(this);

      this.setInitValues();
      if (!this.cursor) {
        this.createCursor();
      }
    }

    setInitValues() {
      d.addEventListener('DOMContentLoaded', () => {
        const innerWidth = this.inner.offsetWidth;
        const cntWidth = this.el.offsetWidth;
        let minOffset = cntWidth - innerWidth;

        if (minOffset >= 0) {
          minOffset = 0;
        } else {
          minOffset -= 100;
        }

        this.state.width = innerWidth;
        this.state.minOffset = minOffset;
        this.state.isActive = !(minOffset === 0);

        if (this.arrowNext && this.arrowNext) {
          this.state.arrows = true;
        }

        if (this.el.classList.contains('scrollRow--nodrag')) {
          this.state.nodrag = true;
        }

        this.addListener();

        if (dev) {
          console.log(this.state);
        }
      });
    }

    // BEGIN listeners and handlers
    addListener() {
      if (!this.state.isActive) return;

      this.el.addEventListener('wheel', (e) => e.preventDefault());
      this.el.addEventListener('wheel', throttle(this.scrollHandler.bind(this), 20).bind(this));

      if (!this.state.nodrag) {

        if (isTouchDevice()) {
          this.el.addEventListener('touchstart', this.moveHandler.bind(this));
        } else {
          this.el.addEventListener('mouseover', this.mouseOverHandler.bind(this));
          this.el.addEventListener('mousedown', this.moveHandler.bind(this));
        }

      }

      if (this.state.arrows) {
        if (dev) {
          console.log('add arrow listeners');
        }
        this.arrowPrev.addEventListener('click', this.arrowClickHandler.bind(this));
        this.arrowNext.addEventListener('click', this.arrowClickHandler.bind(this));
      }
    }

    scrollHandler(e) {
      if (dev) {
        console.log('f: scrollHandler');
        console.log(e);
      }

      const delta = e.deltaY;

      if (delta > 0) {
        this.changeState('forward', Math.abs(delta));
      } else if (delta < 0) {
        this.changeState('backward', Math.abs(delta));
      }

      e.preventDefault();
    }

    mouseOverHandler(e) {
      if (dev) {
        console.log('f: mouseOverHandler');
        console.log(e.target);
      }

      if (e.target != this.innerFistChild && e.target != this.inner && !this.state.dragged) {
        this.cursor.style.display = 'none';
        d.body.style.cursor = '';

        return false;
      }

      this.cursor.style.display = 'block';
      d.body.style.cursor = 'none';

      const moveHandler = (e) => {
        this.cursor.style.top = `${e.clientY}px`;
        this.cursor.style.left = `${e.clientX}px`;
      };

      d.addEventListener('mousemove', moveHandler);

      d.addEventListener('mouseout', (e) => {
        d.removeEventListener('mousemove', moveHandler);
        this.cursor.style.display = 'none';
        d.body.style.cursor = '';
      })

    }

    moveHandler(e) {
      if (dev) {
        console.log('f: moveHandler');
      }


      const self = this;
      let initPos;

      if (e.target != self.innerFistChild && e.target != self.inner && e.target !== d.querySelector('.laureates__items')) {
        if (dev) {
          console.log('in cond');
          console.log(e.target);
        }
        return;
      }

      self.changeState('dragged');

      self.inner.style.transitionDuration = '1s';
      self.inner.style.transitionTimingFunction = 'cubic-bezier(0, 0.65, 0.58, 1)';

      moveAt(e);

      function moveAt(e) {
        const pos = e.pageX ? e.pageX : e.changedTouches[0].pageX;
        let offset;
        const coeff = 2;

        if (initPos && initPos < pos) {
          offset = pos - initPos;
          offset = offset * coeff;
          self.changeState('backward', offset);
        }
        if (initPos && initPos > pos) {
          offset = initPos - pos;
          offset = offset * coeff;
          self.changeState('forward', offset);
        }
        initPos = e.pageX ? e.pageX : e.changedTouches[0].pageX;
      }

      if (isTouchDevice()) {
        document.ontouchmove = throttle(function (e) {
          moveAt(e);
        }, 16);
        self.el.ontouchend = function() {
          if (dev) {
            console.log('f: ontouchend');
          }
          // self.el.classList.remove('-dragged');
          self.inner.style.transitionDuration = '';
          self.inner.style.transitionTimingFunction = '';
          document.ontouchmove = null;
          self.el.ontouchend = null;
          self.changeState('dragged');
        };


      } else {
        document.onmousemove = throttle(function (e) {
          moveAt(e);
        }, 16);
        self.el.onmouseup = function() {
          if (dev) {
            console.log('f: onmouseup');
          }
          // self.el.classList.remove('-dragged');
          self.inner.style.transitionDuration = '';
          self.inner.style.transitionTimingFunction = '';
          document.onmousemove = null;
          self.el.onmouseup = null;
          self.changeState('dragged');
        };
      }

    }

    arrowClickHandler(e) {
      if (dev) {
        console.log('f:arrowClickHandler');
      }
      if (e.target.classList.contains('scrollRow__arrowPrev')) {
        if (dev) {
          console.log('click prev');
        }
        this.changeState('backward', 400);
      }
      if (e.target.classList.contains('scrollRow__arrowNext')) {
        if (dev) {
          console.log('click next');
        }
        this.changeState('forward', 400);
      }
    }
    // END listeners and handlers


    createCursor() {
      if (dev) {
        console.log('f: createCursor');
      }
      this.cursor = d.createElement('div');
      this.cursor.innerHTML = `
        <svg width="178" height="178" viewBox="0 0 178 178" fill="none" xmlns="http://www.w3.org/2000/svg">
          <circle class="dragCursor__circle" cx="89" cy="89" r="88" stroke="#000" stroke-width="2"/>
          <path class="dragCursor__leftArrow" d="M53.5275 89L64 79.9306L64 98.0694L53.5275 89Z" stroke="#000" stroke-width="2"/>
          <path class="dragCursor__rightArrow" d="M124.472 89L114 98.0694L114 79.9306L124.472 89Z" stroke="#000" stroke-width="2"/>
          
        </svg>
      `;
      this.cursor.className = 'dragCursor';

      d.body.appendChild(this.cursor);
    }

    toggleCursorState() {
      if (dev) {
        console.log('f: toggleCursorState');
      }

      const cursor = this.cursor;
      const circle = cursor.querySelector('.dragCursor__circle');
      const arrowLeft = cursor.querySelector('.dragCursor__leftArrow');
      const arrowRight = cursor.querySelector('.dragCursor__rightArrow');


      const animIn = () => {
        circle.setAttribute('r', 38);
        arrowLeft.setAttribute('d', 'M0 90 L20 73 L20 107 L0 90Z');
        arrowRight.setAttribute('d', 'M178 90 L158 107 L158 73 L178 90Z');
      };
      const animOut = () => {
        circle.setAttribute('r', 88);
        arrowLeft.setAttribute('d', 'M53.5275 89L64 79.9306L64 98.0694L53.5275 89Z');
        arrowRight.setAttribute('d', 'M124.472 89L114 98.0694L114 79.9306L124.472 89Z');
      };
      if (this.state.dragged) animIn(); else animOut();
    }

    move() {
      if (dev) {
        console.log('f: move');
      }

      this.inner.style.transform = `translateX(${this.state.offset}px)`;
    }

    end(direction) {
      if (dev) {
        console.log('f: end');
      }

      if (this.state.arrows) {
        if (direction === 'left') {
          this.arrowPrev.classList.add('disabled');
        }

        if (direction === 'right') {
          this.arrowNext.classList.add('disabled');
        }
      }
    }
    notEnd() {
      if (dev) {
        console.log('f: notEnd');
      }

      if (this.state.arrows) {
        this.arrowPrev.classList.remove('disabled');
        this.arrowNext.classList.remove('disabled');
      }
    }

    toggleScrollHideClass() {
      const cls = '-hideLeft';
      const offset = this.state.offset;
      const hideLeft = this.state.hideLeft;

      if (offset !== 0 && !hideLeft || offset === 0 && hideLeft) {
        this.el.classList.toggle(cls);
        this.state.hideLeft = !this.state.hideLeft;
      }
    }


    // actions ---------------------
    changeState(action, payload) {
      switch(action) {
        case 'forward':
          this.notEnd();
          this.actionForward(payload);
          this.move();
          this.toggleScrollHideClass();
          break;
        case 'backward':
          this.notEnd();
          this.actionBackward(payload);
          this.move();
          this.toggleScrollHideClass();
          break;
        case 'dragged':
          this.actionDragged();
          this.el.classList.toggle('-dragged');
          this.toggleCursorState();
          // this.cursor.classList.toggle('dragCursor--active');
          break;
      }
    }

    actionForward(offset) {
      if (dev) {
        console.log('f: scrollForward');
      }


      let newOffset;

      if (offset) {
        newOffset = this.state.offset - offset;
      } else {
        newOffset = this.state.offset - this.step;
      }


      if (newOffset <= this.state.minOffset) {
        if (dev) {
          console.log('newOffset <= this.state.minOffset');
          console.log(this.state.minOffset);
        }
        newOffset = this.state.minOffset;
        this.end('right');
      }

      this.state.offset = newOffset;

    }

    actionBackward(offset) {
      if (dev) {
        console.log('f: scrollBackward');
      }

      let newOffset;

      if (offset) {
        newOffset = this.state.offset + offset;
      } else {
        newOffset = this.state.offset + this.step;
      }

      if (newOffset >= 0) {
        newOffset = 0;
        this.end('left');
      }

      this.state.offset = newOffset;

    }

    actionDragged() {
      if (dev) {
        console.log('f: actionDragged');
      }

      this.state.dragged = !this.state.dragged;
    }
  }


  const items = d.querySelectorAll('.scrollRow');
  try {
    items.forEach(item => {
      if (window.outerWidth >= 768) {
        new ScrollRow(item);
      }
    });
  } catch (e) {
    console.log(e);
  }

})(document);
