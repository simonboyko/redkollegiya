(function (d) {
  // const dev = true;

  if (!d.querySelector('.rules__slider')) return;

  if (dev) {
    console.log('INIT rules-slider');
  }

  const slider = tns({
    container: d.querySelector('.rules__slider'),
    mode: 'gallery',
    controls: false,
    navContainer: d.querySelector('.rulesPreviews__items'),
    disable: true,
    responsive: {
      768: {
        disable: false
      }
    }
  });

  class RulesScroll {
    constructor() {
      this.refs = {
        el: d.querySelector('.rulesPreviews')
      };
      this.state = {};

      if (this.refs.el) {
        this.init();
      }
    }

    init() {
      if (dev) {
        console.log('init: RulesScroll');
      }
    }
  }

  try {
    new RulesScroll;

  } catch (e) {
    console.log(e);
  }
})(document);
