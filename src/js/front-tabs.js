(function (d) {
  // const dev = true;
  if (!d.querySelector('.frontTabsLayout')) return;

  if (dev) {
    console.log('INIT front-tabs');
  }

  class FrontTabs {
    constructor() {
      this.state = {
        prevActiveTabId: 0,
        activeTabId: 0,
      };

      this.refs = {
        contentContainer: d.querySelector('.frontTabsContent'),
        contentItems: d.querySelectorAll('.frontTabsContent__item'),
        navContainer: d.querySelector('.frontTabsNav'),
        navItems: d.querySelectorAll('.frontTabsNav__button'),
        activeContent: null,
        activeTab: null,
      };

      this.button = null;

      this.init();
      this.clickHandler = this.clickHandler.bind(this);
    }

    init() {
      if (dev) {
        console.log('f: init');
      }

      const savedTab = localStorage.getItem('frontTab');
      !savedTab ? localStorage.setItem('frontTab', 2) : null;
      const maxTabsID = this.refs.contentItems.length - 1;
      if (savedTab) {
        if (savedTab <= maxTabsID) {
          this.state.prevActiveTabId = +savedTab;
          this.state.activeTabId= +savedTab;
        } else {
          localStorage.removeItem('frontTab');
        }
      }

      this.addListeners();
      this.updateContent();
      this.fireAnim();
      this.delHiddenAttr();
      this.initLine();
    }

    delHiddenAttr() {
      this.refs.contentItems.forEach(i => {
        i.removeAttribute('hidden');
      });
      this.updateHeight();

    }

    updateHeight() {
      if (dev) {
        console.log('f: updateHeight');
      }

      if (d.documentElement.clientWidth >= 768) {
        setTimeout(() => {
          const active = this.refs.activeContent;
          const height = active.offsetHeight;
          if (height === 0) {
            this.refs.contentContainer.style.height = null;
          } else {
            this.refs.contentContainer.style.height = height + 'px';
          }

          if (dev) {
            console.log(active);
            console.log(height);
          }
        }, 100);
      }

    }

    addListeners() {
      if (dev) {
        console.log('f: addListeners');
      }

      this.refs.navItems.forEach(item => {
        item.addEventListener('click', this.clickHandler.bind(this))
      });
    }

    clickHandler(e) {
      if (dev) {
        console.log('f: clickHandler');
      }

      const newId = e.target.getAttribute('data-tab-id');

      if (!newId) return;

      this.setActiveTabId(newId);

      if (dev) {
        console.log(this.state);
      }
    }

    updateContent() {
      if (dev) {
        console.log('f: updateContent');
      }

      const activeContent = this.refs.activeContent;
      const activeTab = this.refs.activeTab;
      const id = this.state.activeTabId;

      if (activeTab) {
        activeTab.classList.remove('-active')
      }

      if (activeContent) {
        activeContent.classList.remove('-active');
      }


      this.refs.activeTab = this.refs.navItems[id];
      this.refs.activeContent = this.refs.contentItems[id];

      this.updateHeight();
    }

    fireAnim() {
      if (dev) {
        console.log('f: fireAnim');
      }
      const prevId = this.state.prevActiveTabId;
      const nextId = this.state.activeTabId;

      const duration = 400;
      const nextCnt = this.refs.activeContent;
      const nextTab = this.refs.activeTab;

      const prevCnt = this.refs.contentItems[prevId];
      const prevTab = this.refs.navItems[prevId];


      nextTab.classList.add('-active');
      nextCnt.classList.add('-active');

      if (prevId === nextId) return;

      prevTab.classList.remove('-active');


      setTimeout(() => {
        prevCnt.classList.remove('-active');
      }, duration);

      if (prevId < nextId) {
        prevCnt.classList.add('animOutToLeft');
        nextCnt.classList.add('animInToLeft');

        setTimeout(() => {
          prevCnt.classList.remove('animOutToLeft');
          nextCnt.classList.remove('animInToLeft');
        }, duration);
      }

      if (prevId > nextId) {
        prevCnt.classList.add('animOutToRight');
        nextCnt.classList.add('animInToRight');

        setTimeout(() => {
          prevCnt.classList.remove('animOutToRight');
          nextCnt.classList.remove('animInToRight');
        }, duration);
      }

      this.updateHeight();

    }

    setActiveTabId(id) {
      if (dev) {
        console.log('f: setActiveTabId');
      }

      this.state.prevActiveTabId = this.state.activeTabId;
      this.state.activeTabId = id;

      this.refs.activeTab = this.refs.navItems[id];
      this.refs.activeContent = this.refs.contentItems[id];


      // write to localStorage
      localStorage.setItem('frontTab', id);


      // this.updateContent();
      this.fireAnim();
      this.moveLine();

      if (dev) {
        console.log(this.state);
      }
    }

    initLine() {
      this.button = d.createElement('div');
      this.button.classList.add('frontTabsNav__line');

      this.refs.navContainer.appendChild(this.button);

      this.moveLine();

      setTimeout(() => {
        this.moveLine();
      }, 50);

    }

    moveLine() {
      if (dev) {
        console.log('f: moveLine');
      }

      const tab = this.refs.activeTab;
      const parent = tab.closest('.frontTabsNav');
      const width = tab.offsetWidth * 0.8;
      const offset = getCoords(tab).left - getCoords(parent).left;

      this.button.style.cssText = `\
        width: 20px; \
        left: ${offset}px; \
        transition-duration: 0.3s;
        transition-timing-function: linear;`;

      setTimeout(() => {
        this.button.style.cssText = `\
        width: ${width}px; \
        left: ${offset}px; \
        transition-duration: 4s;`;
      }, 300);

    }
  }

  try {
    new FrontTabs();
  } catch (e) {
    console.log(e);
  }

})(document);
