(function (d) {

  // const dev = true;

  if (!d.querySelector('.interviewAside__slides')) return;

  if (dev) {
    console.log('INIT interview-sliders');
  }


  const interview = tns({
    container: d.querySelector('.interviewAside__slides'),
    mode: 'carousel',
    loop: false,
    gutter: -95,
    controls: false,
    nav: false,
    navContainer: d.querySelector('.interviewAside__dots'),
    responsive: {
      768: {
        nav: true,
        mode: 'gallery',
        gutter: 0
      }
    }
  });

  try {
    const contents = d.querySelectorAll('.interviewAside__cntItem');
    const changeContent = (e) => {
      const index = e.index;
      const active = d.querySelector('.interviewAside__cntItem.-active');

      active.classList.remove('-active');
      contents[index].classList.add('-active');

    };
    interview.events.on('indexChanged', changeContent)
  } catch (e) {
    console.log(e);
  }



})(document);
