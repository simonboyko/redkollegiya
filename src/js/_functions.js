(function (d, window) {
  if (dev) {
    console.log('INIT functions');
  }

  const closest = () => {
    if (!Element.prototype.closest) {

      Element.prototype.closest = function (css) {
        let node = this;

        while (node) {
          if (node.matches(css)) {
            return node;
          } else {
            node = node.parentElement;
          }
        }

        return null;
      }
    }
  };
  closest();

  window.getCoords = (el) => {
    let box = el.getBoundingClientRect();

    return {
      top: box.top + pageYOffset,
      left: box.left + pageXOffset
    };
  };

  window.debounce = (f, ms) => {

    let timer = null;

    return function (...args) {
      const onComplete = () => {
        f.apply(this, args);
        timer = null;
      };

      if (timer) {
        clearTimeout(timer);
      }

      timer = setTimeout(onComplete, ms);
    };
  };

  window.throttle = (f, ms) => {
    let lastCall = 0;
    return function (...args) {
      const now = (new Date).getTime();
      if (now - lastCall < ms) {
        return;
      }
      lastCall = now;
      return f(...args);
    }
  };

  window.isTouchDevice = () => {
    const prefixes = ' -webkit- -moz- -o- -ms- '.split(' ');
    const mq = (query) => {
      return window.matchMedia(query).matches;
    };

    if (('ontouchstart' in window) || window.DocumentTouch && document instanceof DocumentTouch) {
      return true;
    }

    const query = ['(', prefixes.join('touch-enabled),('), 'heartz', ')'].join('');
    return mq(query);
  }
})(document, window);
