(function (d) {



  if (dev) {
    console.log('INIT laureates');
  }


  // begin slider

  const slidersItems = d.querySelectorAll('.laureates__slider');
  const sliders = [];
  slidersItems.forEach(slide => {
    const dots = slide.closest('.laureates__item').querySelector('.laureates__dots');
    const arrows = slide.closest('.laureates__item').querySelector('.laureates__arrows');
    sliders.push(tns({
      container: slide,
      mode: 'gallery',
      loop: false,
      navContainer: dots,
      controlsContainer: arrows,
    }));
  });

  /*const changeName = (e) => {
    if (dev) {
      console.log('f: sliderEventHandler');
    }

    const slide = e.slideItems[e.index];
    const name = slide.getAttribute('data-name');
    const nameEl = slide.closest('.laureates__item').querySelector('.laureates__name');

    setTimeout(() => {
      nameEl.innerHTML = name;
    }, 150);
    nameEl.classList.add('laureates__name--anim');
    setTimeout(() => {
      nameEl.classList.remove('laureates__name--anim');

    }, 300);

  };
  for (let i in sliders) {
    sliders[i].events.on('indexChanged', changeName)
  }*/

  // end slider




  // begin hover
  const imgLinks = d.querySelectorAll('.laureates__image a');

  const hoverHandler = (e) => {
    if (dev) {
      console.log('f: hoverHandler');
    }

    const el = e.currentTarget;
    const parent = el.closest('.laureates__item');
    parent.classList.add('laureates__item--imgHovered');

    const mouseLeaveHandler = (e) => {
      if (dev) {
        console.log('f: hoverHandler');
      }

      parent.classList.remove('laureates__item--imgHovered');
      el.removeEventListener('mouseleave', mouseLeaveHandler);
    };

    el.addEventListener('mouseleave', mouseLeaveHandler);
  };

  imgLinks.forEach(i  => {
    i.addEventListener('mouseenter', hoverHandler);
  });

  // end hover


})(document);
