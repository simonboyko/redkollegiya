(function (d) {

  class SearchModel {
    constructor() {
      this.data = {};
      this.url = '/wp-json/api/search';
      this.active = 'applicants_texts';

    }

    initActive() {
      const tabsData = this.getTabsData();
      let active = this.active;
      if (!active || tabsData[active] == 0) {
        active = null;
        for (let key in tabsData) {
          if (tabsData[key] !== 0) {
            active = key;
            this.setActive(active);
            return;
          }
        }

        this.setActive(null);
      }
    }

    initCurrentPage() {
      for (let key in this.data) {
        this.data[key].currentPage = 1;
      }
    }

    getData(searchString, cbFinish, cbStart) {
      cbStart();

      this.searchString = searchString;

      let xhr = new XMLHttpRequest();
      const url = `${this.url}/main?q=${searchString}`;

      xhr.open('get', url);

      xhr.send();
      xhr.onload = () => {
        if (xhr.status != 200) {
          console.log(`Ошибка ${xhr.status}: ${xhr.statusText}`);
        } else {
          const responseData = JSON.parse(xhr.responseText);

          this.data = responseData;
          this.initCurrentPage();
          this.initActive();

          cbFinish();
        }
      };

      xhr.onerror = function () {
        console.log('Ошибка. Запрос не удался');
      };
    }

    updateData(start, finish) {

      const active = this.data[this.active];
      const currentPage = active.currentPage;
      const pages = active.paged;

      if (currentPage === pages) return;


      if (start) start();

      let xhr = new XMLHttpRequest();

      const searchString = this.searchString;
      const nextPage = currentPage + 1;
      const url = `${this.url}/${this.active}?q=${searchString}&paged=${nextPage}`;

      xhr.open('get', url);

      xhr.send();
      xhr.onload = () => {
        if (xhr.status != 200) {
          console.log(`Ошибка ${xhr.status}: ${xhr.statusText}`);
        } else {
          const responseData = JSON.parse(xhr.responseText);
          this.data[this.active].html = this.data[this.active].html + responseData.html;

          finish();
          active.currentPage = nextPage;
        }
      };

      xhr.onerror = function () {
        console.log('Ошибка. Запрос не удался');
      };
    }

    getCurrentItems(cb) {
      let result = this.empty;
      if (this.active) {
        result = this.data[this.active].html;
      }

      cb(result);
    }

    getTabsData(cb) {
      let tabsData = {};
      for (let item in this.data) {
        tabsData[item] = this.data[item].count_all;
      }

      if (cb) {
        cb(tabsData);
      }

      return tabsData;
    }

    getActive(cb) {
      const active = this.active;
      cb(active);
    }



    setActive(value, cb) {
      this.active = value;

      if (cb) {
        cb(value);
      }
    }

  }

  class SearchView {
    constructor(node) {
      this.node = node;
      this.listEl = node.querySelector('.searchResults__list');
      this.tabListenersAdded = false;
      this.scrollListenersAdded = false;
      this.empty = 'По вашему запросу ничего не найдено';
      this.emptyEl = null;


      this.changeTabOnClick = null;
      this.getDataOnScroll = null;

      this.scrollHandler = null;

      this.renderTabs = this.renderTabs.bind(this);
      this.setActive = this.setActive.bind(this);
      this.renderList = this.renderList.bind(this);
      this.preload = this.preload.bind(this);
      this.preloadPart = this.preloadPart.bind(this);

    }

    addTabListeners() {
      const tabs = this.node.querySelectorAll('.searchTabs__tab');
      for (let tab of tabs) {
        tab.addEventListener('click', this.changeTabOnClick);
      }

      this.tabListenersAdded = true;
    }

    addScrollListeners() {
      this.scrollHandler = throttle(this.getDataOnScroll, 500);
      window.addEventListener('scroll', this.scrollHandler);

      this.scrollListenersAdded = true;
    }

    removeScrollListeners() {
      window.removeEventListener('scroll', this.scrollHandler);
      this.scrollListenersAdded = false;
    }

    renderTabs(data) {
      let tab;
      let tabNum;
      for (let i in data) {
        const value = data[i];
        tab = this.node.querySelector(`.searchTabs__tab--${i}`);
        tabNum = tab.querySelector('.searchTabs__num');
        tabNum.innerHTML = value;
        if (value == 0) {
          tab.disabled = true;
        } else {
          tab.disabled = false;
        }
      }

      if (!this.tabListenersAdded) {
        this.addTabListeners();
      }
    }

    renderList(data) {
      this.hideEmpty();

      const listEl = this.listEl;
      listEl.classList.remove('searchResults__list--preloader');

      if (data) {
        listEl.innerHTML = data;
        if (!this.scrollListenersAdded) {
          this.addScrollListeners();
        }
      } else {
        this.renderEmpty();
      }

    }

    setActive(active) {
      const currentActive = this.node.querySelector('.searchTabs__tab--active');
      if (currentActive) {
        currentActive.classList.remove('searchTabs__tab--active');
      }

      if (active) {
        const activeTab = this.node.querySelector(`.searchTabs__tab--${active}`);
        activeTab.classList.add('searchTabs__tab--active');
      }
    }

    preload() {
      this.hideEmpty();

      const tabNums = this.node.querySelectorAll('.searchTabs__num');
      tabNums.forEach(i => {
        i.innerHTML = 0;
      });

      const activeTab = this.node.querySelector('.searchTabs__tab--active');
      if (activeTab) {
        activeTab.classList.remove('searchTabs__tab--active');
      }

      const listEl = this.listEl;
      listEl.innerHTML = '';
      listEl.classList.add('searchResults__list--preloader');

      this.removeScrollListeners();
    }

    preloadPart() {
      this.hideEmpty();

      const listEl = this.listEl;
      listEl.classList.add('searchResults__list--preloader');

      this.removeScrollListeners();
    }

    renderEmpty() {
      let el = this.emptyEl;
      if (!el) {
        const text = this.empty;
        el = d.createElement('div');
        el.className = 'searchResults__empty';
        el.innerHTML = text;
        this.emptyEl = el;
      }

      // const parent = this.node.querySelector('.searchResults__inner');
      const tabs = this.node.querySelector('.searchResults__tabs');
      tabs.after(el);

      this.removeScrollListeners();
    }

    hideEmpty() {
      if (this.emptyEl) {
        this.emptyEl.remove();
      }
    }

    getListPosition() {
      const listEl = this.listEl;
      let coords = listEl.getBoundingClientRect();
      const fromBottom = (coords.y + coords.height) - d.documentElement.clientHeight;
      return fromBottom;
    }

  }

  class SearchController {
    constructor(view, model) {
      this.view = view;
      this.model = model;

      this.showData = this.showData.bind(this);
      this.showList = this.showList.bind(this);

      this.view.changeTabOnClick = this.changeTabOnClick.bind(this);
      this.view.getDataOnScroll = this.getDataOnScroll.bind(this);
    }

    do(searchString) {
      this.model.getData(searchString, this.showData, this.view.preload);
    }

    showData() {
      this.model.getTabsData(this.view.renderTabs);
      this.model.getActive(this.view.setActive);
      this.model.getCurrentItems(this.view.renderList);
    }

    showList() {
      this.model.getCurrentItems(this.view.renderList);
    }

    updateData() {
      this.model.updateData(this.view.preloadPart, this.showList);
    }

    searchString() {
      return 'test search string';
    }

    changeTabOnClick(e) {
      e.preventDefault();
      const target = e.currentTarget;

      if (target.classList.contains('searchTabs__tab--active')) {
        return false;
      } else {
        const active = target.dataset.tabName;

        this.model.setActive(active, this.view.setActive);
        this.model.getCurrentItems(this.view.renderList);
      }
    }

    getDataOnScroll(e) {
      const offset = this.view.getListPosition();
      if (offset <150) {
        this.updateData();
      }
    }

  }

  const rootEl = d.querySelector('.searchResults');
  if (rootEl) {
    try {
      window.SearchAjax = new SearchController(new SearchView(rootEl), new SearchModel());
    } catch (e) {
      console.log(e);
    }
  }

})(document);
