(function (d) {
  // const dev = true;

  class AjaxForm {
    constructor(form) {
      this.form = form;
      this.errEl = null;

      this.send = this.send.bind(this);

      this.init();
    }

    init() {
      this.form.addEventListener('submit', this.send);
    }

    send(e) {
      e.preventDefault();

      const url = this.form.getAttribute('action');
      const method = this.form.getAttribute('method');
      const FD = new FormData(this.form);
      const log = encodeURIComponent(FD.get('log'));
      const psw = encodeURIComponent(FD.get('psw'));
      const params = `log=${log}&psw=${psw}`;


      const xhr = new XMLHttpRequest();
      xhr.open(method, `${url}?${params}`);
      xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
      xhr.send();

      xhr.addEventListener('readystatechange', () => {
        if (xhr.readyState != 4) return;

        const res = JSON.parse(xhr.responseText);

        if (res.status) {
          location.reload();
        } else {
          this.showErr(res.text);
        }

      });
    }

    showErr(msg) {
      const createErrEl = () => {
        const errEl = d.createElement('div');
        errEl.className = 'form__error';
        this.errEl = errEl;
      };

      if (!this.errEl) {
        createErrEl();
      }

      this.errEl.innerHTML = msg;
      this.form.appendChild(this.errEl);
    }

  }

  const loginForm = d.querySelector('.form--login');
  try {
    new AjaxForm(loginForm);
  } catch (e) {
    console.log(e);
  }
})(document);
