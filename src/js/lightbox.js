(function (d) {
  // const dev = true;

  if (!window.fsLightbox) return;

  if (dev) {
    console.log('INIT lightbox');
  }


  fsLightbox.data.slideCounter = true;


  const links = d.querySelectorAll('[data-fslightbox]');
  links.forEach(i => {
    i.addEventListener('click', function () {
      if (dev) {
        console.log('e: click');
      }

      fsLightbox.show();
    });
  });


  try {
    const moreLinks = d.querySelectorAll('.gallery__counter');
    moreLinks.forEach(i => {
      i.addEventListener('click', (e) => {
        e.preventDefault();
        const node = e.target;
        const instance = node.getAttribute('data-fslightbox-gallery');
        fsLightboxInstances[instance].init();
        fsLightboxInstances[instance].show();
      });
    });
  } catch (e) {}


})(document);
