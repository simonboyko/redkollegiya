(function (d) {
  if (!d.querySelector('.foldSocials')) return;

  if (dev) {
    console.log('INIT fond-socials');
  }

  const clickHandler = (e) => {
    const el = e.currentTarget;
    const parent = el.parentElement;
    parent.classList.toggle('foldSocials--active');
  };

  const items = d.querySelectorAll('.foldSocials');
  try {
    items.forEach(i => {
      const btn = i.querySelector('.foldSocials__item--main');
      btn.addEventListener('click', clickHandler);
    });
  } catch (e) {
    console.log(e);
  }


})(document);
