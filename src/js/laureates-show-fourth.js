(function (d) {
  // const dev = true;

  if (!d.querySelector('.laureates--innerPage')) return;

  if (dev) {
    console.log('INIT laureates-show-fourth');
  }


  class ShowFourth {
    constructor(el) {
      this.el = el;
      this.inner = this.el.querySelector('.laureates__items');
      this.fourth = null;
      this.state = {
        shifted: false,
        isActive: false,
        offset: null
      };

      this.init();
    }

    init() {
      const elWidth = this.el.offsetWidth;
      const innerWidth = this.inner.offsetWidth;
      const diff = innerWidth - elWidth;

      this.fourth = this.el.querySelector('.laureates__item:nth-child(4)');

      if (diff > 0 && this.fourth) {
        if (dev) {
          console.log(elWidth+':'+innerWidth);
        }
        this.makeActive(diff);
      }
    }

    makeActive(offset) {
      if (dev) {
        console.log('f: makeActive');
      }

      this.state.isActive = true;
      this.state.offset = offset;

      this.fourth.classList.add('laureates__item--fourth');
      this.el.classList.add('-hideRight');

      this.addListeners();

      if (dev) {
        console.log(this.state);
      }
    }


    // BEGIN listeners and handlers
    addListeners() {
      this.fourth.addEventListener('mouseenter', this.mouseEnterHandler.bind(this));
      this.fourth.addEventListener('mouseleave', this.mouseLeaveHandler.bind(this));
    }

    mouseEnterHandler(e) {
      this.show();
    }

    mouseLeaveHandler(e) {
      this.hide();
    }

    // END listeners and handlers




    show() {
      if (dev) {
        console.log('f: show');
      }

      const offset = -this.state.offset + 'px';
      this.inner.style.transform = `translate3D(${offset},0,0)`;
      this.el.classList.remove('-hideRight');
      this.el.classList.add('-hideLeft');
    }

    hide() {
      if (dev) {
        console.log('f: hide');
      }

      this.inner.style.transform = `translate3D(0,0,0)`;
      this.el.classList.add('-hideRight');
      this.el.classList.remove('-hideLeft');
    }

  }

  const items = d.querySelectorAll('.laureates--innerPage');
  try {
    items.forEach(i => new ShowFourth(i));
  } catch (e) {
    console.log(e);
  }

})(document);
