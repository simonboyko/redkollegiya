(function (d) {

  const dev = true;

  class SearchModal {
    constructor() {
      this.refs = {
        btnOpen: d.querySelectorAll('.headerActions__item--search'),
        btnClose: d.querySelector('.searchPopup__closer'),
        popup: d.querySelector('.searchPopup'),
      };

      this.close = this.close.bind(this);
      this.open = this.open.bind(this);

      this.init();
    }

    init() {
      this.refs.btnClose.addEventListener('click', this.close);
      this.refs.btnOpen.forEach(i => {
        i.addEventListener('click', this.open);
      });
    }

    open(e) {
      this.refs.popup.classList.add('-opened');
      searchField.focus();
    }

    close(e) {
      e.preventDefault();
      this.refs.popup.classList.remove('-opened');
      searchField.clean(e);
    }
  }
  new SearchModal();


  class BaseField {
    constructor(el) {
      this.state = {
        placeholder: 'Поиск',
        value: null,
        url: null,
      };

      this.refs = {
        btnClean: null,
        field: null,
        placeholder: null,
        node: null,
      };

      this.onMouseup = this.onMouseup.bind(this);
      this.onBlur = this.onBlur.bind(this);
      this.onKeyup = this.onKeyup.bind(this);
      this.onKeydown = this.onKeydown.bind(this);
      this.onPaste = this.onPaste.bind(this);
      this.clean = this.clean.bind(this);
      this.send = this.send.bind(this);
      this.focus = this.focus.bind(this);


      if (!(el instanceof HTMLElement)) {
        el = d.querySelector(el)
      }
      this.setRefs(el);

      this.init();
    }

    init() {
      this.initPlaceholder();

      this.state.url = this.refs.node.getAttribute('data-search-url');

      const field = this.refs.field;
      // field.addEventListener('focus', this.onFocus);
      field.addEventListener('mouseup', this.onMouseup);
      field.addEventListener('blur', this.onBlur);
      field.addEventListener('keyup', this.onKeyup);
      field.addEventListener('keydown', this.onKeydown);
      field.addEventListener('paste', this.onPaste);

      this.refs.btnClean.addEventListener('click', this.clean);
    }

    setRefs(node) {
      this.refs = {
        btnClean: node.querySelector('.searchField__clean'),
        field: node.querySelector('.searchField__field'),
        placeholder: node.querySelector('.searchField__placeholder'),
        node: node,
      };
    }

    clean(e) {
      e.preventDefault();
      this.focus();

      this.state.value = null;

      const field = this.refs.field;
      field.innerHTML = null;
      this.refs.placeholder.style = null;
      field.classList.remove('-hasValue');
    }

    initPlaceholder() {
      const placeholder = this.refs.placeholder;
      this.state.placeholder = placeholder.innerHTML || this.state.placeholder;
      placeholder.innerHTML = this.state.placeholder;
    }

    focus() {
      setTimeout(() => {
        this.refs.field.focus();
      }, 500);
    }

    onKeyup(e) {
      const value = e.target.innerHTML;
      this.state.value = value;

      const field = this.refs.field;

      if (value) {
        field.classList.add('-hasValue');
      } else {
        field.classList.remove('-hasValue');
      }
    }

    onKeydown(e) {
      const keyCode = e.key;
      if (keyCode === 'Enter' || keyCode === 'NumpadEnter') {
        e.preventDefault();
        this.send();
        return false;
      }
    }

    onMouseup(e) {
      const value = this.state.value;
      const field = this.refs.field;
      const placeholder = this.refs.placeholder;

      if (!value) {
        field.innerHTML = '';
        placeholder.style.display = 'none';
      }
    }

    onPaste(e) {
      e.preventDefault();
      const data = e.clipboardData.getData("text/plain");
      e.target.innerHTML = data;
    }

    onBlur(e) {
      const node = e.target;
      const value = node.innerHTML;
      const field = this.refs.field;
      const placeholder = this.refs.placeholder;

      this.state.value = value;
      if (!value) {
        field.innerHTML = null;
        placeholder.style = '';
      }
    }

    send() {
      console.log('send');
      const value = this.state.value;
      if (!value) {
        console.log('in cond');
        return false;
      }

      return true;

      // метод определен в наследующих классах
    }

  }

  class SearchField extends BaseField {
    constructor(el) {
      super(el);
    }

    send() {
      if (!super.send()) return;

      const value = this.state.value;
      const url = this.state.url + `?q=${value}`;

      location.href = url;
    }
  }

  class PageSearchField extends BaseField {
    constructor(el) {
      super(el);
      this.state.firstVisit = true;

      this.setInitValue();
    }

    setInitValue() {
      const query = new URLSearchParams(location.search);
      const searchString = query.get('q');
      this.state.value = searchString;

      if (searchString) {
        const field = this.refs.field;
        field.innerHTML = searchString;
        field.classList.add('-hasValue');
        this.send();
      }

    }

    send() {
      if (!super.send()) return;

      if (!this.state.firstVisit) {
        this.changeUrl();
      } else {
        this.state.firstVisit = false;
      }
      SearchAjax.do(this.state.value);
    }

    changeUrl() {
      const string = this.state.value;
      const url = location.pathname + '?q=' + string;

      let stateObj = {
        search: 'search'
      };
      history.pushState(stateObj, 'hello', url);
    }
  }


  let searchField;
  const pageFieldEl = d.querySelector('.searchField--page');
  try {
    searchField = new SearchField('.searchField--popup');
    if (pageFieldEl) {
      new PageSearchField(pageFieldEl);
    }
  } catch (e) {
    console.log(e);
  }

})(document);


