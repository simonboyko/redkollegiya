(function (d) {
  // также для страницы События
  const dev = true;

  // if (!d.querySelector('.frontTabsContentApplicants__item')
  // || !d.querySelector('.applicants__card')) return;

  if (dev) {
    console.log('INIT applicants-preview');
  }

  class ApplicantsPreview {
    constructor() {
      this.html = '<div class="applicantsPreview">\n' +
        '  <div class="applicantsPreview__header">\n' +
        '    <div class="applicantsPreview__top"><time class="applicantsPreview__date"></time>\n' +
        '      <div class="applicantsPreview__source"></div>\n' +
        '    </div>\n' +
        '    <h3 class="applicantsPreview__title"></h3>\n' +
        '  </div>\n' +
        '  <div class="applicantsPreview__body"></div>\n' +
        '  <div class="applicantsPreview__footer"><a class="button applicantsPreview__button" target="_blank"><span>Читать материал</span></a></div><button class="applicantsPreview__close"><span class="visually-hidden">Закрыть</span></button>\n' +
        '</div>';
      this.refs = {
        items: d.querySelectorAll('.frontTabsContentApplicants__card a, .applicants__card'),
        parent: d.querySelector('.page'),
        close: null,
        popup: null,
        title: null,
        date: null,
        source: null,
        body: null,
        button: null,
      };
      this.state = {
        url: null,
        title: null,
        source: null,
        date: null,
        dateFormat: null,
        body: null,
        lastClicked: null,
        active: false
      };

      this.bodyClickHanlder = this.bodyClickHanlder.bind(this);
      this.clickHandler = this.clickHandler.bind(this);
      this.hide = this.hide.bind(this);

      this.init();
    }

    init() {
      if (dev) {
        console.log('init ApplicantsPreview');
      }
      this.createPopup();
      this.addListeners();
    }

    addListeners() {
      this.refs.items.forEach(i => {
        i.addEventListener('click', this.clickHandler);
      });
      this.refs.close.addEventListener('click', this.hide);
    }
    clickHandler(e) {
      if (dev) {
        console.log('f: clickHandler');
      }

      const el = e.currentTarget.closest('.frontTabsContentApplicants__card') || e.currentTarget;

      if (el === this.state.lastClicked) {
        return;
      }

      let data = {};
      if (el === e.currentTarget) {
        data = {
          url: el.getAttribute('href'),
          title: el.querySelector('.applicants__title').innerHTML,
          source: el.querySelector('.applicants__source').innerHTML,
          date: el.querySelector('.applicants__date').innerHTML,
          dateFormat: el.querySelector('.applicants__date').getAttribute('datetime'),
          body: el.getAttribute('data-body'),
          clickedEl: el
        };
      } else {
        data = {
          url: e.currentTarget.getAttribute('href'),
          title: el.querySelector('.frontTabsContentApplicants__title').innerHTML,
          source: el.querySelector('.frontTabsContentApplicants__source').innerHTML,
          date: el.querySelector('.frontTabsContentApplicants__date').innerHTML,
          dateFormat: el.querySelector('.frontTabsContentApplicants__date').getAttribute('datetime'),
          body: el.getAttribute('data-body'),
          clickedEl: el
        };
      }


      this.actionSetParams(data);
      e.preventDefault();
    }
    bodyClickHanlder(e) {
      if (dev) {
        console.log('f: bodyClickHanlder');
      }

      const el = e.target;
      if (dev) {
        console.log(el);
        console.log(el.closest('.frontTabsContentApplicants'));
        console.log(el.closest('.applicants__item'));
        console.log(!(el.closest('.frontTabsContentApplicants') || el.closest('.applicantsPreview'))
          || !el.closest('.applicants__item'));
      }

      if (
        !(el.closest('.frontTabsContentApplicants') || el.closest('.applicantsPreview'))
        && !el.closest('.applicants__item')
      ) {
        this.hide();
      }
    }

    createPopup() {
      if (dev) {
        console.log('f: createPopup');
      }
      let el = d.createElement('div');
      el.innerHTML = this.html;
      el = el.firstChild;

      this.refs.popup = el;
      this.refs.title = el.querySelector('.applicantsPreview__title');
      this.refs.date = el.querySelector('.applicantsPreview__date');
      this.refs.source = el.querySelector('.applicantsPreview__source');
      this.refs.body = el.querySelector('.applicantsPreview__body');
      this.refs.button = el.querySelector('.applicantsPreview__button');
      this.refs.close = el.querySelector('.applicantsPreview__close');

      this.refs.parent.appendChild(this.refs.popup);
    }

    updateView() {
      if (dev) {
        console.log('f: updateView');
        console.log(this.state);
      }

      if (this.state.active) {
        this.show();
        this.refs.title.innerHTML = `<a href="${this.state.url}" target="_blank">${this.state.title}</a>`;
        this.refs.date.innerHTML = this.state.date;
        this.refs.date.setAttribute('datetime', this.state.dateFormat);
        this.refs.source.innerHTML = this.state.source;
        this.refs.body.innerHTML = this.state.body;
        this.refs.button.setAttribute('href', this.state.url);
      } else {
        this.hide();
      }
    }

    show() {
      if (dev) {
        console.log('f: show');
      }
      this.refs.popup.classList.add('applicantsPreview--active');
      d.body.addEventListener('click', this.bodyClickHanlder);
    }

    hide() {
      if (dev) {
        console.log('f: hide');
      }
      this.refs.popup.classList.remove('applicantsPreview--active');
      this.state.lastClicked = null;
      d.body.removeEventListener('click', this.bodyClickHanlder);
    }

    // actions
    actionSetParams(params) {
      this.state.url = params.url;
      this.state.title = params.title;
      this.state.source = params.source;
      this.state.date = params.date;
      this.state.dateFormat = params.dateFormat;
      this.state.body = params.body;
      this.state.lastClicked = params.clickedEl;
      this.state.active = true;

      this.updateView();
    }

  }

  try {
    new ApplicantsPreview();
  } catch (e) {}

})(document);
