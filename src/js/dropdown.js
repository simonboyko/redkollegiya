(function (d) {
  // const dev = true;
  if (!d.querySelector('.dropdown')) return;

  if (dev) {
    console.log('INIT dropdown');
  }

  class Dropdown {
    constructor(el) {
      this.refs = {
        cnt: el,
        title: el.querySelector('.dropdown__title'),
        main: el.querySelector('.dropdown__items')
      };
      this.state = {
        active: false,
        offset: 0
      };

      this.init = this.init.bind(this);

      window.addEventListener('load', this.init);
    }

    init() {
      this.refs.title.addEventListener('click', this.toggleState.bind(this));
      this.state.offset = this.refs.main.offsetHeight;
      this.refs.main.style.position = 'relative';
      this.toggleOffset();
    }

    toggleState() {
      this.state.active = !this.state.active;
      this.toggleClass();
      this.toggleOffset();
    }

    toggleOffset() {
      if (this.state.active) {
        this.refs.main.style.cssText = `transition-duration: 0.3s; position: relative; margin-top: 0;`
      } else {
        this.refs.main.style.cssText = `transition-duration: 0.3s; position: relative; margin-top: -${this.state.offset}px;`
      }
    }

    toggleClass() {
      this.refs.cnt.classList.toggle('dropdown--active');
    }

    // reload() {
    //   if (dev) {
    //     console.log('f: reload');
    //   }
    // }

  }

  const items = d.querySelectorAll('.dropdown');
  try {
    items.forEach(i => new Dropdown(i));
  } catch (e) {
    console.log(e);
  }
})(document);
