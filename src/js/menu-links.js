(function () {
  if (dev) {
    console.log('INIT menu-links');
  }
  
  /**
   * Не должно быть перехода по ссылке, если в href есть #
   */
  
  document.getElementsByClassName('header__menu')[0]
    .addEventListener('click', function (e) {
      if (e.target.getAttribute('href') && e.target.getAttribute('href') === '#') e.preventDefault();
    });

})();