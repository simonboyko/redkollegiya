(function (d) {
  // const dev = true;
  if (dev) {
    console.log('INIT form');
  }

  class AnimLabel {
    constructor(input) {
      this.state = {
        filled: false,
        focused: false,
      };

      this.input = input;
      this.item = input.closest('.form__item');

      this.changeHandler = this.changeHandler.bind(this);
      this.focusHandler = this.focusHandler.bind(this);
      this.blurHandler = this.blurHandler.bind(this);

      this.addListeners();
    }

    addListeners() {
      this.input.addEventListener('change', this.changeHandler);
      this.input.addEventListener('focus', this.focusHandler);
      this.input.addEventListener('blur', this.blurHandler);
    }

    focusHandler() {
      if (dev) {
        console.log('f: focusHandler');
      }

      this.changeState('toggleFocused');
    }

    blurHandler() {
      if (dev) {
        console.log('f: blurHandler');
      }

      this.changeState('toggleFocused');
    }

    changeHandler(e) {
      if (dev) {
        console.log('f: changeHandler');
      }

      const state = this.state.filled;
      const value = e.target.value;

      if (!state && value || state && !value) {
        this.changeState('toggleFilled');
      }
    }

    toggleClass() {
      if (dev) {
        console.log('f: toggleClass');
      }

      this.item.classList.toggle('form__item--inputFilled');
    }

    toggleFocusClass() {
      if (dev) {
        console.log('f: toggleFocusClass');
      }

      this.item.classList.toggle('form__item--focused');
    }


    // state modifiers
    changeState(action) {
      switch (action) {
        case 'toggleFilled':
          this.actionToggleFilled();
          this.toggleClass();
          if (dev) {
            console.log(this.state);
          }
          break;
        case 'toggleFocused':
          this.toggleFocusClass();
          if (dev) {
            console.log(this.state);
          }
          break;
      }
    }

    actionToggleFilled() {
      if (dev) {
        console.log('f: actionToggleFilled');
      }

      this.state.filled = !this.state.filled;
    }

    actionToggleFocused() {
      if (dev) {
        console.log('f: actionToggleFocused');
      }

      this.state.filled = !this.state.filled;
    }
  }



  // const wpcf7Elm = document.querySelector( '.wpcf7' );
  // if (wpcf7Elm) {
  //   wpcf7Elm.addEventListener( 'wpcf7submit', function(e) {
  //     openModal('.modal--success');
  //   }, false );


    // для тестирования
    // setTimeout(() => {
    //   const ev = new Event('wpcf7submit');
    //   wpcf7Elm.dispatchEvent(ev);
    // }, 4000);
  // }


  // для тестирования
  // const form = d.querySelector('.form--propose');
  // const invalidEvent = () => {
  //   if (dev) {
  //     console.log('f: invalidEvent');
  //   }
  // };
  // form.addEventListener('invalid', invalidEvent);
  //
  // setTimeout(() => {
  //   const event = new Event('invalid');
  //   form.dispatchEvent(event);
  // }, 3000);

  const inputs = d.querySelectorAll('.form--animLabels .form__input');
  try {
    inputs.forEach(i => new AnimLabel(i));
  } catch (e) {
    console.log(e);
  }

})(document);
