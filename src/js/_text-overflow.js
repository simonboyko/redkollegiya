// Add data-text-overflow with count of lines for clipping a rest.

(function (d) {
  if (dev) {
    console.log('INIT text-overflow');
  }

  class TextOverflow {
    constructor(el) {
      this.el = el;
      this.state = {
        lines: null,
      };

      this.initValue();
      this.clipText();
    }

    initValue() {
      const lines = this.el.getAttribute('data-text-overflow');

      this.changeState('set', lines);
    }

    clipText() {
      const css = getComputedStyle(this.el);
      const lh = Number(css.lineHeight.slice(0, -2));
      const height = Math.floor(lh * this.state.lines);
      const width = css.width;


      let ghost = this.el.cloneNode(true);
      ghost.style.width = width;
      ghost.style.position = 'absolute';
      ghost.style.visibility = 'hidden';
      this.el.parentElement.appendChild(ghost);

      let text = ghost.innerHTML;
      while (text.length > 0 && ghost.offsetHeight > height) {
        text = text.substr(0, text.length -1);
        ghost.innerHTML = (text + '...');
      }

      const newText = ghost.innerHTML;
      ghost.remove();

      this.el.innerHTML = newText;

    }


    // actions -------------------
    changeState(action, payload) {
      switch (action) {
        case 'set':
          this.actionSet(payload);
          break;
      }
    }

    actionSet(value) {
      if (dev) {
        console.log('f: actionSet');
      }

      value = Number(value);

      if (
        Number.isNaN(value)
        || value <= 0
        || parseInt(value) !== value
      ) {
        console.error('Значение в data-text-overflow должно быть целым числом больше 0.');
      }

      this.state.lines = value;
    }
  }

  const els = d.querySelectorAll('[data-text-overflow]');
  try {
    els.forEach(el => new TextOverflow(el));
  } catch (e) {
  }

})(document);
