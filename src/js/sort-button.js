(function (d) {
  // const dev = true;

  if (!d.querySelector('.filter__sort')) return;

  if (dev) {
    console.log('INIT sort-button');
  }

  const sort = () => {
    const button = d.querySelector('.filter__sortCurrent');
    const list = d.querySelector('.filter__sortList');
    const links = d.querySelectorAll('.filter__sortButton');
    const openCls = '-active';
    let isOpened = false;


    const bodyClickHandler = (e) => {
      if (dev) {
        console.log('f: bodyClickHandler');
      }

      const el = e.target;
      if (el !== button && Array.from(links).indexOf(el) === -1) {
        close();
      }
    };


    const open = () => {
      if (dev) {
        console.log('f: open');
      }

      list.classList.add(openCls);
      button.classList.add(openCls);
      d.body.addEventListener('click', bodyClickHandler);
      isOpened = true;
    };
    const close = () => {
      if (dev) {
        console.log('f: close');
      }

      list.classList.remove(openCls);
      button.classList.remove(openCls);
      d.body.removeEventListener('click', bodyClickHandler);
      isOpened = false;
    };


    const buttonClickHandler = (e) => {
      if (isOpened) {
        close();
      } else {
        open();
      }

      e.preventDefault();
    };
    button.addEventListener('click', buttonClickHandler);


    links.forEach(link => {
      link.addEventListener('click', close);
    });
  };
  const date = () => {
    const btn = d.querySelector('.filter__dateCurrent');
    const cnt = d.querySelector('.filter__dateList');
    const openCls = '-active';
    let isOpened = false;

    const bodyClickHandler = (e) => {
      if (dev) {
        console.log('f: bodyClickHandler');
      }

      const parent = e.target.closest('.filter__date');
      if (!parent) {
        close();
      }
    };
    const open = () => {
      if (dev) {
        console.log('f: open');
      }

      cnt.classList.add(openCls);
      btn.classList.add(openCls);
      d.body.addEventListener('click', bodyClickHandler);
      isOpened = true;
    };
    const close = () => {
      if (dev) {
        console.log('f: close');
      }

      cnt.classList.remove(openCls);
      btn.classList.remove(openCls);
      d.body.removeEventListener('click', bodyClickHandler);
      isOpened = false;
    };

    const buttonClickHandler = (e) => {
      if (isOpened) {
        close();
      } else {
        open();
      }

      e.preventDefault();
    };
    btn.addEventListener('click', buttonClickHandler);
  };


  try {
    sort();
    date();
  } catch (e) {
    console.log(e);
  }


})(document);
