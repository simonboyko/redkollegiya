"use strict";

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

var tns = function () {
  Object.keys || (Object.keys = function (t) {
    var e = [];

    for (var n in t) {
      Object.prototype.hasOwnProperty.call(t, n) && e.push(n);
    }

    return e;
  }), "remove" in Element.prototype || (Element.prototype.remove = function () {
    this.parentNode && this.parentNode.removeChild(this);
  });

  var t = window,
      Oi = t.requestAnimationFrame || t.webkitRequestAnimationFrame || t.mozRequestAnimationFrame || t.msRequestAnimationFrame || function (t) {
    return setTimeout(t, 16);
  },
      e = window,
      Di = e.cancelAnimationFrame || e.mozCancelAnimationFrame || function (t) {
    clearTimeout(t);
  };

  function Hi() {
    for (var t, e, n, i = arguments[0] || {}, a = 1, r = arguments.length; a < r; a++) {
      if (null !== (t = arguments[a])) for (e in t) {
        i !== (n = t[e]) && void 0 !== n && (i[e] = n);
      }
    }

    return i;
  }

  function ki(t) {
    return 0 <= ["true", "false"].indexOf(t) ? JSON.parse(t) : t;
  }

  function Ri(t, e, n, i) {
    if (i) try {
      t.setItem(e, n);
    } catch (t) {}
    return n;
  }

  function Ii() {
    var t = document,
        e = t.body;
    return e || ((e = t.createElement("body")).fake = !0), e;
  }

  var n = document.documentElement;

  function Pi(t) {
    var e = "";
    return t.fake && (e = n.style.overflow, t.style.background = "", t.style.overflow = n.style.overflow = "hidden", n.appendChild(t)), e;
  }

  function zi(t, e) {
    t.fake && (t.remove(), n.style.overflow = e, n.offsetHeight);
  }

  function Wi(t, e, n, i) {
    "insertRule" in t ? t.insertRule(e + "{" + n + "}", i) : t.addRule(e, n, i);
  }

  function Fi(t) {
    return ("insertRule" in t ? t.cssRules : t.rules).length;
  }

  function qi(t, e, n) {
    for (var i = 0, a = t.length; i < a; i++) {
      e.call(n, t[i], i);
    }
  }

  var i = "classList" in document.createElement("_"),
      ji = i ? function (t, e) {
    return t.classList.contains(e);
  } : function (t, e) {
    return 0 <= t.className.indexOf(e);
  },
      Vi = i ? function (t, e) {
    ji(t, e) || t.classList.add(e);
  } : function (t, e) {
    ji(t, e) || (t.className += " " + e);
  },
      Gi = i ? function (t, e) {
    ji(t, e) && t.classList.remove(e);
  } : function (t, e) {
    ji(t, e) && (t.className = t.className.replace(e, ""));
  };

  function Qi(t, e) {
    return t.hasAttribute(e);
  }

  function Xi(t, e) {
    return t.getAttribute(e);
  }

  function r(t) {
    return void 0 !== t.item;
  }

  function Yi(t, e) {
    if (t = r(t) || t instanceof Array ? t : [t], "[object Object]" === Object.prototype.toString.call(e)) for (var n = t.length; n--;) {
      for (var i in e) {
        t[n].setAttribute(i, e[i]);
      }
    }
  }

  function Ki(t, e) {
    t = r(t) || t instanceof Array ? t : [t];

    for (var n = (e = e instanceof Array ? e : [e]).length, i = t.length; i--;) {
      for (var a = n; a--;) {
        t[i].removeAttribute(e[a]);
      }
    }
  }

  function Ji(t) {
    for (var e = [], n = 0, i = t.length; n < i; n++) {
      e.push(t[n]);
    }

    return e;
  }

  function Ui(t, e) {
    "none" !== t.style.display && (t.style.display = "none");
  }

  function _i(t, e) {
    "none" === t.style.display && (t.style.display = "");
  }

  function Zi(t) {
    return "none" !== window.getComputedStyle(t).display;
  }

  function $i(e) {
    if ("string" == typeof e) {
      var n = [e],
          i = e.charAt(0).toUpperCase() + e.substr(1);
      ["Webkit", "Moz", "ms", "O"].forEach(function (t) {
        "ms" === t && "transform" !== e || n.push(t + i);
      }), e = n;
    }

    for (var t = document.createElement("fakeelement"), a = (e.length, 0); a < e.length; a++) {
      var r = e[a];
      if (void 0 !== t.style[r]) return r;
    }

    return !1;
  }

  function ta(t, e) {
    var n = !1;
    return /^Webkit/.test(t) ? n = "webkit" + e + "End" : /^O/.test(t) ? n = "o" + e + "End" : t && (n = e.toLowerCase() + "end"), n;
  }

  var a = !1;

  try {
    var o = Object.defineProperty({}, "passive", {
      get: function get() {
        a = !0;
      }
    });
    window.addEventListener("test", null, o);
  } catch (t) {}

  var u = !!a && {
    passive: !0
  };

  function ea(t, e, n) {
    for (var i in e) {
      var a = 0 <= ["touchstart", "touchmove"].indexOf(i) && !n && u;
      t.addEventListener(i, e[i], a);
    }
  }

  function na(t, e) {
    for (var n in e) {
      var i = 0 <= ["touchstart", "touchmove"].indexOf(n) && u;
      t.removeEventListener(n, e[n], i);
    }
  }

  function ia() {
    return {
      topics: {},
      on: function on(t, e) {
        this.topics[t] = this.topics[t] || [], this.topics[t].push(e);
      },
      off: function off(t, e) {
        if (this.topics[t]) for (var n = 0; n < this.topics[t].length; n++) {
          if (this.topics[t][n] === e) {
            this.topics[t].splice(n, 1);
            break;
          }
        }
      },
      emit: function emit(e, n) {
        n.type = e, this.topics[e] && this.topics[e].forEach(function (t) {
          t(n, e);
        });
      }
    };
  }

  var aa = function aa(O) {
    O = Hi({
      container: ".slider",
      mode: "carousel",
      axis: "horizontal",
      items: 1,
      gutter: 0,
      edgePadding: 0,
      fixedWidth: !1,
      autoWidth: !1,
      viewportMax: !1,
      slideBy: 1,
      center: !1,
      controls: !0,
      controlsPosition: "top",
      controlsText: ["prev", "next"],
      controlsContainer: !1,
      prevButton: !1,
      nextButton: !1,
      nav: !0,
      navPosition: "top",
      navContainer: !1,
      navAsThumbnails: !1,
      arrowKeys: !1,
      speed: 300,
      autoplay: !1,
      autoplayPosition: "top",
      autoplayTimeout: 5e3,
      autoplayDirection: "forward",
      autoplayText: ["start", "stop"],
      autoplayHoverPause: !1,
      autoplayButton: !1,
      autoplayButtonOutput: !0,
      autoplayResetOnVisibility: !0,
      animateIn: "tns-fadeIn",
      animateOut: "tns-fadeOut",
      animateNormal: "tns-normal",
      animateDelay: !1,
      loop: !0,
      rewind: !1,
      autoHeight: !1,
      responsive: !1,
      lazyload: !1,
      lazyloadSelector: ".tns-lazy-img",
      touch: !0,
      mouseDrag: !1,
      swipeAngle: 15,
      nested: !1,
      preventActionWhenRunning: !1,
      preventScrollOnTouch: !1,
      freezable: !0,
      onInit: !1,
      useLocalStorage: !0
    }, O || {});
    var D = document,
        h = window,
        a = {
      ENTER: 13,
      SPACE: 32,
      LEFT: 37,
      RIGHT: 39
    },
        e = {},
        n = O.useLocalStorage;

    if (n) {
      var t = navigator.userAgent,
          i = new Date();

      try {
        (e = h.localStorage) ? (e.setItem(i, i), n = e.getItem(i) == i, e.removeItem(i)) : n = !1, n || (e = {});
      } catch (t) {
        n = !1;
      }

      n && (e.tnsApp && e.tnsApp !== t && ["tC", "tPL", "tMQ", "tTf", "t3D", "tTDu", "tTDe", "tADu", "tADe", "tTE", "tAE"].forEach(function (t) {
        e.removeItem(t);
      }), localStorage.tnsApp = t);
    }

    var r,
        o,
        u,
        l,
        s,
        c,
        f,
        y = e.tC ? ki(e.tC) : Ri(e, "tC", function () {
      var t = document,
          e = Ii(),
          n = Pi(e),
          i = t.createElement("div"),
          a = !1;
      e.appendChild(i);

      try {
        for (var r, o = "(10px * 10)", u = ["calc" + o, "-moz-calc" + o, "-webkit-calc" + o], l = 0; l < 3; l++) {
          if (r = u[l], i.style.width = r, 100 === i.offsetWidth) {
            a = r.replace(o, "");
            break;
          }
        }
      } catch (t) {}

      return e.fake ? zi(e, n) : i.remove(), a;
    }(), n),
        g = e.tPL ? ki(e.tPL) : Ri(e, "tPL", function () {
      var t,
          e = document,
          n = Ii(),
          i = Pi(n),
          a = e.createElement("div"),
          r = e.createElement("div"),
          o = "";
      a.className = "tns-t-subp2", r.className = "tns-t-ct";

      for (var u = 0; u < 70; u++) {
        o += "<div></div>";
      }

      return r.innerHTML = o, a.appendChild(r), n.appendChild(a), t = Math.abs(a.getBoundingClientRect().left - r.children[67].getBoundingClientRect().left) < 2, n.fake ? zi(n, i) : a.remove(), t;
    }(), n),
        H = e.tMQ ? ki(e.tMQ) : Ri(e, "tMQ", (o = document, u = Ii(), l = Pi(u), s = o.createElement("div"), c = o.createElement("style"), f = "@media all and (min-width:1px){.tns-mq-test{position:absolute}}", c.type = "text/css", s.className = "tns-mq-test", u.appendChild(c), u.appendChild(s), c.styleSheet ? c.styleSheet.cssText = f : c.appendChild(o.createTextNode(f)), r = window.getComputedStyle ? window.getComputedStyle(s).position : s.currentStyle.position, u.fake ? zi(u, l) : s.remove(), "absolute" === r), n),
        d = e.tTf ? ki(e.tTf) : Ri(e, "tTf", $i("transform"), n),
        v = e.t3D ? ki(e.t3D) : Ri(e, "t3D", function (t) {
      if (!t) return !1;
      if (!window.getComputedStyle) return !1;
      var e,
          n = document,
          i = Ii(),
          a = Pi(i),
          r = n.createElement("p"),
          o = 9 < t.length ? "-" + t.slice(0, -9).toLowerCase() + "-" : "";
      return o += "transform", i.insertBefore(r, null), r.style[t] = "translate3d(1px,1px,1px)", e = window.getComputedStyle(r).getPropertyValue(o), i.fake ? zi(i, a) : r.remove(), void 0 !== e && 0 < e.length && "none" !== e;
    }(d), n),
        x = e.tTDu ? ki(e.tTDu) : Ri(e, "tTDu", $i("transitionDuration"), n),
        p = e.tTDe ? ki(e.tTDe) : Ri(e, "tTDe", $i("transitionDelay"), n),
        b = e.tADu ? ki(e.tADu) : Ri(e, "tADu", $i("animationDuration"), n),
        m = e.tADe ? ki(e.tADe) : Ri(e, "tADe", $i("animationDelay"), n),
        C = e.tTE ? ki(e.tTE) : Ri(e, "tTE", ta(x, "Transition"), n),
        w = e.tAE ? ki(e.tAE) : Ri(e, "tAE", ta(b, "Animation"), n),
        M = h.console && "function" == typeof h.console.warn,
        T = ["container", "controlsContainer", "prevButton", "nextButton", "navContainer", "autoplayButton"],
        E = {};

    if (T.forEach(function (t) {
      if ("string" == typeof O[t]) {
        var e = O[t],
            n = D.querySelector(e);
        if (E[t] = e, !n || !n.nodeName) return void (M && console.warn("Can't find", O[t]));
        O[t] = n;
      }
    }), !(O.container.children.length < 1)) {
      var k = O.responsive,
          R = O.nested,
          I = "carousel" === O.mode;

      if (k) {
        0 in k && (O = Hi(O, k[0]), delete k[0]);
        var A = {};

        for (var N in k) {
          var L = k[N];
          L = "number" == typeof L ? {
            items: L
          } : L, A[N] = L;
        }

        k = A, A = null;
      }

      if (I || function t(e) {
        for (var n in e) {
          I || ("slideBy" === n && (e[n] = "page"), "edgePadding" === n && (e[n] = !1), "autoHeight" === n && (e[n] = !1)), "responsive" === n && t(e[n]);
        }
      }(O), !I) {
        O.axis = "horizontal", O.slideBy = "page", O.edgePadding = !1;
        var P = O.animateIn,
            z = O.animateOut,
            B = O.animateDelay,
            W = O.animateNormal;
      }

      var S,
          F,
          q = "horizontal" === O.axis,
          j = D.createElement("div"),
          V = D.createElement("div"),
          G = O.container,
          Q = G.parentNode,
          X = G.outerHTML,
          Y = G.children,
          K = Y.length,
          J = sn(),
          U = !1;
      k && Bn(), I && (G.className += " tns-vpfix");

      var _,
          Z,
          $,
          tt,
          et,
          nt,
          it,
          at,
          rt = O.autoWidth,
          ot = vn("fixedWidth"),
          ut = vn("edgePadding"),
          lt = vn("gutter"),
          st = fn(),
          ct = vn("center"),
          ft = rt ? 1 : Math.floor(vn("items")),
          dt = vn("slideBy"),
          vt = O.viewportMax || O.fixedWidthViewportWidth,
          pt = vn("arrowKeys"),
          mt = vn("speed"),
          ht = O.rewind,
          yt = !ht && O.loop,
          gt = vn("autoHeight"),
          xt = vn("controls"),
          bt = vn("controlsText"),
          Ct = vn("nav"),
          wt = vn("touch"),
          Mt = vn("mouseDrag"),
          Tt = vn("autoplay"),
          Et = vn("autoplayTimeout"),
          At = vn("autoplayText"),
          Nt = vn("autoplayHoverPause"),
          Lt = vn("autoplayResetOnVisibility"),
          Bt = (at = document.createElement("style"), it && at.setAttribute("media", it), document.querySelector("head").appendChild(at), at.sheet ? at.sheet : at.styleSheet),
          St = O.lazyload,
          Ot = (O.lazyloadSelector, []),
          Dt = yt ? (et = function () {
        {
          if (rt || ot && !vt) return K - 1;
          var t = ot ? "fixedWidth" : "items",
              e = [];
          if ((ot || O[t] < K) && e.push(O[t]), k) for (var n in k) {
            var i = k[n][t];
            i && (ot || i < K) && e.push(i);
          }
          return e.length || e.push(0), Math.ceil(ot ? vt / Math.min.apply(null, e) : Math.max.apply(null, e));
        }
      }(), nt = I ? Math.ceil((5 * et - K) / 2) : 4 * et - K, nt = Math.max(et, nt), dn("edgePadding") ? nt + 1 : nt) : 0,
          Ht = I ? K + 2 * Dt : K + Dt,
          kt = !(!ot && !rt || yt),
          Rt = ot ? ni() : null,
          It = !I || !yt,
          Pt = q ? "left" : "top",
          zt = "",
          Wt = "",
          Ft = ot ? function () {
        return ct && !yt ? K - 1 : Math.ceil(-Rt / (ot + lt));
      } : rt ? function () {
        for (var t = Ht; t--;) {
          if (_[t] >= -Rt) return t;
        }
      } : function () {
        return ct && I && !yt ? K - 1 : yt || I ? Math.max(0, Ht - Math.ceil(ft)) : Ht - 1;
      },
          qt = on(vn("startIndex")),
          jt = qt,
          Vt = (rn(), 0),
          Gt = rt ? null : Ft(),
          Qt = O.preventActionWhenRunning,
          Xt = O.swipeAngle,
          Yt = !Xt || "?",
          Kt = !1,
          Jt = O.onInit,
          Ut = new ia(),
          _t = " tns-slider tns-" + O.mode,
          Zt = G.id || (tt = window.tnsId, window.tnsId = tt ? tt + 1 : 1, "tns" + window.tnsId),
          $t = vn("disable"),
          te = !1,
          ee = O.freezable,
          ne = !(!ee || rt) && Ln(),
          ie = !1,
          ae = {
        click: fi,
        keydown: function keydown(t) {
          t = xi(t);
          var e = [a.LEFT, a.RIGHT].indexOf(t.keyCode);
          0 <= e && (0 === e ? Ee.disabled || fi(t, -1) : Ae.disabled || fi(t, 1));
        }
      },
          re = {
        click: function click(t) {
          if (Kt) {
            if (Qt) return;
            si();
          }

          var e = bi(t = xi(t));

          for (; e !== Se && !Qi(e, "data-nav");) {
            e = e.parentNode;
          }

          if (Qi(e, "data-nav")) {
            var n = ke = Number(Xi(e, "data-nav")),
                i = ot || rt ? n * K / De : n * ft,
                a = ve ? n : Math.min(Math.ceil(i), K - 1);
            ci(a, t), Re === n && (qe && hi(), ke = -1);
          }
        },
        keydown: function keydown(t) {
          t = xi(t);
          var e = D.activeElement;
          if (!Qi(e, "data-nav")) return;
          var n = [a.LEFT, a.RIGHT, a.ENTER, a.SPACE].indexOf(t.keyCode),
              i = Number(Xi(e, "data-nav"));
          0 <= n && (0 === n ? 0 < i && gi(Be[i - 1]) : 1 === n ? i < De - 1 && gi(Be[i + 1]) : ci(ke = i, t));
        }
      },
          oe = {
        mouseover: function mouseover() {
          qe && (vi(), je = !0);
        },
        mouseout: function mouseout() {
          je && (di(), je = !1);
        }
      },
          ue = {
        visibilitychange: function visibilitychange() {
          D.hidden ? qe && (vi(), Ge = !0) : Ge && (di(), Ge = !1);
        }
      },
          le = {
        keydown: function keydown(t) {
          t = xi(t);
          var e = [a.LEFT, a.RIGHT].indexOf(t.keyCode);
          0 <= e && fi(t, 0 === e ? -1 : 1);
        }
      },
          se = {
        touchstart: Ti,
        touchmove: Ei,
        touchend: Ai,
        touchcancel: Ai
      },
          ce = {
        mousedown: Ti,
        mousemove: Ei,
        mouseup: Ai,
        mouseleave: Ai
      },
          fe = dn("controls"),
          de = dn("nav"),
          ve = !!rt || O.navAsThumbnails,
          pe = dn("autoplay"),
          me = dn("touch"),
          he = dn("mouseDrag"),
          ye = "tns-slide-active",
          ge = "tns-complete",
          xe = {
        load: function load(t) {
          zn(bi(t));
        },
        error: function error(t) {
          e = bi(t), Vi(e, "failed"), Wn(e);
          var e;
        }
      },
          be = "force" === O.preventScrollOnTouch;

      if (fe) var Ce,
          we,
          Me = O.controlsContainer,
          Te = O.controlsContainer ? O.controlsContainer.outerHTML : "",
          Ee = O.prevButton,
          Ae = O.nextButton,
          Ne = O.prevButton ? O.prevButton.outerHTML : "",
          Le = O.nextButton ? O.nextButton.outerHTML : "";
      if (de) var Be,
          Se = O.navContainer,
          Oe = O.navContainer ? O.navContainer.outerHTML : "",
          De = rt ? K : Li(),
          He = 0,
          ke = -1,
          Re = ln(),
          Ie = Re,
          Pe = "tns-nav-active",
          ze = "Carousel Page ",
          We = " (Current Slide)";
      if (pe) var Fe,
          qe,
          je,
          Ve,
          Ge,
          Qe = "forward" === O.autoplayDirection ? 1 : -1,
          Xe = O.autoplayButton,
          Ye = O.autoplayButton ? O.autoplayButton.outerHTML : "",
          Ke = ["<span class='tns-visually-hidden'>", " animation</span>"];
      if (me || he) var Je,
          Ue,
          _e = {},
          Ze = {},
          $e = !1,
          tn = q ? function (t, e) {
        return t.x - e.x;
      } : function (t, e) {
        return t.y - e.y;
      };
      rt || an($t || ne), d && (Pt = d, zt = "translate", v ? (zt += q ? "3d(" : "3d(0px, ", Wt = q ? ", 0px, 0px)" : ", 0px)") : (zt += q ? "X(" : "Y(", Wt = ")")), I && (G.className = G.className.replace("tns-vpfix", "")), function () {
        dn("gutter");
        j.className = "tns-outer", V.className = "tns-inner", j.id = Zt + "-ow", V.id = Zt + "-iw", "" === G.id && (G.id = Zt);
        _t += g || rt ? " tns-subpixel" : " tns-no-subpixel", _t += y ? " tns-calc" : " tns-no-calc", rt && (_t += " tns-autowidth");
        _t += " tns-" + O.axis, G.className += _t, I ? ((S = D.createElement("div")).id = Zt + "-mw", S.className = "tns-ovh", j.appendChild(S), S.appendChild(V)) : j.appendChild(V);

        if (gt) {
          var t = S || V;
          t.className += " tns-ah";
        }

        if (Q.insertBefore(j, G), V.appendChild(G), qi(Y, function (t, e) {
          Vi(t, "tns-item"), t.id || (t.id = Zt + "-item" + e), !I && W && Vi(t, W), Yi(t, {
            "aria-hidden": "true",
            tabindex: "-1"
          });
        }), Dt) {
          for (var e = D.createDocumentFragment(), n = D.createDocumentFragment(), i = Dt; i--;) {
            var a = i % K,
                r = Y[a].cloneNode(!0);

            if (Ki(r, "id"), n.insertBefore(r, n.firstChild), I) {
              var o = Y[K - 1 - a].cloneNode(!0);
              Ki(o, "id"), e.appendChild(o);
            }
          }

          G.insertBefore(e, G.firstChild), G.appendChild(n), Y = G.children;
        }
      }(), function () {
        if (!I) for (var t = qt, e = qt + Math.min(K, ft); t < e; t++) {
          var n = Y[t];
          n.style.left = 100 * (t - qt) / ft + "%", Vi(n, P), Gi(n, W);
        }
        q && (g || rt ? (Wi(Bt, "#" + Zt + " > .tns-item", "font-size:" + h.getComputedStyle(Y[0]).fontSize + ";", Fi(Bt)), Wi(Bt, "#" + Zt, "font-size:0;", Fi(Bt))) : I && qi(Y, function (t, e) {
          var n;
          t.style.marginLeft = (n = e, y ? y + "(" + 100 * n + "% / " + Ht + ")" : 100 * n / Ht + "%");
        }));

        if (H) {
          if (x) {
            var i = S && O.autoHeight ? xn(O.speed) : "";
            Wi(Bt, "#" + Zt + "-mw", i, Fi(Bt));
          }

          i = pn(O.edgePadding, O.gutter, O.fixedWidth, O.speed, O.autoHeight), Wi(Bt, "#" + Zt + "-iw", i, Fi(Bt)), I && (i = q && !rt ? "width:" + mn(O.fixedWidth, O.gutter, O.items) + ";" : "", x && (i += xn(mt)), Wi(Bt, "#" + Zt, i, Fi(Bt))), i = q && !rt ? hn(O.fixedWidth, O.gutter, O.items) : "", O.gutter && (i += yn(O.gutter)), I || (x && (i += xn(mt)), b && (i += bn(mt))), i && Wi(Bt, "#" + Zt + " > .tns-item", i, Fi(Bt));
        } else {
          Gn(), V.style.cssText = pn(ut, lt, ot, gt), I && q && !rt && (G.style.width = mn(ot, lt, ft));
          var i = q && !rt ? hn(ot, lt, ft) : "";
          lt && (i += yn(lt)), i && Wi(Bt, "#" + Zt + " > .tns-item", i, Fi(Bt));
        }

        if (k && H) for (var a in k) {
          a = parseInt(a);
          var r = k[a],
              i = "",
              o = "",
              u = "",
              l = "",
              s = "",
              c = rt ? null : vn("items", a),
              f = vn("fixedWidth", a),
              d = vn("speed", a),
              v = vn("edgePadding", a),
              p = vn("autoHeight", a),
              m = vn("gutter", a);
          x && S && vn("autoHeight", a) && "speed" in r && (o = "#" + Zt + "-mw{" + xn(d) + "}"), ("edgePadding" in r || "gutter" in r) && (u = "#" + Zt + "-iw{" + pn(v, m, f, d, p) + "}"), I && q && !rt && ("fixedWidth" in r || "items" in r || ot && "gutter" in r) && (l = "width:" + mn(f, m, c) + ";"), x && "speed" in r && (l += xn(d)), l && (l = "#" + Zt + "{" + l + "}"), ("fixedWidth" in r || ot && "gutter" in r || !I && "items" in r) && (s += hn(f, m, c)), "gutter" in r && (s += yn(m)), !I && "speed" in r && (x && (s += xn(d)), b && (s += bn(d))), s && (s = "#" + Zt + " > .tns-item{" + s + "}"), (i = o + u + l + s) && Bt.insertRule("@media (min-width: " + a / 16 + "em) {" + i + "}", Bt.cssRules.length);
        }
      }(), Cn();
      var en = yt ? I ? function () {
        var t = Vt,
            e = Gt;
        t += dt, e -= dt, ut ? (t += 1, e -= 1) : ot && (st + lt) % (ot + lt) && (e -= 1), Dt && (e < qt ? qt -= K : qt < t && (qt += K));
      } : function () {
        if (Gt < qt) for (; Vt + K <= qt;) {
          qt -= K;
        } else if (qt < Vt) for (; qt <= Gt - K;) {
          qt += K;
        }
      } : function () {
        qt = Math.max(Vt, Math.min(Gt, qt));
      },
          nn = I ? function () {
        var e, n, i, a, t, r, o, u, l, s, c;
        ti(G, ""), x || !mt ? (ri(), mt && Zi(G) || si()) : (e = G, n = Pt, i = zt, a = Wt, t = ii(), r = mt, o = si, u = Math.min(r, 10), l = 0 <= t.indexOf("%") ? "%" : "px", t = t.replace(l, ""), s = Number(e.style[n].replace(i, "").replace(a, "").replace(l, "")), c = (t - s) / r * u, setTimeout(function t() {
          r -= u, s += c, e.style[n] = i + s + l + a, 0 < r ? setTimeout(t, u) : o();
        }, u)), q || Ni();
      } : function () {
        Ot = [];
        var t = {};
        t[C] = t[w] = si, na(Y[jt], t), ea(Y[qt], t), oi(jt, P, z, !0), oi(qt, W, P), C && w && mt && Zi(G) || si();
      };
      return {
        version: "2.9.2",
        getInfo: Si,
        events: Ut,
        goTo: ci,
        play: function play() {
          Tt && !qe && (mi(), Ve = !1);
        },
        pause: function pause() {
          qe && (hi(), Ve = !0);
        },
        isOn: U,
        updateSliderHeight: Xn,
        refresh: Cn,
        destroy: function destroy() {
          if (Bt.disabled = !0, Bt.ownerNode && Bt.ownerNode.remove(), na(h, {
            resize: An
          }), pt && na(D, le), Me && na(Me, ae), Se && na(Se, re), na(G, oe), na(G, ue), Xe && na(Xe, {
            click: yi
          }), Tt && clearInterval(Fe), I && C) {
            var t = {};
            t[C] = si, na(G, t);
          }

          wt && na(G, se), Mt && na(G, ce);
          var r = [X, Te, Ne, Le, Oe, Ye];

          for (var e in T.forEach(function (t, e) {
            var n = "container" === t ? j : O[t];

            if ("object" == _typeof(n)) {
              var i = !!n.previousElementSibling && n.previousElementSibling,
                  a = n.parentNode;
              n.outerHTML = r[e], O[t] = i ? i.nextElementSibling : a.firstElementChild;
            }
          }), T = P = z = B = W = q = j = V = G = Q = X = Y = K = F = J = rt = ot = ut = lt = st = ft = dt = vt = pt = mt = ht = yt = gt = Bt = St = _ = Ot = Dt = Ht = kt = Rt = It = Pt = zt = Wt = Ft = qt = jt = Vt = Gt = Xt = Yt = Kt = Jt = Ut = _t = Zt = $t = te = ee = ne = ie = ae = re = oe = ue = le = se = ce = fe = de = ve = pe = me = he = ye = ge = xe = Z = xt = bt = Me = Te = Ee = Ae = Ce = we = Ct = Se = Oe = Be = De = He = ke = Re = Ie = Pe = ze = We = Tt = Et = Qe = At = Nt = Xe = Ye = Lt = Ke = Fe = qe = je = Ve = Ge = _e = Ze = Je = $e = Ue = tn = wt = Mt = null, this) {
            "rebuild" !== e && (this[e] = null);
          }

          U = !1;
        },
        rebuild: function rebuild() {
          return aa(Hi(O, E));
        }
      };
    }

    function an(t) {
      t && (xt = Ct = wt = Mt = pt = Tt = Nt = Lt = !1);
    }

    function rn() {
      for (var t = I ? qt - Dt : qt; t < 0;) {
        t += K;
      }

      return t % K + 1;
    }

    function on(t) {
      return t = t ? Math.max(0, Math.min(yt ? K - 1 : K - ft, t)) : 0, I ? t + Dt : t;
    }

    function un(t) {
      for (null == t && (t = qt), I && (t -= Dt); t < 0;) {
        t += K;
      }

      return Math.floor(t % K);
    }

    function ln() {
      var t,
          e = un();
      return t = ve ? e : ot || rt ? Math.ceil((e + 1) * De / K - 1) : Math.floor(e / ft), !yt && I && qt === Gt && (t = De - 1), t;
    }

    function sn() {
      return h.innerWidth || D.documentElement.clientWidth || D.body.clientWidth;
    }

    function cn(t) {
      return "top" === t ? "afterbegin" : "beforeend";
    }

    function fn() {
      var t = ut ? 2 * ut - lt : 0;
      return function t(e) {
        var n,
            i,
            a = D.createElement("div");
        return e.appendChild(a), i = (n = a.getBoundingClientRect()).right - n.left, a.remove(), i || t(e.parentNode);
      }(Q) - t;
    }

    function dn(t) {
      if (O[t]) return !0;
      if (k) for (var e in k) {
        if (k[e][t]) return !0;
      }
      return !1;
    }

    function vn(t, e) {
      if (null == e && (e = J), "items" === t && ot) return Math.floor((st + lt) / (ot + lt)) || 1;
      var n = O[t];
      if (k) for (var i in k) {
        e >= parseInt(i) && t in k[i] && (n = k[i][t]);
      }
      return "slideBy" === t && "page" === n && (n = vn("items")), I || "slideBy" !== t && "items" !== t || (n = Math.floor(n)), n;
    }

    function pn(t, e, n, i, a) {
      var r = "";

      if (void 0 !== t) {
        var o = t;
        e && (o -= e), r = q ? "margin: 0 " + o + "px 0 " + t + "px;" : "margin: " + t + "px 0 " + o + "px 0;";
      } else if (e && !n) {
        var u = "-" + e + "px";
        r = "margin: 0 " + (q ? u + " 0 0" : "0 " + u + " 0") + ";";
      }

      return !I && a && x && i && (r += xn(i)), r;
    }

    function mn(t, e, n) {
      return t ? (t + e) * Ht + "px" : y ? y + "(" + 100 * Ht + "% / " + n + ")" : 100 * Ht / n + "%";
    }

    function hn(t, e, n) {
      var i;
      if (t) i = t + e + "px";else {
        I || (n = Math.floor(n));
        var a = I ? Ht : n;
        i = y ? y + "(100% / " + a + ")" : 100 / a + "%";
      }
      return i = "width:" + i, "inner" !== R ? i + ";" : i + " !important;";
    }

    function yn(t) {
      var e = "";
      !1 !== t && (e = (q ? "padding-" : "margin-") + (q ? "right" : "bottom") + ": " + t + "px;");
      return e;
    }

    function gn(t, e) {
      var n = t.substring(0, t.length - e).toLowerCase();
      return n && (n = "-" + n + "-"), n;
    }

    function xn(t) {
      return gn(x, 18) + "transition-duration:" + t / 1e3 + "s;";
    }

    function bn(t) {
      return gn(b, 17) + "animation-duration:" + t / 1e3 + "s;";
    }

    function Cn() {
      if (dn("autoHeight") || rt || !q) {
        var t = G.querySelectorAll("img");
        qi(t, function (t) {
          var e = t.src;
          e && e.indexOf("data:image") < 0 ? (ea(t, xe), t.src = "", t.src = e, Vi(t, "loading")) : St || zn(t);
        }), Oi(function () {
          jn(Ji(t), function () {
            Z = !0;
          });
        }), !rt && q && (t = Fn(qt, Math.min(qt + ft - 1, Ht - 1))), St ? wn() : Oi(function () {
          jn(Ji(t), wn);
        });
      } else I && ai(), Tn(), En();
    }

    function wn() {
      if (rt) {
        var e = yt ? qt : K - 1;
        !function t() {
          Y[e - 1].getBoundingClientRect().right.toFixed(2) === Y[e].getBoundingClientRect().left.toFixed(2) ? Mn() : setTimeout(function () {
            t();
          }, 16);
        }();
      } else Mn();
    }

    function Mn() {
      q && !rt || (Yn(), rt ? (Rt = ni(), ee && (ne = Ln()), Gt = Ft(), an($t || ne)) : Ni()), I && ai(), Tn(), En();
    }

    function Tn() {
      if (Kn(), j.insertAdjacentHTML("afterbegin", '<div class="tns-liveregion tns-visually-hidden" aria-live="polite" aria-atomic="true">slide <span class="current">' + Rn() + "</span>  of " + K + "</div>"), $ = j.querySelector(".tns-liveregion .current"), pe) {
        var t = Tt ? "stop" : "start";
        Xe ? Yi(Xe, {
          "data-action": t
        }) : O.autoplayButtonOutput && (j.insertAdjacentHTML(cn(O.autoplayPosition), '<button data-action="' + t + '">' + Ke[0] + t + Ke[1] + At[0] + "</button>"), Xe = j.querySelector("[data-action]")), Xe && ea(Xe, {
          click: yi
        }), Tt && (mi(), Nt && ea(G, oe), Lt && ea(G, ue));
      }

      if (de) {
        if (Se) Yi(Se, {
          "aria-label": "Carousel Pagination"
        }), qi(Be = Se.children, function (t, e) {
          Yi(t, {
            "data-nav": e,
            tabindex: "-1",
            "aria-label": ze + (e + 1),
            "aria-controls": Zt
          });
        });else {
          for (var e = "", n = ve ? "" : 'style="display:none"', i = 0; i < K; i++) {
            e += '<button data-nav="' + i + '" tabindex="-1" aria-controls="' + Zt + '" ' + n + ' aria-label="' + ze + (i + 1) + '"></button>';
          }

          e = '<div class="tns-nav" aria-label="Carousel Pagination">' + e + "</div>", j.insertAdjacentHTML(cn(O.navPosition), e), Se = j.querySelector(".tns-nav"), Be = Se.children;
        }

        if (Bi(), x) {
          var a = x.substring(0, x.length - 18).toLowerCase(),
              r = "transition: all " + mt / 1e3 + "s";
          a && (r = "-" + a + "-" + r), Wi(Bt, "[aria-controls^=" + Zt + "-item]", r, Fi(Bt));
        }

        Yi(Be[Re], {
          "aria-label": ze + (Re + 1) + We
        }), Ki(Be[Re], "tabindex"), Vi(Be[Re], Pe), ea(Se, re);
      }

      fe && (Me || Ee && Ae || (j.insertAdjacentHTML(cn(O.controlsPosition), '<div class="tns-controls" aria-label="Carousel Navigation" tabindex="0"><button data-controls="prev" tabindex="-1" aria-controls="' + Zt + '">' + bt[0] + '</button><button data-controls="next" tabindex="-1" aria-controls="' + Zt + '">' + bt[1] + "</button></div>"), Me = j.querySelector(".tns-controls")), Ee && Ae || (Ee = Me.children[0], Ae = Me.children[1]), O.controlsContainer && Yi(Me, {
        "aria-label": "Carousel Navigation",
        tabindex: "0"
      }), (O.controlsContainer || O.prevButton && O.nextButton) && Yi([Ee, Ae], {
        "aria-controls": Zt,
        tabindex: "-1"
      }), (O.controlsContainer || O.prevButton && O.nextButton) && (Yi(Ee, {
        "data-controls": "prev"
      }), Yi(Ae, {
        "data-controls": "next"
      })), Ce = Un(Ee), we = Un(Ae), $n(), Me ? ea(Me, ae) : (ea(Ee, ae), ea(Ae, ae))), Sn();
    }

    function En() {
      if (I && C) {
        var t = {};
        t[C] = si, ea(G, t);
      }

      wt && ea(G, se, O.preventScrollOnTouch), Mt && ea(G, ce), pt && ea(D, le), "inner" === R ? Ut.on("outerResized", function () {
        Nn(), Ut.emit("innerLoaded", Si());
      }) : (k || ot || rt || gt || !q) && ea(h, {
        resize: An
      }), gt && ("outer" === R ? Ut.on("innerLoaded", qn) : $t || qn()), Pn(), $t ? Hn() : ne && Dn(), Ut.on("indexChanged", Vn), "inner" === R && Ut.emit("innerLoaded", Si()), "function" == typeof Jt && Jt(Si()), U = !0;
    }

    function An(t) {
      Oi(function () {
        Nn(xi(t));
      });
    }

    function Nn(t) {
      if (U) {
        "outer" === R && Ut.emit("outerResized", Si(t)), J = sn();
        var e,
            n = F,
            i = !1;
        k && (Bn(), (e = n !== F) && Ut.emit("newBreakpointStart", Si(t)));
        var a,
            r,
            o,
            u,
            l = ft,
            s = $t,
            c = ne,
            f = pt,
            d = xt,
            v = Ct,
            p = wt,
            m = Mt,
            h = Tt,
            y = Nt,
            g = Lt,
            x = qt;

        if (e) {
          var b = ot,
              C = gt,
              w = bt,
              M = ct,
              T = At;
          if (!H) var E = lt,
              A = ut;
        }

        if (pt = vn("arrowKeys"), xt = vn("controls"), Ct = vn("nav"), wt = vn("touch"), ct = vn("center"), Mt = vn("mouseDrag"), Tt = vn("autoplay"), Nt = vn("autoplayHoverPause"), Lt = vn("autoplayResetOnVisibility"), e && ($t = vn("disable"), ot = vn("fixedWidth"), mt = vn("speed"), gt = vn("autoHeight"), bt = vn("controlsText"), At = vn("autoplayText"), Et = vn("autoplayTimeout"), H || (ut = vn("edgePadding"), lt = vn("gutter"))), an($t), st = fn(), q && !rt || $t || (Yn(), q || (Ni(), i = !0)), (ot || rt) && (Rt = ni(), Gt = Ft()), (e || ot) && (ft = vn("items"), dt = vn("slideBy"), (r = ft !== l) && (ot || rt || (Gt = Ft()), en())), e && $t !== s && ($t ? Hn() : function () {
          if (!te) return;
          if (Bt.disabled = !1, G.className += _t, ai(), yt) for (var t = Dt; t--;) {
            I && _i(Y[t]), _i(Y[Ht - t - 1]);
          }
          if (!I) for (var e = qt, n = qt + K; e < n; e++) {
            var i = Y[e],
                a = e < qt + ft ? P : W;
            i.style.left = 100 * (e - qt) / ft + "%", Vi(i, a);
          }
          On(), te = !1;
        }()), ee && (e || ot || rt) && (ne = Ln()) !== c && (ne ? (ri(ii(on(0))), Dn()) : (!function () {
          if (!ie) return;
          ut && H && (V.style.margin = "");
          if (Dt) for (var t = "tns-transparent", e = Dt; e--;) {
            I && Gi(Y[e], t), Gi(Y[Ht - e - 1], t);
          }
          On(), ie = !1;
        }(), i = !0)), an($t || ne), Tt || (Nt = Lt = !1), pt !== f && (pt ? ea(D, le) : na(D, le)), xt !== d && (xt ? Me ? _i(Me) : (Ee && _i(Ee), Ae && _i(Ae)) : Me ? Ui(Me) : (Ee && Ui(Ee), Ae && Ui(Ae))), Ct !== v && (Ct ? _i(Se) : Ui(Se)), wt !== p && (wt ? ea(G, se, O.preventScrollOnTouch) : na(G, se)), Mt !== m && (Mt ? ea(G, ce) : na(G, ce)), Tt !== h && (Tt ? (Xe && _i(Xe), qe || Ve || mi()) : (Xe && Ui(Xe), qe && hi())), Nt !== y && (Nt ? ea(G, oe) : na(G, oe)), Lt !== g && (Lt ? ea(D, ue) : na(D, ue)), e) {
          if (ot === b && ct === M || (i = !0), gt !== C && (gt || (V.style.height = "")), xt && bt !== w && (Ee.innerHTML = bt[0], Ae.innerHTML = bt[1]), Xe && At !== T) {
            var N = Tt ? 1 : 0,
                L = Xe.innerHTML,
                B = L.length - T[N].length;
            L.substring(B) === T[N] && (Xe.innerHTML = L.substring(0, B) + At[N]);
          }
        } else ct && (ot || rt) && (i = !0);

        if ((r || ot && !rt) && (De = Li(), Bi()), (a = qt !== x) ? (Ut.emit("indexChanged", Si()), i = !0) : r ? a || Vn() : (ot || rt) && (Pn(), Kn(), kn()), r && !I && function () {
          for (var t = qt + Math.min(K, ft), e = Ht; e--;) {
            var n = Y[e];
            qt <= e && e < t ? (Vi(n, "tns-moving"), n.style.left = 100 * (e - qt) / ft + "%", Vi(n, P), Gi(n, W)) : n.style.left && (n.style.left = "", Vi(n, W), Gi(n, P)), Gi(n, z);
          }

          setTimeout(function () {
            qi(Y, function (t) {
              Gi(t, "tns-moving");
            });
          }, 300);
        }(), !$t && !ne) {
          if (e && !H && (gt === autoheightTem && mt === speedTem || Gn(), ut === A && lt === E || (V.style.cssText = pn(ut, lt, ot, mt, gt)), q)) {
            I && (G.style.width = mn(ot, lt, ft));
            var S = hn(ot, lt, ft) + yn(lt);
            u = Fi(o = Bt) - 1, "deleteRule" in o ? o.deleteRule(u) : o.removeRule(u), Wi(Bt, "#" + Zt + " > .tns-item", S, Fi(Bt));
          }

          gt && qn(), i && (ai(), jt = qt);
        }

        e && Ut.emit("newBreakpointEnd", Si(t));
      }
    }

    function Ln() {
      if (!ot && !rt) return K <= (ct ? ft - (ft - 1) / 2 : ft);
      var t = ot ? (ot + lt) * K : _[K],
          e = ut ? st + 2 * ut : st + lt;
      return ct && (e -= ot ? (st - ot) / 2 : (st - (_[qt + 1] - _[qt] - lt)) / 2), t <= e;
    }

    function Bn() {
      for (var t in F = 0, k) {
        (t = parseInt(t)) <= J && (F = t);
      }
    }

    function Sn() {
      !Tt && Xe && Ui(Xe), !Ct && Se && Ui(Se), xt || (Me ? Ui(Me) : (Ee && Ui(Ee), Ae && Ui(Ae)));
    }

    function On() {
      Tt && Xe && _i(Xe), Ct && Se && _i(Se), xt && (Me ? _i(Me) : (Ee && _i(Ee), Ae && _i(Ae)));
    }

    function Dn() {
      if (!ie) {
        if (ut && (V.style.margin = "0px"), Dt) for (var t = "tns-transparent", e = Dt; e--;) {
          I && Vi(Y[e], t), Vi(Y[Ht - e - 1], t);
        }
        Sn(), ie = !0;
      }
    }

    function Hn() {
      if (!te) {
        if (Bt.disabled = !0, G.className = G.className.replace(_t.substring(1), ""), Ki(G, ["style"]), yt) for (var t = Dt; t--;) {
          I && Ui(Y[t]), Ui(Y[Ht - t - 1]);
        }
        if (q && I || Ki(V, ["style"]), !I) for (var e = qt, n = qt + K; e < n; e++) {
          var i = Y[e];
          Ki(i, ["style"]), Gi(i, P), Gi(i, W);
        }
        Sn(), te = !0;
      }
    }

    function kn() {
      var t = Rn();
      $.innerHTML !== t && ($.innerHTML = t);
    }

    function Rn() {
      var t = In(),
          e = t[0] + 1,
          n = t[1] + 1;
      return e === n ? e + "" : e + " to " + n;
    }

    function In(t) {
      null == t && (t = ii());
      var n,
          i,
          a,
          r = qt;
      if (ct || ut ? (rt || ot) && (i = -(parseFloat(t) + ut), a = i + st + 2 * ut) : rt && (i = _[qt], a = i + st), rt) _.forEach(function (t, e) {
        e < Ht && ((ct || ut) && t <= i + .5 && (r = e), .5 <= a - t && (n = e));
      });else {
        if (ot) {
          var e = ot + lt;
          ct || ut ? (r = Math.floor(i / e), n = Math.ceil(a / e - 1)) : n = r + Math.ceil(st / e) - 1;
        } else if (ct || ut) {
          var o = ft - 1;

          if (ct ? (r -= o / 2, n = qt + o / 2) : n = qt + o, ut) {
            var u = ut * ft / st;
            r -= u, n += u;
          }

          r = Math.floor(r), n = Math.ceil(n);
        } else n = r + ft - 1;

        r = Math.max(r, 0), n = Math.min(n, Ht - 1);
      }
      return [r, n];
    }

    function Pn() {
      St && !$t && Fn.apply(null, In()).forEach(function (t) {
        if (!ji(t, ge)) {
          var e = {};
          e[C] = function (t) {
            t.stopPropagation();
          }, ea(t, e), ea(t, xe), t.src = Xi(t, "data-src");
          var n = Xi(t, "data-srcset");
          n && (t.srcset = n), Vi(t, "loading");
        }
      });
    }

    function zn(t) {
      Vi(t, "loaded"), Wn(t);
    }

    function Wn(t) {
      Vi(t, "tns-complete"), Gi(t, "loading"), na(t, xe);
    }

    function Fn(t, e) {
      for (var n = []; t <= e;) {
        qi(Y[t].querySelectorAll("img"), function (t) {
          n.push(t);
        }), t++;
      }

      return n;
    }

    function qn() {
      var t = Fn.apply(null, In());
      Oi(function () {
        jn(t, Xn);
      });
    }

    function jn(n, t) {
      return Z ? t() : (n.forEach(function (t, e) {
        ji(t, ge) && n.splice(e, 1);
      }), n.length ? void Oi(function () {
        jn(n, t);
      }) : t());
    }

    function Vn() {
      Pn(), Kn(), kn(), $n(), function () {
        if (Ct && (Re = 0 <= ke ? ke : ln(), ke = -1, Re !== Ie)) {
          var t = Be[Ie],
              e = Be[Re];
          Yi(t, {
            tabindex: "-1",
            "aria-label": ze + (Ie + 1)
          }), Gi(t, Pe), Yi(e, {
            "aria-label": ze + (Re + 1) + We
          }), Ki(e, "tabindex"), Vi(e, Pe), Ie = Re;
        }
      }();
    }

    function Gn() {
      I && gt && (S.style[x] = mt / 1e3 + "s");
    }

    function Qn(t, e) {
      for (var n = [], i = t, a = Math.min(t + e, Ht); i < a; i++) {
        n.push(Y[i].offsetHeight);
      }

      return Math.max.apply(null, n);
    }

    function Xn() {
      var t = gt ? Qn(qt, ft) : Qn(Dt, K),
          e = S || V;
      e.style.height !== t && (e.style.height = t + "px");
    }

    function Yn() {
      _ = [0];
      var n = q ? "left" : "top",
          i = q ? "right" : "bottom",
          a = Y[0].getBoundingClientRect()[n];
      qi(Y, function (t, e) {
        e && _.push(t.getBoundingClientRect()[n] - a), e === Ht - 1 && _.push(t.getBoundingClientRect()[i] - a);
      });
    }

    function Kn() {
      var t = In(),
          n = t[0],
          i = t[1];
      qi(Y, function (t, e) {
        n <= e && e <= i ? Qi(t, "aria-hidden") && (Ki(t, ["aria-hidden", "tabindex"]), Vi(t, ye)) : Qi(t, "aria-hidden") || (Yi(t, {
          "aria-hidden": "true",
          tabindex: "-1"
        }), Gi(t, ye));
      });
    }

    function Jn(t) {
      return t.nodeName.toLowerCase();
    }

    function Un(t) {
      return "button" === Jn(t);
    }

    function _n(t) {
      return "true" === t.getAttribute("aria-disabled");
    }

    function Zn(t, e, n) {
      t ? e.disabled = n : e.setAttribute("aria-disabled", n.toString());
    }

    function $n() {
      if (xt && !ht && !yt) {
        var t = Ce ? Ee.disabled : _n(Ee),
            e = we ? Ae.disabled : _n(Ae),
            n = qt <= Vt,
            i = !ht && Gt <= qt;
        n && !t && Zn(Ce, Ee, !0), !n && t && Zn(Ce, Ee, !1), i && !e && Zn(we, Ae, !0), !i && e && Zn(we, Ae, !1);
      }
    }

    function ti(t, e) {
      x && (t.style[x] = e);
    }

    function ei(t) {
      return null == t && (t = qt), rt ? (st - (ut ? lt : 0) - (_[t + 1] - _[t] - lt)) / 2 : ot ? (st - ot) / 2 : (ft - 1) / 2;
    }

    function ni() {
      var t = st + (ut ? lt : 0) - (ot ? (ot + lt) * Ht : _[Ht]);
      return ct && !yt && (t = ot ? -(ot + lt) * (Ht - 1) - ei() : ei(Ht - 1) - _[Ht - 1]), 0 < t && (t = 0), t;
    }

    function ii(t) {
      var e;
      if (null == t && (t = qt), q && !rt) {
        if (ot) e = -(ot + lt) * t, ct && (e += ei());else {
          var n = d ? Ht : ft;
          ct && (t -= ei()), e = 100 * -t / n;
        }
      } else e = -_[t], ct && rt && (e += ei());
      return kt && (e = Math.max(e, Rt)), e += !q || rt || ot ? "px" : "%";
    }

    function ai(t) {
      ti(G, "0s"), ri(t);
    }

    function ri(t) {
      null == t && (t = ii()), G.style[Pt] = zt + t + Wt;
    }

    function oi(t, e, n, i) {
      var a = t + ft;
      yt || (a = Math.min(a, Ht));

      for (var r = t; r < a; r++) {
        var o = Y[r];
        i || (o.style.left = 100 * (r - qt) / ft + "%"), B && p && (o.style[p] = o.style[m] = B * (r - t) / 1e3 + "s"), Gi(o, e), Vi(o, n), i && Ot.push(o);
      }
    }

    function ui(t, e) {
      It && en(), (qt !== jt || e) && (Ut.emit("indexChanged", Si()), Ut.emit("transitionStart", Si()), gt && qn(), qe && t && 0 <= ["click", "keydown"].indexOf(t.type) && hi(), Kt = !0, nn());
    }

    function li(t) {
      return t.toLowerCase().replace(/-/g, "");
    }

    function si(t) {
      if (I || Kt) {
        if (Ut.emit("transitionEnd", Si(t)), !I && 0 < Ot.length) for (var e = 0; e < Ot.length; e++) {
          var n = Ot[e];
          n.style.left = "", m && p && (n.style[m] = "", n.style[p] = ""), Gi(n, z), Vi(n, W);
        }

        if (!t || !I && t.target.parentNode === G || t.target === G && li(t.propertyName) === li(Pt)) {
          if (!It) {
            var i = qt;
            en(), qt !== i && (Ut.emit("indexChanged", Si()), ai());
          }

          "inner" === R && Ut.emit("innerLoaded", Si()), Kt = !1, jt = qt;
        }
      }
    }

    function ci(t, e) {
      if (!ne) if ("prev" === t) fi(e, -1);else if ("next" === t) fi(e, 1);else {
        if (Kt) {
          if (Qt) return;
          si();
        }

        var n = un(),
            i = 0;

        if ("first" === t ? i = -n : "last" === t ? i = I ? K - ft - n : K - 1 - n : ("number" != typeof t && (t = parseInt(t)), isNaN(t) || (e || (t = Math.max(0, Math.min(K - 1, t))), i = t - n)), !I && i && Math.abs(i) < ft) {
          var a = 0 < i ? 1 : -1;
          i += Vt <= qt + i - K ? K * a : 2 * K * a * -1;
        }

        qt += i, I && yt && (qt < Vt && (qt += K), Gt < qt && (qt -= K)), un(qt) !== un(jt) && ui(e);
      }
    }

    function fi(t, e) {
      if (Kt) {
        if (Qt) return;
        si();
      }

      var n;

      if (!e) {
        for (var i = bi(t = xi(t)); i !== Me && [Ee, Ae].indexOf(i) < 0;) {
          i = i.parentNode;
        }

        var a = [Ee, Ae].indexOf(i);
        0 <= a && (n = !0, e = 0 === a ? -1 : 1);
      }

      if (ht) {
        if (qt === Vt && -1 === e) return void ci("last", t);
        if (qt === Gt && 1 === e) return void ci("first", t);
      }

      e && (qt += dt * e, rt && (qt = Math.floor(qt)), ui(n || t && "keydown" === t.type ? t : null));
    }

    function di() {
      Fe = setInterval(function () {
        fi(null, Qe);
      }, Et), qe = !0;
    }

    function vi() {
      clearInterval(Fe), qe = !1;
    }

    function pi(t, e) {
      Yi(Xe, {
        "data-action": t
      }), Xe.innerHTML = Ke[0] + t + Ke[1] + e;
    }

    function mi() {
      di(), Xe && pi("stop", At[1]);
    }

    function hi() {
      vi(), Xe && pi("start", At[0]);
    }

    function yi() {
      qe ? (hi(), Ve = !0) : (mi(), Ve = !1);
    }

    function gi(t) {
      t.focus();
    }

    function xi(t) {
      return Ci(t = t || h.event) ? t.changedTouches[0] : t;
    }

    function bi(t) {
      return t.target || h.event.srcElement;
    }

    function Ci(t) {
      return 0 <= t.type.indexOf("touch");
    }

    function wi(t) {
      t.preventDefault ? t.preventDefault() : t.returnValue = !1;
    }

    function Mi() {
      return a = Ze.y - _e.y, r = Ze.x - _e.x, t = Math.atan2(a, r) * (180 / Math.PI), e = Xt, n = !1, i = Math.abs(90 - Math.abs(t)), 90 - e <= i ? n = "horizontal" : i <= e && (n = "vertical"), n === O.axis;
      var t, e, n, i, a, r;
    }

    function Ti(t) {
      if (Kt) {
        if (Qt) return;
        si();
      }

      Tt && qe && vi(), $e = !0, Ue && (Di(Ue), Ue = null);
      var e = xi(t);
      Ut.emit(Ci(t) ? "touchStart" : "dragStart", Si(t)), !Ci(t) && 0 <= ["img", "a"].indexOf(Jn(bi(t))) && wi(t), Ze.x = _e.x = e.clientX, Ze.y = _e.y = e.clientY, I && (Je = parseFloat(G.style[Pt].replace(zt, "")), ti(G, "0s"));
    }

    function Ei(t) {
      if ($e) {
        var e = xi(t);
        Ze.x = e.clientX, Ze.y = e.clientY, I ? Ue || (Ue = Oi(function () {
          !function t(e) {
            if (!Yt) return void ($e = !1);
            Di(Ue);
            $e && (Ue = Oi(function () {
              t(e);
            }));
            "?" === Yt && (Yt = Mi());

            if (Yt) {
              !be && Ci(e) && (be = !0);

              try {
                e.type && Ut.emit(Ci(e) ? "touchMove" : "dragMove", Si(e));
              } catch (t) {}

              var n = Je,
                  i = tn(Ze, _e);
              if (!q || ot || rt) n += i, n += "px";else {
                var a = d ? i * ft * 100 / ((st + lt) * Ht) : 100 * i / (st + lt);
                n += a, n += "%";
              }
              G.style[Pt] = zt + n + Wt;
            }
          }(t);
        })) : ("?" === Yt && (Yt = Mi()), Yt && (be = !0)), be && t.preventDefault();
      }
    }

    function Ai(i) {
      if ($e) {
        Ue && (Di(Ue), Ue = null), I && ti(G, ""), $e = !1;
        var t = xi(i);
        Ze.x = t.clientX, Ze.y = t.clientY;
        var a = tn(Ze, _e);

        if (Math.abs(a)) {
          if (!Ci(i)) {
            var n = bi(i);
            ea(n, {
              click: function t(e) {
                wi(e), na(n, {
                  click: t
                });
              }
            });
          }

          I ? Ue = Oi(function () {
            if (q && !rt) {
              var t = -a * ft / (st + lt);
              t = 0 < a ? Math.floor(t) : Math.ceil(t), qt += t;
            } else {
              var e = -(Je + a);
              if (e <= 0) qt = Vt;else if (e >= _[Ht - 1]) qt = Gt;else for (var n = 0; n < Ht && e >= _[n];) {
                e > _[qt = n] && a < 0 && (qt += 1), n++;
              }
            }

            ui(i, a), Ut.emit(Ci(i) ? "touchEnd" : "dragEnd", Si(i));
          }) : Yt && fi(i, 0 < a ? -1 : 1);
        }
      }

      "auto" === O.preventScrollOnTouch && (be = !1), Xt && (Yt = "?"), Tt && !qe && di();
    }

    function Ni() {
      (S || V).style.height = _[qt + ft] - _[qt] + "px";
    }

    function Li() {
      var t = ot ? (ot + lt) * K / st : K / ft;
      return Math.min(Math.ceil(t), K);
    }

    function Bi() {
      if (Ct && !ve && De !== He) {
        var t = He,
            e = De,
            n = _i;

        for (De < He && (t = De, e = He, n = Ui); t < e;) {
          n(Be[t]), t++;
        }

        He = De;
      }
    }

    function Si(t) {
      return {
        container: G,
        slideItems: Y,
        navContainer: Se,
        navItems: Be,
        controlsContainer: Me,
        hasControls: fe,
        prevButton: Ee,
        nextButton: Ae,
        items: ft,
        slideBy: dt,
        cloneCount: Dt,
        slideCount: K,
        slideCountNew: Ht,
        index: qt,
        indexCached: jt,
        displayIndex: rn(),
        navCurrentIndex: Re,
        navCurrentIndexCached: Ie,
        pages: De,
        pagesCached: He,
        sheet: Bt,
        isOn: U,
        event: t || {}
      };
    }

    M && console.warn("No slides found in", O.container);
  };

  return aa;
}();
"use strict";

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

!function () {
  return function e(t, n, s) {
    function i(r, d) {
      if (!n[r]) {
        if (!t[r]) {
          var l = "function" == typeof require && require;
          if (!d && l) return l(r, !0);
          if (o) return o(r, !0);
          var a = new Error("Cannot find module '" + r + "'");
          throw a.code = "MODULE_NOT_FOUND", a;
        }

        var c = n[r] = {
          exports: {}
        };
        t[r][0].call(c.exports, function (e) {
          return i(t[r][1][e] || e);
        }, c, c.exports, e, t, n, s);
      }

      return n[r].exports;
    }

    for (var o = "function" == typeof require && require, r = 0; r < s.length; r++) {
      i(s[r]);
    }

    return i;
  };
}()({
  1: [function (e, t, n) {
    t.exports = function (e) {
      this.elem = document.createElement(e), this.addClassesAndCreate = function (e) {
        for (var _t in e) {
          this.elem.classList.add(e[_t]);
        }

        return this.elem;
      };
    };
  }, {}],
  2: [function (e, t, n) {
    var s = e("./DOMObject");

    t.exports = function () {
      var e = new s("div").addClassesAndCreate(["fslightbox-media-holder"]);
      return window.innerWidth > 1e3 ? (e.style.width = window.innerWidth - .1 * window.innerWidth + "px", e.style.height = window.innerHeight - .1 * window.innerHeight + "px") : (e.style.width = window.innerWidth + "px", e.style.height = window.innerHeight + "px"), e;
    };
  }, {
    "./DOMObject": 1
  }],
  3: [function (e, t, n) {
    t.exports = function () {
      this.svg = document.createElementNS("http://www.w3.org/2000/svg", "svg"), this.svg.setAttributeNS(null, "class", "fslightbox-svg-icon"), this.svg.setAttributeNS(null, "viewBox", "0 0 15 15"), this.path = document.createElementNS("http://www.w3.org/2000/svg", "path"), this.path.setAttributeNS(null, "class", "fslightbox-svg-path"), this.getSVGIcon = function (e, t, n) {
        return this.path.setAttributeNS(null, "d", n), this.svg.setAttributeNS(null, "viewBox", e), this.svg.setAttributeNS(null, "width", t), this.svg.setAttributeNS(null, "height", t), this.svg.appendChild(this.path), this.svg;
      };
    };
  }, {}],
  4: [function (e, t, n) {
    var s = e("./DOMObject");

    t.exports = function (e) {
      this.toolbarElem = new s("div").addClassesAndCreate(["fslightbox-toolbar"]);
      var t = "M4.5 11H3v4h4v-1.5H4.5V11zM3 7h1.5V4.5H7V3H3v4zm10.5 6.5H11V15h4v-4h-1.5v2.5zM11 3v1.5h2.5V7H15V3h-4z";
      var n;
      var i = this;
      this.renderDefaultButtons = function () {
        var o = e.data.toolbarButtons;

        if (!0 === o.fullscreen) {
          var _o = new s("div").addClassesAndCreate(["fslightbox-toolbar-button", "fslightbox-flex-centered"]);

          n = new e.SVGIcon().getSVGIcon("0 0 17.5 17.5", "20px", t), _o.appendChild(n), _o.onclick = function () {
            e.data.fullscreen ? i.closeFullscreen() : i.openFullscreen();
          }, this.toolbarElem.appendChild(_o);
        }

        if (!0 === o.close) {
          var _t2 = new s("div").addClassesAndCreate(["fslightbox-toolbar-button", "fslightbox-flex-centered"]),
              _n = new e.SVGIcon().getSVGIcon("0 0 20 20", "16px", "M 11.469 10 l 7.08 -7.08 c 0.406 -0.406 0.406 -1.064 0 -1.469 c -0.406 -0.406 -1.063 -0.406 -1.469 0 L 10 8.53 l -7.081 -7.08 c -0.406 -0.406 -1.064 -0.406 -1.469 0 c -0.406 0.406 -0.406 1.063 0 1.469 L 8.531 10 L 1.45 17.081 c -0.406 0.406 -0.406 1.064 0 1.469 c 0.203 0.203 0.469 0.304 0.735 0.304 c 0.266 0 0.531 -0.101 0.735 -0.304 L 10 11.469 l 7.08 7.081 c 0.203 0.203 0.469 0.304 0.735 0.304 c 0.267 0 0.532 -0.101 0.735 -0.304 c 0.406 -0.406 0.406 -1.064 0 -1.469 L 11.469 10 Z");

          _t2.appendChild(_n), _t2.onclick = function () {
            e.data.fadingOut || e.hide();
          }, this.toolbarElem.appendChild(_t2);
        }
      }, this.openFullscreen = function () {
        e.data.fullscreen = !0, n.firstChild.setAttributeNS(null, "d", "M682 342h128v84h-212v-212h84v128zM598 810v-212h212v84h-128v128h-84zM342 342v-128h84v212h-212v-84h128zM214 682v-84h212v212h-84v-128h-128z"), n.setAttributeNS(null, "viewBox", "0 0 950 1024"), n.setAttributeNS(null, "width", "24px"), n.setAttributeNS(null, "height", "24px");
        var t = document.documentElement;
        t.requestFullscreen ? t.requestFullscreen() : t.mozRequestFullScreen ? t.mozRequestFullScreen() : t.webkitRequestFullscreen ? t.webkitRequestFullscreen() : t.msRequestFullscreen && t.msRequestFullscreen();
      }, this.closeFullscreen = function () {
        e.data.fullscreen = !1, n.firstChild.setAttributeNS(null, "d", t), n.setAttributeNS(null, "viewBox", "0 0 17.5 17.5"), n.setAttributeNS(null, "width", "20px"), n.setAttributeNS(null, "height", "20px"), document.exitFullscreen ? document.exitFullscreen() : document.mozCancelFullScreen ? document.mozCancelFullScreen() : document.webkitExitFullscreen ? document.webkitExitFullscreen() : document.msExitFullscreen && document.msExitFullscreen();
      }, this.renderToolbar = function (e) {
        this.renderDefaultButtons(), e.appendChild(this.toolbarElem);
      };
    };
  }, {
    "./DOMObject": 1
  }],
  5: [function (e, t, n) {
    var s = e("../Components/DOMObject");

    t.exports = function (e) {
      this.renderDOM = function () {
        e.element.id = "fslightbox-container", document.body.appendChild(e.element), i(e.element), e.data.totalSlides > 1 && r(e.element), e.element.appendChild(e.mediaHolder), e.element.appendChild(t()), e.data.isfirstTimeLoad = !0;
      };

      var t = function t() {
        return e.data.downEventDetector = new s("div").addClassesAndCreate(["fslightbox-down-event-detector", "fslightbox-full-dimension"]);
      },
          n = function n() {
        var t = new s("div").addClassesAndCreate(["fslightbox-slide-number-container", "fslightbox-flex-centered"]);
        e.data.slideCounterElem = document.createElement("div");
        var n = e.data.slideCounterElem;
        n.innerHTML = e.data.slide, n.id = "current_slide";
        var i = new s("div").addClassesAndCreate(["fslightbox-slash"]);
        i.innerHTML = "/";
        var o = document.createElement("div");
        o.innerHTML = e.data.totalSlides, t.appendChild(n), t.appendChild(i), t.appendChild(o), this.renderSlideCounter = function (n) {
          e.data.slideCounter && n.appendChild(t);
        };
      },
          i = function i(t) {
        if (e.data.nav = new s("div").addClassesAndCreate(["fslightbox-nav"]), e.toolbar.renderToolbar(e.data.nav), e.data.totalSlides > 1) {
          new n().renderSlideCounter(e.data.nav);
        }

        t.appendChild(e.data.nav);
      },
          o = function o(t, n, i) {
        var o = new s("div").addClassesAndCreate(["fslightbox-slide-btn", "fslightbox-flex-centered"]);
        o.appendChild(new e.SVGIcon().getSVGIcon("0 0 20 20", "22px", i)), t.appendChild(o), n.appendChild(t);
      },
          r = function r(t) {
        if (!1 === e.data.slideButtons) return !1;
        var n = new s("div").addClassesAndCreate(["fslightbox-slide-btn-container", "fslightbox-slide-btn-left-container"]);
        o(n, t, "M18.271,9.212H3.615l4.184-4.184c0.306-0.306,0.306-0.801,0-1.107c-0.306-0.306-0.801-0.306-1.107,0L1.21,9.403C1.194,9.417,1.174,9.421,1.158,9.437c-0.181,0.181-0.242,0.425-0.209,0.66c0.005,0.038,0.012,0.071,0.022,0.109c0.028,0.098,0.075,0.188,0.142,0.271c0.021,0.026,0.021,0.061,0.045,0.085c0.015,0.016,0.034,0.02,0.05,0.033l5.484,5.483c0.306,0.307,0.801,0.307,1.107,0c0.306-0.305,0.306-0.801,0-1.105l-4.184-4.185h14.656c0.436,0,0.788-0.353,0.788-0.788S18.707,9.212,18.271,9.212z"), n.onclick = function () {
          e.appendMethods.previousSlideViaButton(e.data.slide);
        };
        var i = new s("div").addClassesAndCreate(["fslightbox-slide-btn-container", "fslightbox-slide-btn-right-container"]);
        o(i, t, "M1.729,9.212h14.656l-4.184-4.184c-0.307-0.306-0.307-0.801,0-1.107c0.305-0.306,0.801-0.306,1.106,0l5.481,5.482c0.018,0.014,0.037,0.019,0.053,0.034c0.181,0.181,0.242,0.425,0.209,0.66c-0.004,0.038-0.012,0.071-0.021,0.109c-0.028,0.098-0.075,0.188-0.143,0.271c-0.021,0.026-0.021,0.061-0.045,0.085c-0.015,0.016-0.034,0.02-0.051,0.033l-5.483,5.483c-0.306,0.307-0.802,0.307-1.106,0c-0.307-0.305-0.307-0.801,0-1.105l4.184-4.185H1.729c-0.436,0-0.788-0.353-0.788-0.788S1.293,9.212,1.729,9.212z"), i.onclick = function () {
          e.appendMethods.nextSlideViaButton(e.data.slide);
        };
      };
    };
  }, {
    "../Components/DOMObject": 1
  }],
  6: [function (e, t, n) {
    t.exports = function (e) {
      this.handleKeyDown = function (t) {
        switch (t.code) {
          case "Escape":
            e.hide();
            break;

          case "ArrowLeft":
            e.data.totalSlides > 1 && e.appendMethods.previousSlideViaButton(e.data.slide);
            break;

          case "ArrowRight":
            e.data.totalSlides > 1 && e.appendMethods.nextSlideViaButton(e.data.slide);
        }
      };
    };
  }, {}],
  7: [function (e, t, n) {
    t.exports = function (e) {
      this.addRecompense = function () {
        if (!n()) return;
        document.documentElement.style.marginRight = e + "px";
        var s = t();
        if (s) for (var _t3 = 0; _t3 < s.length; _t3++) {
          s[_t3].style.marginRight = e + "px";
        }
      }, this.removeRecompense = function () {
        if (!n()) return;
        document.documentElement.style.marginRight = "";
        var e = t();
        if (e) for (var _t4 = 0; _t4 < e.length; _t4++) {
          e[_t4].style.marginRight = "";
        }
      };

      var t = function t() {
        return document.getElementsByClassName("recompense-for-scrollbar");
      },
          n = function n() {
        return !!e;
      };
    };
  }, {}],
  8: [function (e, t, n) {
    t.exports = function (e) {
      this.getWidth = function () {
        var t = document.createElement("div");
        t.style.visibility = "hidden", t.style.width = "100px", t.style.msOverflowStyle = "scrollbar", document.body.appendChild(t);
        var n = t.offsetWidth;
        t.style.overflow = "scroll";
        var s = document.createElement("div");
        s.style.width = "100%", t.appendChild(s);
        var i = s.offsetWidth;
        t.parentNode.removeChild(t), e.scrollbarWidth = n - i;
      };
    };
  }, {}],
  9: [function (e, t, n) {
    t.exports = function (t) {
      var n = new (e("../Components/DOMObject"))("div").addClassesAndCreate(["fslightbox-invisible-hover"]),
          s = {
        mediaHolder: t.mediaHolder,
        invisibleHover: n,
        downEventDetector: t.data.downEventDetector
      },
          i = t.data.sources,
          o = t.data.urls.length;
      var r,
          d,
          l,
          a = !1,
          c = !0;

      var u = function u(e) {
        "VIDEO" === e.target.tagName || e.touches || e.preventDefault(), e.target.classList.contains("fslightbox-source") && (d = !0), a = !0, l = 0, 1 !== t.data.totalSlides && (r = e.touches ? e.touches[0].clientX : e.clientX);
      },
          h = function h() {
        if (!a) return;
        if (a = !1, t.element.contains(n) && t.element.removeChild(n), t.element.classList.contains("fslightbox-cursor-grabbing") && t.element.classList.remove("fslightbox-cursor-grabbing"), 0 === l) return d || t.hide(), void (d = !1);
        if (d = !1, !c) return;
        c = !1;
        var e = t.stageSourceIndexes.all(t.data.slide);
        i[e.previous].classList.add("fslightbox-transform-transition"), i[e.current].classList.add("fslightbox-transform-transition"), i[e.next].classList.add("fslightbox-transform-transition"), l > 0 ? (1 === t.data.slide ? t.updateSlideNumber(t.data.totalSlides) : t.updateSlideNumber(t.data.slide - 1), o >= 2 ? (t.slideTransformer.plus(i[e.current]), t.slideTransformer.zero(i[e.previous])) : t.slideTransformer.zero(i[e.current]), e = t.stageSourceIndexes.all(t.data.slide), void 0 === t.data.sources[e.previous] && t.loadsources("previous", t.data.slide)) : l < 0 && (t.data.slide === t.data.totalSlides ? t.updateSlideNumber(1) : t.updateSlideNumber(t.data.slide + 1), o > 1 ? (t.slideTransformer.minus(i[e.current]), t.slideTransformer.zero(i[e.next])) : t.slideTransformer.zero(i[e.current]), e = t.stageSourceIndexes.all(t.data.slide), void 0 === t.data.sources[e.next] && t.loadsources("next", t.data.slide)), l = 0, t.stopVideos(), setTimeout(function () {
          i[e.previous].classList.remove("fslightbox-transform-transition"), i[e.current].classList.remove("fslightbox-transform-transition"), i[e.next].classList.remove("fslightbox-transform-transition"), c = !0;
        }, 250);
      },
          f = function f(e) {
        if (!a || !c) return;
        var s;
        if (s = e.touches ? e.touches[0].clientX : e.clientX, 0 !== (l = s - r) && 1 === t.data.totalSlides) return void (l = 1);
        t.element.classList.contains("fslightbox-cursor-grabbing") || t.element.classList.add("fslightbox-cursor-grabbing"), t.element.contains(n) || t.element.appendChild(n);
        var d = t.stageSourceIndexes.all(t.data.slide);
        o >= 3 && (i[d.previous].style.transform = "translate(" + (-t.data.slideDistance * window.innerWidth + l) + "px,0)"), o >= 1 && (i[d.current].style.transform = "translate(" + l + "px,0)"), o >= 2 && (i[d.next].style.transform = "translate(" + (t.data.slideDistance * window.innerWidth + l) + "px,0)");
      };

      for (var _e in s) {
        s[_e].addEventListener("mousedown", u), s[_e].addEventListener("touchstart", u, {
          passive: !0
        });
      }

      this.addWindowEvents = function () {
        window.addEventListener("mouseup", h), window.addEventListener("touchend", h), window.addEventListener("mousemove", f), window.addEventListener("touchmove", f, {
          passive: !0
        });
      }, this.removeWindowEvents = function () {
        window.removeEventListener("mouseup", h), window.removeEventListener("touchend", h), window.removeEventListener("mousemove", f), window.removeEventListener("touchmove", f);
      }, n.addEventListener("mouseup", h), n.addEventListener("touchend", h, {
        passive: !0
      }), t.data.nav.addEventListener("mousedown", function (e) {
        e.preventDefault();
      });
    };
  }, {
    "../Components/DOMObject": 1
  }],
  10: [function (e, t, n) {
    t.exports = function (e) {
      this.minus = function (t) {
        t.style.transform = "translate(" + -e * window.innerWidth + "px,0)";
      }, this.zero = function (e) {
        e.style.transform = "translate(0,0)";
      }, this.plus = function (t) {
        t.style.transform = "translate(" + e * window.innerWidth + "px,0)";
      };
    };
  }, {}],
  11: [function (e, t, n) {
    t.exports = function (e) {
      this.previous = function (t) {
        var n;
        var s = t - 1;
        return n = 0 === s ? e.totalSlides - 1 : s - 1;
      }, this.next = function (t) {
        var n;
        var s = t - 1;
        return n = t === e.totalSlides ? 0 : s + 1;
      }, this.all = function (t) {
        var n = t - 1,
            s = {
          previous: 0,
          current: 0,
          next: 0
        };
        return s.previous = 0 === n ? e.totalSlides - 1 : n - 1, s.current = n, t === e.totalSlides ? s.next = 0 : s.next = n + 1, s;
      };
    };
  }, {}],
  12: [function (e, t, n) {
    t.exports = function (e) {
      var t = e.keyboardController;
      this.attachListener = function () {
        document.addEventListener("keydown", t.handleKeyDown);
      }, this.removeListener = function () {
        document.removeEventListener("keydown", t.handleKeyDown);
      };
    };
  }, {}],
  13: [function (e, t, n) {
    t.exports = function (t) {
      var n = "fslightbox-transform-transition",
          s = "fslightbox-fade-out",
          i = function i(n) {
        var s = new (e("./Components/DOMObject"))("div").addClassesAndCreate(["fslightbox-source-holder", "fslightbox-full-dimension"]);
        return s.innerHTML = '<div class="fslightbox-loader"><div></div><div></div><div></div><div></div></div>', t.data.sources[n] = s, s;
      },
          o = function o(e) {
        e.firstChild.classList.add("fslightbox-fade-in");
      },
          r = function r(e) {
        var t = e.firstChild;
        t.classList.remove("fslightbox-fade-in"), t.classList.remove(s), t.offsetWidth;
      },
          d = function d(e) {
        e.firstChild.classList.add(s);
      };

      this.renderHolderInitial = function (e) {
        var n = t.stageSourceIndexes.all(e),
            s = t.data.totalSlides;

        if (s >= 3) {
          var _e2 = i(n.previous);

          t.slideTransformer.minus(_e2), t.mediaHolder.appendChild(_e2);
        }

        if (s >= 1) {
          var _e3 = i(n.current);

          t.mediaHolder.appendChild(_e3);
        }

        if (s >= 2) {
          var _e4 = i(n.next);

          t.slideTransformer.plus(_e4), t.mediaHolder.appendChild(_e4);
        }
      }, this.renderHolder = function (e, t) {
        switch (t) {
          case "previous":
            l(e);
            break;

          case "current":
            c(e);
            break;

          case "next":
            a(e);
        }
      };

      var l = function l(e) {
        var n = t.stageSourceIndexes.previous(e),
            s = i(n);
        t.slideTransformer.minus(s), t.mediaHolder.insertAdjacentElement("afterbegin", s);
      },
          a = function a(e) {
        var n = t.stageSourceIndexes.next(e),
            s = i(n);
        t.slideTransformer.plus(s), t.mediaHolder.appendChild(s);
      },
          c = function c(e) {
        var n = t.stageSourceIndexes.all(e),
            s = i(n.current);
        t.slideTransformer.zero(s), t.mediaHolder.insertBefore(s, t.data.sources[n.next]);
      };

      this.previousSlideViaButton = function (e) {
        1 === e ? t.data.slide = t.data.totalSlides : t.data.slide -= 1;
        var i = u();
        void 0 === t.data.sources[i.previous] && t.loadsources("previous", t.data.slide);
        var l = t.data.sources,
            a = l[i.current],
            c = l[i.next];
        c.classList.remove(n), a.classList.remove(n), l[i.previous].classList.remove(n), r(a), o(a), d(c), t.slideTransformer.zero(a), setTimeout(function () {
          i.next !== t.data.slide - 1 && t.slideTransformer.plus(c), c.firstChild.classList.remove(s);
        }, 220);
      }, this.nextSlideViaButton = function (e) {
        e === t.data.totalSlides ? t.data.slide = 1 : t.data.slide += 1;
        var i = u();
        void 0 === t.data.sources[i.next] && t.loadsources("next", t.data.slide);
        var l = t.data.sources,
            a = l[i.current],
            c = l[i.previous];
        c.classList.remove(n), a.classList.remove(n), l[i.next].classList.remove(n), r(a), o(a), d(c), t.slideTransformer.zero(a), setTimeout(function () {
          i.previous !== t.data.slide - 1 && t.slideTransformer.minus(c), c.firstChild.classList.remove(s);
        }, 220);
      };

      var u = function u() {
        return t.stopVideos(), t.updateSlideNumber(t.data.slide), t.stageSourceIndexes.all(t.data.slide);
      };
    };
  }, {
    "./Components/DOMObject": 1
  }],
  14: [function (e, t, n) {
    window.fsLightboxClass = function () {
      var t = e("./Components/DOMObject");
      this.data = {
        slide: 1,
        totalSlides: 1,
        slideDistance: 1.3,
        slideCounter: !0,
        slideButtons: !0,
        isFirstTimeLoad: !1,
        moveSlidesViaDrag: !0,
        toolbarButtons: {
          close: !0,
          fullscreen: !0
        },
        name: "",
        scrollbarWidth: 0,
        urls: [],
        sources: [],
        sourcesLoaded: [],
        rememberedSourcesDimensions: [],
        videos: [],
        videosPosters: [],
        holderWrapper: null,
        mediaHolder: null,
        nav: null,
        toolbar: null,
        slideCounterElem: null,
        downEventDetector: null,
        initiated: !1,
        fullscreen: !1,
        fadingOut: !1
      };
      var n = this;
      this.init = function (t) {
        if (this.data.initiated) return this.initSetSlide(t), void this.show();
        var n = this.data.name,
            i = [];
        var o = fsLightboxHelpers.a;

        for (var _e5 = 0; _e5 < o.length; _e5++) {
          if (o[_e5].hasAttribute("data-fslightbox") && o[_e5].getAttribute("data-fslightbox") === n) {
            var _t5 = i.push(o[_e5].getAttribute("href"));

            o[_e5].hasAttribute("data-video-poster") && (this.data.videosPosters[_t5 - 1] = o[_e5].getAttribute("data-video-poster"));
          }
        }

        this.data.urls = i, this.data.totalSlides = i.length, s.renderDOM(), document.documentElement.classList.add("fslightbox-open"), this.scrollbarRecompensor.addRecompense(), this.onResizeEvent.init(), this.eventsControllers.document.keyDown.attachListener(), this.throwEvent("init"), this.throwEvent("open"), this.slideSwiping = new (e("./Core/SlideSwiping.js"))(this), this.slideSwiping.addWindowEvents(), this.initSetSlide(t), this.data.initiated = !0, this.element.classList.add("fslightbox-open");
      }, this.initSetSlide = function (e) {
        switch (_typeof(e)) {
          case "string":
            this.setSlide(this.data.urls.indexOf(e) + 1);
            break;

          case "number":
            this.setSlide(e);
            break;

          case "undefined":
            this.setSlide(1);
        }
      }, this.show = function () {
        var e = this.element;
        this.scrollbarRecompensor.addRecompense(), e.classList.remove("fslightbox-fade-out-complete"), document.documentElement.classList.add("fslightbox-open"), e.offsetWidth, e.classList.add("fslightbox-fade-in-complete"), document.body.appendChild(e), this.onResizeEvent.addListener(), this.onResizeEvent.resizeListener(), this.eventsControllers.document.keyDown.attachListener(), this.slideSwiping.addWindowEvents(), this.throwEvent("show"), this.throwEvent("open");
      }, this.hide = function () {
        this.data.fullscreen && this.toolbar.closeFullscreen(), this.element.classList.add("fslightbox-fade-out-complete"), this.data.fadingOut = !0, this.throwEvent("close"), this.onResizeEvent.removeListener(), this.slideSwiping.removeWindowEvents(), this.eventsControllers.document.keyDown.removeListener(), setTimeout(function () {
          n.scrollbarRecompensor.removeRecompense(), document.documentElement.classList.remove("fslightbox-open"), n.data.fadingOut = !1, document.body.removeChild(n.element);
        }, 250);
      }, this.updateSlideNumber = function (e) {
        this.data.slide = e, this.data.totalSlides > 1 && (this.data.slideCounterElem.innerHTML = e);
      }, this.throwEvent = function (e) {
        var t;
        "function" == typeof Event ? t = new Event(e) : (t = document.createEvent("Event")).initEvent(e, !0, !0), this.element.dispatchEvent(t);
      }, this.element = new t("div").addClassesAndCreate(["fslightbox-container", "fslightbox-full-dimension"]), this.mediaHolder = new (e("./Components/MediaHolder"))();
      var s = new (e("./Core/DomRenderer"))(this);
      this.stageSourceIndexes = new (e("./Core/StageSourcesIndexes"))(this.data), this.keyboardController = new (e("./Core/KeyboardController"))(this), new (e("./Core/ScrollbarWidthGetter"))(this.data).getWidth(), this.onResizeEvent = new (e("./onResizeEvent"))(this), this.scrollbarRecompensor = new (e("./Core/ScrollbarRecompensor"))(this.data.scrollbarWidth), this.slideTransformer = new (e("./Core/SlideTransformer"))(this.data.slideDistance), this.slideSwiping = null, this.toolbar = new (e("./Components/Toolbar"))(this), this.SVGIcon = e("./Components/SVGIcon"), this.appendMethods = new (e("./appendMethods"))(this), this.eventsControllers = {
        document: {
          keyDown: new (e("./Core/events-controllers/DocumentKeyDownEventController"))(this)
        }
      }, this.loadsources = function (t, n) {
        return new (e("./loadSource.js"))(this, t, n);
      }, this.stopVideos = function () {
        var e = this.data.videos,
            t = this.data.sources;

        for (var _n2 in e) {
          !0 === e[_n2] ? void 0 !== t[_n2].firstChild.pause && t[_n2].firstChild.pause() : t[_n2].firstChild.contentWindow.postMessage('{"event":"command","func":"stopVideo","args":""}', "*");
        }
      }, this.setSlide = function (e) {
        this.data.slide = e, this.updateSlideNumber(e);
        var t = this.stageSourceIndexes.all(e),
            n = this.data.sources;
        0 === n.length ? this.loadsources("initial", e) : (void 0 === n[t.previous] && this.loadsources("previous", e), void 0 === n[t.current] && this.loadsources("current", e), void 0 === n[t.next] && this.loadsources("next", e));

        for (var _e6 in n) {
          n[_e6].classList.remove("fslightbox-transform-transition"), _e6 == t.previous && n.length > 1 ? this.slideTransformer.minus(n[t.previous]) : _e6 != t.current ? _e6 != t.next ? this.slideTransformer.minus(n[_e6]) : this.slideTransformer.plus(n[t.next]) : this.slideTransformer.zero(n[t.current]);
        }
      };
    }, function () {
      window.fsLightboxInstances = [], window.fsLightboxHelpers = {
        a: document.getElementsByTagName("a")
      };
      var e = window.fsLightboxHelpers.a;

      for (var _t6 = 0; _t6 < e.length; _t6++) {
        if (!e[_t6].hasAttribute("data-fslightbox")) continue;

        var _n3 = e[_t6].getAttribute("data-fslightbox");

        void 0 === window.fsLightboxInstances[_n3] && (window.fsLightbox = new window.fsLightboxClass(), window.fsLightbox.data.name = _n3, window.fsLightboxInstances[_n3] = window.fsLightbox), e[_t6].addEventListener("click", function (e) {
          e.preventDefault();
          var t = this.getAttribute("data-fslightbox");
          if (window.fsLightboxInstances[t].data.initiated) return window.fsLightboxInstances[t].setSlide(window.fsLightboxInstances[t].data.urls.indexOf(this.getAttribute("href")) + 1), void window.fsLightboxInstances[t].show();
          window.fsLightboxInstances[t].init(this.getAttribute("href"));
        });
      }
    }(document, window);
  }, {
    "./Components/DOMObject": 1,
    "./Components/MediaHolder": 2,
    "./Components/SVGIcon": 3,
    "./Components/Toolbar": 4,
    "./Core/DomRenderer": 5,
    "./Core/KeyboardController": 6,
    "./Core/ScrollbarRecompensor": 7,
    "./Core/ScrollbarWidthGetter": 8,
    "./Core/SlideSwiping.js": 9,
    "./Core/SlideTransformer": 10,
    "./Core/StageSourcesIndexes": 11,
    "./Core/events-controllers/DocumentKeyDownEventController": 12,
    "./appendMethods": 13,
    "./loadSource.js": 15,
    "./onResizeEvent": 16
  }],
  15: [function (e, t, n) {
    t.exports = function (t, n, s) {
      var i = e("./Components/DOMObject"),
          o = t.stageSourceIndexes.all(s),
          r = t.data.urls,
          d = t.data.sources;

      var l = function l(e, n, s, o) {
        var r = new i("div").addClassesAndCreate(["fslightbox-source-holder"]);
        t.data.rememberedSourcesDimensions[o] = {
          width: n,
          height: s
        }, r.appendChild(e), function (e, t) {
          e.innerHTML = "", e.appendChild(t), e.firstChild.offsetWidth;
        }(d[o], e), t.onResizeEvent.scaleSource(o);
      };

      var a = function a(e) {
        var t = new i("div").addClassesAndCreate(["fslightbox-invalid-file-wrapper", "fslightbox-flex-centered"]);
        t.innerHTML = "Invalid file", l(t, window.innerWidth, window.innerHeight, e);
      };

      if (this.createSourceElem = function (e) {
        var n = document.createElement("a"),
            s = t.data.urls[e];
        if (n.href = s, "www.youtube.com" === n.hostname) t.data.videos[e] = !1, function (e, n) {
          var s = new i("iframe").addClassesAndCreate(["fslightbox-source"]);
          s.src = "//www.youtube.com/embed/" + e + "?enablejsapi=1", s.setAttribute("allowfullscreen", ""), s.setAttribute("frameborder", "0"), t.mediaHolder.appendChild(s), l(s, 1920, 1080, n);
        }(function (e) {
          var t = e.match(/^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=)([^#\&\?]*).*/);
          return t && 11 == t[2].length ? t[2] : "error";
        }(s), e);else {
          var _n4 = new XMLHttpRequest();

          _n4.onreadystatechange = function () {
            if (2 === _n4.readyState) {
              if (200 === _n4.status || 206 === _n4.status) {
                var _s = _n4.getResponseHeader("content-type"),
                    _o2 = _s.slice(0, _s.indexOf("/"));

                "image" === _o2 ? function (e, t) {
                  var n = new i("img").addClassesAndCreate(["fslightbox-source"]);
                  n.src = e, n.addEventListener("load", function () {
                    l(n, this.width, this.height, t);
                  });
                }(r[e], e) : "video" === _o2 ? (!function (e, n, s) {
                  var o,
                      r,
                      d = !1,
                      a = new i("video").addClassesAndCreate(["fslightbox-source"]),
                      c = new i("source").elem;
                  t.data.videosPosters[n] && (a.poster = t.data.videosPosters[n], a.style.objectFit = "cover"), c.src = e, c.type = s, a.appendChild(c), a.onloadedmetadata = function () {
                    d || (this.videoWidth && 0 !== this.videoWidth ? (o = this.videoWidth, r = this.videoHeight) : (o = 1920, r = 1080), d = !0, l(a, o, r, n));
                  };
                  var u = 0,
                      h = setInterval(function () {
                    if (d) clearInterval(h);else {
                      if (a.videoWidth && 0 !== a.videoWidth) o = a.videoWidth, r = a.videoHeight;else {
                        if (u < 31) return void u++;
                        o = 1920, r = 1080;
                      }
                      d = !0, l(a, o, r, n), clearInterval(h);
                    }
                  }, 100);
                  a.setAttribute("controls", "");
                }(r[e], e, _s), t.data.videos[e] = !0) : a(e);
              } else a(e);

              _n4.abort();
            }
          }, _n4.open("get", s, !0), _n4.send(null);
        }
      }, "initial" === n) t.appendMethods.renderHolderInitial(s, i), r.length >= 1 && this.createSourceElem(o.current), r.length >= 2 && this.createSourceElem(o.next), r.length >= 3 && this.createSourceElem(o.previous);else switch (t.appendMethods.renderHolder(s, n), n) {
        case "previous":
          this.createSourceElem(o.previous);
          break;

        case "current":
          this.createSourceElem(o.current);
          break;

        case "next":
          this.createSourceElem(o.next);
      }
    };
  }, {
    "./Components/DOMObject": 1
  }],
  16: [function (e, t, n) {
    t.exports = function (e) {
      var t = this,
          n = e.data.sources,
          s = e.data.rememberedSourcesDimensions;
      this.mediaHolderDimensions = function () {
        var t = e.mediaHolder.style,
            n = window.innerWidth,
            s = window.innerHeight;
        n > 1e3 ? (t.width = n - .1 * n + "px", t.height = s - .1 * s + "px") : (t.width = n + "px", t.height = s - .1 * s + "px");
      }, this.scaleAndTransformSources = function () {
        var t = e.data.urls.length,
            s = e.stageSourceIndexes.all(e.data.slide);
        t > 0 && e.slideTransformer.zero(n[s.current]), t > 1 && e.slideTransformer.plus(n[s.next]), t > 2 && e.slideTransformer.minus(n[s.previous]);

        for (var i = 0; i < t; i++) {
          this.scaleSource(i), i !== s.current && i !== s.next && i !== s.previous && n[i] && e.slideTransformer.plus(n[i]);
        }
      }, this.scaleSource = function (t) {
        if (!n[t]) return;
        var i = n[t].firstChild;
        var o = s[t].width,
            r = s[t].height;
        var d = o / r,
            l = parseInt(e.mediaHolder.style.width),
            a = parseInt(e.mediaHolder.style.height);
        var c = l / d;

        var u = function u() {
          i.style.height = c + "px", i.style.width = c * d + "px";
        };

        if (c < a) return o < l && (c = r), void u();
        c = r > a ? a : r, u();
      }, this.init = function () {
        this.mediaHolderDimensions(), this.addListener();
      }, this.addListener = function () {
        window.addEventListener("resize", this.resizeListener);
      }, this.resizeListener = function () {
        t.mediaHolderDimensions(), t.scaleAndTransformSources();
      }, this.removeListener = function () {
        window.removeEventListener("resize", this.resizeListener);
      };
    };
  }, {}]
}, {}, [14]);
"use strict";

/*!
  * Stickyfill – `position: sticky` polyfill
  * v. 2.1.0 | https://github.com/wilddeer/stickyfill
  * MIT License
  */
!function (a, b) {
  "use strict";

  function c(a, b) {
    if (!(a instanceof b)) throw new TypeError("Cannot call a class as a function");
  }

  function d(a, b) {
    for (var c in b) {
      b.hasOwnProperty(c) && (a[c] = b[c]);
    }
  }

  function e(a) {
    return parseFloat(a) || 0;
  }

  function f(a) {
    for (var b = 0; a;) {
      b += a.offsetTop, a = a.offsetParent;
    }

    return b;
  }

  function g() {
    function c() {
      a.pageXOffset != m.left ? (m.top = a.pageYOffset, m.left = a.pageXOffset, p.refreshAll()) : a.pageYOffset != m.top && (m.top = a.pageYOffset, m.left = a.pageXOffset, n.forEach(function (a) {
        return a._recalcPosition();
      }));
    }

    function d() {
      f = setInterval(function () {
        n.forEach(function (a) {
          return a._fastCheck();
        });
      }, 500);
    }

    function e() {
      clearInterval(f);
    }

    if (!k) {
      k = !0, c(), a.addEventListener("scroll", c), a.addEventListener("resize", p.refreshAll), a.addEventListener("orientationchange", p.refreshAll);
      var f = void 0,
          g = void 0,
          h = void 0;
      "hidden" in b ? (g = "hidden", h = "visibilitychange") : "webkitHidden" in b && (g = "webkitHidden", h = "webkitvisibilitychange"), h ? (b[g] || d(), b.addEventListener(h, function () {
        b[g] ? e() : d();
      })) : d();
    }
  }

  var h = function () {
    function a(a, b) {
      for (var c = 0; c < b.length; c++) {
        var d = b[c];
        d.enumerable = d.enumerable || !1, d.configurable = !0, "value" in d && (d.writable = !0), Object.defineProperty(a, d.key, d);
      }
    }

    return function (b, c, d) {
      return c && a(b.prototype, c), d && a(b, d), b;
    };
  }(),
      i = !1,
      j = "undefined" != typeof a;

  j && a.getComputedStyle ? !function () {
    var a = b.createElement("div");
    ["", "-webkit-", "-moz-", "-ms-"].some(function (b) {
      try {
        a.style.position = b + "sticky";
      } catch (a) {}

      return "" != a.style.position;
    }) && (i = !0);
  }() : i = !0;

  var k = !1,
      l = "undefined" != typeof ShadowRoot,
      m = {
    top: null,
    left: null
  },
      n = [],
      o = function () {
    function g(a) {
      if (c(this, g), !(a instanceof HTMLElement)) throw new Error("First argument must be HTMLElement");
      if (n.some(function (b) {
        return b._node === a;
      })) throw new Error("Stickyfill is already applied to this node");
      this._node = a, this._stickyMode = null, this._active = !1, n.push(this), this.refresh();
    }

    return h(g, [{
      key: "refresh",
      value: function value() {
        if (!i && !this._removed) {
          this._active && this._deactivate();
          var c = this._node,
              g = getComputedStyle(c),
              h = {
            position: g.position,
            top: g.top,
            display: g.display,
            marginTop: g.marginTop,
            marginBottom: g.marginBottom,
            marginLeft: g.marginLeft,
            marginRight: g.marginRight,
            cssFloat: g.cssFloat
          };

          if (!isNaN(parseFloat(h.top)) && "table-cell" != h.display && "none" != h.display) {
            this._active = !0;
            var j = c.style.position;
            "sticky" != g.position && "-webkit-sticky" != g.position || (c.style.position = "static");
            var k = c.parentNode,
                m = l && k instanceof ShadowRoot ? k.host : k,
                n = c.getBoundingClientRect(),
                o = m.getBoundingClientRect(),
                p = getComputedStyle(m);
            this._parent = {
              node: m,
              styles: {
                position: m.style.position
              },
              offsetHeight: m.offsetHeight
            }, this._offsetToWindow = {
              left: n.left,
              right: b.documentElement.clientWidth - n.right
            }, this._offsetToParent = {
              top: n.top - o.top - e(p.borderTopWidth),
              left: n.left - o.left - e(p.borderLeftWidth),
              right: -n.right + o.right - e(p.borderRightWidth)
            }, this._styles = {
              position: j,
              top: c.style.top,
              bottom: c.style.bottom,
              left: c.style.left,
              right: c.style.right,
              width: c.style.width,
              marginTop: c.style.marginTop,
              marginLeft: c.style.marginLeft,
              marginRight: c.style.marginRight
            };
            var q = e(h.top);
            this._limits = {
              start: n.top + a.pageYOffset - q,
              end: o.top + a.pageYOffset + m.offsetHeight - e(p.borderBottomWidth) - c.offsetHeight - q - e(h.marginBottom)
            };
            var r = p.position;
            "absolute" != r && "relative" != r && (m.style.position = "relative"), this._recalcPosition();
            var s = this._clone = {};
            s.node = b.createElement("div"), d(s.node.style, {
              width: n.right - n.left + "px",
              height: n.bottom - n.top + "px",
              marginTop: h.marginTop,
              marginBottom: h.marginBottom,
              marginLeft: h.marginLeft,
              marginRight: h.marginRight,
              cssFloat: h.cssFloat,
              padding: 0,
              border: 0,
              borderSpacing: 0,
              fontSize: "1em",
              position: "static"
            }), k.insertBefore(s.node, c), s.docOffsetTop = f(s.node);
          }
        }
      }
    }, {
      key: "_recalcPosition",
      value: function value() {
        if (this._active && !this._removed) {
          var a = m.top <= this._limits.start ? "start" : m.top >= this._limits.end ? "end" : "middle";

          if (this._stickyMode != a) {
            switch (a) {
              case "start":
                d(this._node.style, {
                  position: "absolute",
                  left: this._offsetToParent.left + "px",
                  right: this._offsetToParent.right + "px",
                  top: this._offsetToParent.top + "px",
                  bottom: "auto",
                  width: "auto",
                  marginLeft: 0,
                  marginRight: 0,
                  marginTop: 0
                });
                break;

              case "middle":
                d(this._node.style, {
                  position: "fixed",
                  left: this._offsetToWindow.left + "px",
                  right: this._offsetToWindow.right + "px",
                  top: this._styles.top,
                  bottom: "auto",
                  width: "auto",
                  marginLeft: 0,
                  marginRight: 0,
                  marginTop: 0
                });
                break;

              case "end":
                d(this._node.style, {
                  position: "absolute",
                  left: this._offsetToParent.left + "px",
                  right: this._offsetToParent.right + "px",
                  top: "auto",
                  bottom: 0,
                  width: "auto",
                  marginLeft: 0,
                  marginRight: 0
                });
            }

            this._stickyMode = a;
          }
        }
      }
    }, {
      key: "_fastCheck",
      value: function value() {
        this._active && !this._removed && (Math.abs(f(this._clone.node) - this._clone.docOffsetTop) > 1 || Math.abs(this._parent.node.offsetHeight - this._parent.offsetHeight) > 1) && this.refresh();
      }
    }, {
      key: "_deactivate",
      value: function value() {
        var a = this;
        this._active && !this._removed && (this._clone.node.parentNode.removeChild(this._clone.node), delete this._clone, d(this._node.style, this._styles), delete this._styles, n.some(function (b) {
          return b !== a && b._parent && b._parent.node === a._parent.node;
        }) || d(this._parent.node.style, this._parent.styles), delete this._parent, this._stickyMode = null, this._active = !1, delete this._offsetToWindow, delete this._offsetToParent, delete this._limits);
      }
    }, {
      key: "remove",
      value: function value() {
        var a = this;
        this._deactivate(), n.some(function (b, c) {
          if (b._node === a._node) return n.splice(c, 1), !0;
        }), this._removed = !0;
      }
    }]), g;
  }(),
      p = {
    stickies: n,
    Sticky: o,
    forceSticky: function forceSticky() {
      i = !1, g(), this.refreshAll();
    },
    addOne: function addOne(a) {
      if (!(a instanceof HTMLElement)) {
        if (!a.length || !a[0]) return;
        a = a[0];
      }

      for (var b = 0; b < n.length; b++) {
        if (n[b]._node === a) return n[b];
      }

      return new o(a);
    },
    add: function add(a) {
      if (a instanceof HTMLElement && (a = [a]), a.length) {
        for (var b = [], c = function c(_c) {
          var d = a[_c];
          return d instanceof HTMLElement ? n.some(function (a) {
            if (a._node === d) return b.push(a), !0;
          }) ? "continue" : void b.push(new o(d)) : (b.push(void 0), "continue");
        }, d = 0; d < a.length; d++) {
          c(d);
        }

        return b;
      }
    },
    refreshAll: function refreshAll() {
      n.forEach(function (a) {
        return a.refresh();
      });
    },
    removeOne: function removeOne(a) {
      if (!(a instanceof HTMLElement)) {
        if (!a.length || !a[0]) return;
        a = a[0];
      }

      n.some(function (b) {
        if (b._node === a) return b.remove(), !0;
      });
    },
    remove: function remove(a) {
      if (a instanceof HTMLElement && (a = [a]), a.length) for (var b = function b(_b) {
        var c = a[_b];
        n.some(function (a) {
          if (a._node === c) return a.remove(), !0;
        });
      }, c = 0; c < a.length; c++) {
        b(c);
      }
    },
    removeAll: function removeAll() {
      for (; n.length;) {
        n[0].remove();
      }
    }
  };

  i || g(), "undefined" != typeof module && module.exports ? module.exports = p : j && (a.Stickyfill = p);
}(window, document);
"use strict";

(function (d, window) {
  if (dev) {
    console.log('INIT functions');
  }

  var closest = function closest() {
    if (!Element.prototype.closest) {
      Element.prototype.closest = function (css) {
        var node = this;

        while (node) {
          if (node.matches(css)) {
            return node;
          } else {
            node = node.parentElement;
          }
        }

        return null;
      };
    }
  };

  closest();

  window.getCoords = function (el) {
    var box = el.getBoundingClientRect();
    return {
      top: box.top + pageYOffset,
      left: box.left + pageXOffset
    };
  };

  window.debounce = function (f, ms) {
    var timer = null;
    return function () {
      var _this = this;

      for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
        args[_key] = arguments[_key];
      }

      var onComplete = function onComplete() {
        f.apply(_this, args);
        timer = null;
      };

      if (timer) {
        clearTimeout(timer);
      }

      timer = setTimeout(onComplete, ms);
    };
  };

  window.throttle = function (f, ms) {
    var lastCall = 0;
    return function () {
      var now = new Date().getTime();

      if (now - lastCall < ms) {
        return;
      }

      lastCall = now;
      return f.apply(void 0, arguments);
    };
  };

  window.isTouchDevice = function () {
    var prefixes = ' -webkit- -moz- -o- -ms- '.split(' ');

    var mq = function mq(query) {
      return window.matchMedia(query).matches;
    };

    if ('ontouchstart' in window || window.DocumentTouch && document instanceof DocumentTouch) {
      return true;
    }

    var query = ['(', prefixes.join('touch-enabled),('), 'heartz', ')'].join('');
    return mq(query);
  };
})(document, window);
"use strict";

/* Сейчас мне нужна только одна переменная, но возможно потом нужно будет еще что-то))
*
* Нижнее подчеркивание в названии нужно для того, чтобы файл подключался первым и переменна была видна в других файлах, т.к. файлы подключаются автоматом, порядок явно не задается.
* */
// если true, то будет выводиться инфа в console.log
var dev = false;
"use strict";

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

// Add data-text-overflow with count of lines for clipping a rest.
(function (d) {
  if (dev) {
    console.log('INIT text-overflow');
  }

  var TextOverflow =
  /*#__PURE__*/
  function () {
    function TextOverflow(el) {
      _classCallCheck(this, TextOverflow);

      this.el = el;
      this.state = {
        lines: null
      };
      this.initValue();
      this.clipText();
    }

    _createClass(TextOverflow, [{
      key: "initValue",
      value: function initValue() {
        var lines = this.el.getAttribute('data-text-overflow');
        this.changeState('set', lines);
      }
    }, {
      key: "clipText",
      value: function clipText() {
        var css = getComputedStyle(this.el);
        var lh = Number(css.lineHeight.slice(0, -2));
        var height = Math.floor(lh * this.state.lines);
        var width = css.width;
        var ghost = this.el.cloneNode(true);
        ghost.style.width = width;
        ghost.style.position = 'absolute';
        ghost.style.visibility = 'hidden';
        this.el.parentElement.appendChild(ghost);
        var text = ghost.innerHTML;

        while (text.length > 0 && ghost.offsetHeight > height) {
          text = text.substr(0, text.length - 1);
          ghost.innerHTML = text + '...';
        }

        var newText = ghost.innerHTML;
        ghost.remove();
        this.el.innerHTML = newText;
      } // actions -------------------

    }, {
      key: "changeState",
      value: function changeState(action, payload) {
        switch (action) {
          case 'set':
            this.actionSet(payload);
            break;
        }
      }
    }, {
      key: "actionSet",
      value: function actionSet(value) {
        if (dev) {
          console.log('f: actionSet');
        }

        value = Number(value);

        if (Number.isNaN(value) || value <= 0 || parseInt(value) !== value) {
          console.error('Значение в data-text-overflow должно быть целым числом больше 0.');
        }

        this.state.lines = value;
      }
    }]);

    return TextOverflow;
  }();

  var els = d.querySelectorAll('[data-text-overflow]');

  try {
    els.forEach(function (el) {
      return new TextOverflow(el);
    });
  } catch (e) {}
})(document);
"use strict";

(function (d) {
  var dev = true;
  if (!d.querySelector('.aboutPage')) return;

  if (dev) {
    console.log('INIT about-page');
  }

  var items = d.querySelectorAll('.aboutPage__body');

  try {
    items.forEach(function (i) {
      return Stickyfill.add(i);
    });
    Stickyfill.forceSticky();
  } catch (e) {
    console.log(e);
  }
})(document);
"use strict";

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

(function (d) {
  // также для страницы События
  var dev = true; // if (!d.querySelector('.frontTabsContentApplicants__item')
  // || !d.querySelector('.applicants__card')) return;

  if (dev) {
    console.log('INIT applicants-preview');
  }

  var ApplicantsPreview =
  /*#__PURE__*/
  function () {
    function ApplicantsPreview() {
      _classCallCheck(this, ApplicantsPreview);

      this.html = '<div class="applicantsPreview">\n' + '  <div class="applicantsPreview__header">\n' + '    <div class="applicantsPreview__top"><time class="applicantsPreview__date"></time>\n' + '      <div class="applicantsPreview__source"></div>\n' + '    </div>\n' + '    <h3 class="applicantsPreview__title"></h3>\n' + '  </div>\n' + '  <div class="applicantsPreview__body"></div>\n' + '  <div class="applicantsPreview__footer"><a class="button applicantsPreview__button" target="_blank"><span>Читать материал</span></a></div><button class="applicantsPreview__close"><span class="visually-hidden">Закрыть</span></button>\n' + '</div>';
      this.refs = {
        items: d.querySelectorAll('.frontTabsContentApplicants__card a, .applicants__card'),
        parent: d.querySelector('.page'),
        close: null,
        popup: null,
        title: null,
        date: null,
        source: null,
        body: null,
        button: null
      };
      this.state = {
        url: null,
        title: null,
        source: null,
        date: null,
        dateFormat: null,
        body: null,
        lastClicked: null,
        active: false
      };
      this.bodyClickHanlder = this.bodyClickHanlder.bind(this);
      this.clickHandler = this.clickHandler.bind(this);
      this.hide = this.hide.bind(this);
      this.init();
    }

    _createClass(ApplicantsPreview, [{
      key: "init",
      value: function init() {
        if (dev) {
          console.log('init ApplicantsPreview');
        }

        this.createPopup();
        this.addListeners();
      }
    }, {
      key: "addListeners",
      value: function addListeners() {
        var _this = this;

        this.refs.items.forEach(function (i) {
          i.addEventListener('click', _this.clickHandler);
        });
        this.refs.close.addEventListener('click', this.hide);
      }
    }, {
      key: "clickHandler",
      value: function clickHandler(e) {
        if (dev) {
          console.log('f: clickHandler');
        }

        var el = e.currentTarget.closest('.frontTabsContentApplicants__card') || e.currentTarget;

        if (el === this.state.lastClicked) {
          return;
        }

        var data = {};

        if (el === e.currentTarget) {
          data = {
            url: el.getAttribute('href'),
            title: el.querySelector('.applicants__title').innerHTML,
            source: el.querySelector('.applicants__source').innerHTML,
            date: el.querySelector('.applicants__date').innerHTML,
            dateFormat: el.querySelector('.applicants__date').getAttribute('datetime'),
            body: el.getAttribute('data-body'),
            clickedEl: el
          };
        } else {
          data = {
            url: e.currentTarget.getAttribute('href'),
            title: el.querySelector('.frontTabsContentApplicants__title').innerHTML,
            source: el.querySelector('.frontTabsContentApplicants__source').innerHTML,
            date: el.querySelector('.frontTabsContentApplicants__date').innerHTML,
            dateFormat: el.querySelector('.frontTabsContentApplicants__date').getAttribute('datetime'),
            body: el.getAttribute('data-body'),
            clickedEl: el
          };
        }

        this.actionSetParams(data);
        e.preventDefault();
      }
    }, {
      key: "bodyClickHanlder",
      value: function bodyClickHanlder(e) {
        if (dev) {
          console.log('f: bodyClickHanlder');
        }

        var el = e.target;

        if (dev) {
          console.log(el);
          console.log(el.closest('.frontTabsContentApplicants'));
          console.log(el.closest('.applicants__item'));
          console.log(!(el.closest('.frontTabsContentApplicants') || el.closest('.applicantsPreview')) || !el.closest('.applicants__item'));
        }

        if (!(el.closest('.frontTabsContentApplicants') || el.closest('.applicantsPreview')) && !el.closest('.applicants__item')) {
          this.hide();
        }
      }
    }, {
      key: "createPopup",
      value: function createPopup() {
        if (dev) {
          console.log('f: createPopup');
        }

        var el = d.createElement('div');
        el.innerHTML = this.html;
        el = el.firstChild;
        this.refs.popup = el;
        this.refs.title = el.querySelector('.applicantsPreview__title');
        this.refs.date = el.querySelector('.applicantsPreview__date');
        this.refs.source = el.querySelector('.applicantsPreview__source');
        this.refs.body = el.querySelector('.applicantsPreview__body');
        this.refs.button = el.querySelector('.applicantsPreview__button');
        this.refs.close = el.querySelector('.applicantsPreview__close');
        this.refs.parent.appendChild(this.refs.popup);
      }
    }, {
      key: "updateView",
      value: function updateView() {
        if (dev) {
          console.log('f: updateView');
          console.log(this.state);
        }

        if (this.state.active) {
          this.show();
          this.refs.title.innerHTML = "<a href=\"".concat(this.state.url, "\" target=\"_blank\">").concat(this.state.title, "</a>");
          this.refs.date.innerHTML = this.state.date;
          this.refs.date.setAttribute('datetime', this.state.dateFormat);
          this.refs.source.innerHTML = this.state.source;
          this.refs.body.innerHTML = this.state.body;
          this.refs.button.setAttribute('href', this.state.url);
        } else {
          this.hide();
        }
      }
    }, {
      key: "show",
      value: function show() {
        if (dev) {
          console.log('f: show');
        }

        this.refs.popup.classList.add('applicantsPreview--active');
        d.body.addEventListener('click', this.bodyClickHanlder);
      }
    }, {
      key: "hide",
      value: function hide() {
        if (dev) {
          console.log('f: hide');
        }

        this.refs.popup.classList.remove('applicantsPreview--active');
        this.state.lastClicked = null;
        d.body.removeEventListener('click', this.bodyClickHanlder);
      } // actions

    }, {
      key: "actionSetParams",
      value: function actionSetParams(params) {
        this.state.url = params.url;
        this.state.title = params.title;
        this.state.source = params.source;
        this.state.date = params.date;
        this.state.dateFormat = params.dateFormat;
        this.state.body = params.body;
        this.state.lastClicked = params.clickedEl;
        this.state.active = true;
        this.updateView();
      }
    }]);

    return ApplicantsPreview;
  }();

  try {
    new ApplicantsPreview();
  } catch (e) {}
})(document);
"use strict";

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

(function (d) {
  // const dev = true;
  if (!d.querySelector('.calender')) return;

  if (dev) {
    console.log('INIT calender');
  }

  var CalenderFull =
  /*#__PURE__*/
  function () {
    function CalenderFull(el) {
      _classCallCheck(this, CalenderFull);

      this.refs = {
        el: el,
        month: el.querySelectorAll('.calender__month'),
        allForY: el.querySelector('.calender__all'),
        pagerCurrent: el.querySelector('.calender__currentY'),
        prevY: el.querySelector('.calender__arrow--prev'),
        nextY: el.querySelector('.calender__arrow--next')
      };
      this.state = {
        curM: null,
        curY: null,
        curS: null,
        pager: null,
        minDate: new Date('2016-09'),
        baseUrl: location.pathname
      };
      this.today = new Date();
      this.init();
    }

    _createClass(CalenderFull, [{
      key: "init",
      value: function init() {
        var q = new URLSearchParams(location.search);
        var now = this.today;
        this.state.curY = +q.get('y');
        this.state.curM = +q.get('m');
        this.state.curS = q.get('sort');

        if (!this.state.curY && !this.state.curM) {
          this.state.curY = now.getFullYear();
          this.state.curM = now.getMonth() + 1;
        }

        this.state.pager = this.state.curY;
        this.addListeners();
        this.updateMonth();
        this.updatePager();

        if (dev) {
          console.log(this.state);
        }
      }
    }, {
      key: "addListeners",
      value: function addListeners() {
        var _this = this;

        this.refs.prevY.addEventListener('click', function () {
          return _this.actionPager(-1);
        });
        this.refs.nextY.addEventListener('click', function () {
          return _this.actionPager(1);
        });
      }
    }, {
      key: "updateMonth",
      value: function updateMonth() {
        var _this2 = this;

        // clean classes
        this.refs.month.forEach(function (m) {
          m.classList.remove('calender__month--dis');
          m.classList.remove('calender__month--active');
        });
        this.refs.allForY.classList.remove('calender__all--active'); // set url and classes

        var y = this.state.pager;
        var m = this.state.curM;
        var s = this.state.curS;
        var path = this.state.baseUrl;
        var curY = this.state.curY;
        var q = new URLSearchParams();
        q.append('y', y);

        if (s) {
          q.append('y', y);
        }

        this.refs.allForY.href = "".concat(path, "?").concat(q);
        this.refs.month.forEach(function (month, index) {
          var curM = index + 1;
          q.set('m', curM);
          month.href = "".concat(path, "?").concat(q);
          var now = _this2.today;

          if (y === curY && m === curM) {
            month.classList.add('calender__month--active');
          }

          var minDate = _this2.state.minDate;
          var maxY = now.getFullYear();
          var minY = minDate.getFullYear();

          if (y === maxY && curM > Number(now.getMonth() + 1) || y > maxY || y === minY && curM < Number(minDate.getMonth() + 1) || y < minY) {
            month.classList.add('calender__month--dis');
          }
        });

        if (!m) {
          this.refs.allForY.classList.add('calender__all--active');
        }
      }
    }, {
      key: "updatePager",
      value: function updatePager() {
        var pagerY = this.state.pager;
        this.refs.pagerCurrent.innerHTML = pagerY;
        var now = this.today;

        if (pagerY >= now.getFullYear()) {
          this.refs.nextY.classList.add('calender__arrow--dis');
        } else {
          this.refs.nextY.classList.remove('calender__arrow--dis');
        }

        if (pagerY <= this.state.minDate.getFullYear()) {
          this.refs.prevY.classList.add('calender__arrow--dis');
        } else {
          this.refs.prevY.classList.remove('calender__arrow--dis');
        }
      } // actions

    }, {
      key: "actionPager",
      value: function actionPager(shift) {
        if (dev) {
          console.log('f: actionPager');
        }

        this.state.pager += shift;
        this.updatePager();
        this.updateMonth();
      }
    }]);

    return CalenderFull;
  }();

  var CalenderYears =
  /*#__PURE__*/
  function () {
    function CalenderYears(el) {
      _classCallCheck(this, CalenderYears);

      this.refs = {
        el: el,
        list: el.querySelector('.calender__list')
      };
      this.state = {
        curY: null,
        minYear: 2016,
        baseUrl: location.pathname
      };
      this.today = new Date();
      this.init();
    }

    _createClass(CalenderYears, [{
      key: "init",
      value: function init() {
        var q = new URLSearchParams(location.search);
        var now = this.today;
        this.state.curY = +q.get('y') || now.getFullYear();
        this.createYears();

        if (dev) {
          console.log(this.state);
        }
      }
    }, {
      key: "createYears",
      value: function createYears() {
        if (dev) {
          console.log('f: createYears');
        }

        var html = '';
        var now = this.today;
        var minY = this.state.minYear;
        var maxY = now.getFullYear();
        var curY = this.state.curY;
        var i = minY;

        while (i <= maxY) {
          var url = "".concat(this.state.baseUrl, "?y=").concat(i);
          var classes = 'calender__month';

          if (i === curY) {
            classes += ' calender__month--active';
          }

          html = html + "<a href=\"".concat(url, "\" class=\"").concat(classes, "\">").concat(i, "</a>");
          i++;
        }

        this.refs.list.innerHTML = html;
      }
    }]);

    return CalenderYears;
  }();

  try {
    new CalenderFull(d.querySelector('.calender:not(.calender--years)'));
  } catch (e) {
    if (dev) {
      console.log(e);
    }
  }

  try {
    new CalenderYears(d.querySelector('.calender--years'));
  } catch (e) {
    if (dev) {
      console.log(e);
    }
  }
})(document);
"use strict";

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

(function (d) {
  // const dev = true;
  if (!d.querySelector('.dropdown')) return;

  if (dev) {
    console.log('INIT dropdown');
  }

  var Dropdown =
  /*#__PURE__*/
  function () {
    function Dropdown(el) {
      _classCallCheck(this, Dropdown);

      this.refs = {
        cnt: el,
        title: el.querySelector('.dropdown__title'),
        main: el.querySelector('.dropdown__items')
      };
      this.state = {
        active: false,
        offset: 0
      };
      this.init = this.init.bind(this);
      window.addEventListener('load', this.init);
    }

    _createClass(Dropdown, [{
      key: "init",
      value: function init() {
        this.refs.title.addEventListener('click', this.toggleState.bind(this));
        this.state.offset = this.refs.main.offsetHeight;
        this.refs.main.style.position = 'relative';
        this.toggleOffset();
      }
    }, {
      key: "toggleState",
      value: function toggleState() {
        this.state.active = !this.state.active;
        this.toggleClass();
        this.toggleOffset();
      }
    }, {
      key: "toggleOffset",
      value: function toggleOffset() {
        if (this.state.active) {
          this.refs.main.style.cssText = "transition-duration: 0.3s; position: relative; margin-top: 0;";
        } else {
          this.refs.main.style.cssText = "transition-duration: 0.3s; position: relative; margin-top: -".concat(this.state.offset, "px;");
        }
      }
    }, {
      key: "toggleClass",
      value: function toggleClass() {
        this.refs.cnt.classList.toggle('dropdown--active');
      } // reload() {
      //   if (dev) {
      //     console.log('f: reload');
      //   }
      // }

    }]);

    return Dropdown;
  }();

  var items = d.querySelectorAll('.dropdown');

  try {
    items.forEach(function (i) {
      return new Dropdown(i);
    });
  } catch (e) {
    console.log(e);
  }
})(document);
"use strict";

(function (d) {
  if (!d.querySelector('.foldSocials')) return;

  if (dev) {
    console.log('INIT fond-socials');
  }

  var clickHandler = function clickHandler(e) {
    var el = e.currentTarget;
    var parent = el.parentElement;
    parent.classList.toggle('foldSocials--active');
  };

  var items = d.querySelectorAll('.foldSocials');

  try {
    items.forEach(function (i) {
      var btn = i.querySelector('.foldSocials__item--main');
      btn.addEventListener('click', clickHandler);
    });
  } catch (e) {
    console.log(e);
  }
})(document);
"use strict";

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

(function (d) {
  // const dev = true;
  var AjaxForm =
  /*#__PURE__*/
  function () {
    function AjaxForm(form) {
      _classCallCheck(this, AjaxForm);

      this.form = form;
      this.errEl = null;
      this.send = this.send.bind(this);
      this.init();
    }

    _createClass(AjaxForm, [{
      key: "init",
      value: function init() {
        this.form.addEventListener('submit', this.send);
      }
    }, {
      key: "send",
      value: function send(e) {
        var _this = this;

        e.preventDefault();
        var url = this.form.getAttribute('action');
        var method = this.form.getAttribute('method');
        var FD = new FormData(this.form);
        var log = encodeURIComponent(FD.get('log'));
        var psw = encodeURIComponent(FD.get('psw'));
        var params = "log=".concat(log, "&psw=").concat(psw);
        var xhr = new XMLHttpRequest();
        xhr.open(method, "".concat(url, "?").concat(params));
        xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
        xhr.send();
        xhr.addEventListener('readystatechange', function () {
          if (xhr.readyState != 4) return;
          var res = JSON.parse(xhr.responseText);

          if (res.status) {
            location.reload();
          } else {
            _this.showErr(res.text);
          }
        });
      }
    }, {
      key: "showErr",
      value: function showErr(msg) {
        var _this2 = this;

        var createErrEl = function createErrEl() {
          var errEl = d.createElement('div');
          errEl.className = 'form__error';
          _this2.errEl = errEl;
        };

        if (!this.errEl) {
          createErrEl();
        }

        this.errEl.innerHTML = msg;
        this.form.appendChild(this.errEl);
      }
    }]);

    return AjaxForm;
  }();

  var loginForm = d.querySelector('.form--login');

  try {
    new AjaxForm(loginForm);
  } catch (e) {
    console.log(e);
  }
})(document);
"use strict";

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

(function (d) {
  // const dev = true;
  if (dev) {
    console.log('INIT form');
  }

  var AnimLabel =
  /*#__PURE__*/
  function () {
    function AnimLabel(input) {
      _classCallCheck(this, AnimLabel);

      this.state = {
        filled: false,
        focused: false
      };
      this.input = input;
      this.item = input.closest('.form__item');
      this.changeHandler = this.changeHandler.bind(this);
      this.focusHandler = this.focusHandler.bind(this);
      this.blurHandler = this.blurHandler.bind(this);
      this.addListeners();
    }

    _createClass(AnimLabel, [{
      key: "addListeners",
      value: function addListeners() {
        this.input.addEventListener('change', this.changeHandler);
        this.input.addEventListener('focus', this.focusHandler);
        this.input.addEventListener('blur', this.blurHandler);
      }
    }, {
      key: "focusHandler",
      value: function focusHandler() {
        if (dev) {
          console.log('f: focusHandler');
        }

        this.changeState('toggleFocused');
      }
    }, {
      key: "blurHandler",
      value: function blurHandler() {
        if (dev) {
          console.log('f: blurHandler');
        }

        this.changeState('toggleFocused');
      }
    }, {
      key: "changeHandler",
      value: function changeHandler(e) {
        if (dev) {
          console.log('f: changeHandler');
        }

        var state = this.state.filled;
        var value = e.target.value;

        if (!state && value || state && !value) {
          this.changeState('toggleFilled');
        }
      }
    }, {
      key: "toggleClass",
      value: function toggleClass() {
        if (dev) {
          console.log('f: toggleClass');
        }

        this.item.classList.toggle('form__item--inputFilled');
      }
    }, {
      key: "toggleFocusClass",
      value: function toggleFocusClass() {
        if (dev) {
          console.log('f: toggleFocusClass');
        }

        this.item.classList.toggle('form__item--focused');
      } // state modifiers

    }, {
      key: "changeState",
      value: function changeState(action) {
        switch (action) {
          case 'toggleFilled':
            this.actionToggleFilled();
            this.toggleClass();

            if (dev) {
              console.log(this.state);
            }

            break;

          case 'toggleFocused':
            this.toggleFocusClass();

            if (dev) {
              console.log(this.state);
            }

            break;
        }
      }
    }, {
      key: "actionToggleFilled",
      value: function actionToggleFilled() {
        if (dev) {
          console.log('f: actionToggleFilled');
        }

        this.state.filled = !this.state.filled;
      }
    }, {
      key: "actionToggleFocused",
      value: function actionToggleFocused() {
        if (dev) {
          console.log('f: actionToggleFocused');
        }

        this.state.filled = !this.state.filled;
      }
    }]);

    return AnimLabel;
  }(); // const wpcf7Elm = document.querySelector( '.wpcf7' );
  // if (wpcf7Elm) {
  //   wpcf7Elm.addEventListener( 'wpcf7submit', function(e) {
  //     openModal('.modal--success');
  //   }, false );
  // для тестирования
  // setTimeout(() => {
  //   const ev = new Event('wpcf7submit');
  //   wpcf7Elm.dispatchEvent(ev);
  // }, 4000);
  // }
  // для тестирования
  // const form = d.querySelector('.form--propose');
  // const invalidEvent = () => {
  //   if (dev) {
  //     console.log('f: invalidEvent');
  //   }
  // };
  // form.addEventListener('invalid', invalidEvent);
  //
  // setTimeout(() => {
  //   const event = new Event('invalid');
  //   form.dispatchEvent(event);
  // }, 3000);


  var inputs = d.querySelectorAll('.form--animLabels .form__input');

  try {
    inputs.forEach(function (i) {
      return new AnimLabel(i);
    });
  } catch (e) {
    console.log(e);
  }
})(document);
"use strict";

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

(function (d) {
  // const dev = true;
  if (!d.querySelector('.frontTabsLayout')) return;

  if (dev) {
    console.log('INIT front-tabs');
  }

  var FrontTabs =
  /*#__PURE__*/
  function () {
    function FrontTabs() {
      _classCallCheck(this, FrontTabs);

      this.state = {
        prevActiveTabId: 0,
        activeTabId: 0
      };
      this.refs = {
        contentContainer: d.querySelector('.frontTabsContent'),
        contentItems: d.querySelectorAll('.frontTabsContent__item'),
        navContainer: d.querySelector('.frontTabsNav'),
        navItems: d.querySelectorAll('.frontTabsNav__button'),
        activeContent: null,
        activeTab: null
      };
      this.button = null;
      this.init();
      this.clickHandler = this.clickHandler.bind(this);
    }

    _createClass(FrontTabs, [{
      key: "init",
      value: function init() {
        if (dev) {
          console.log('f: init');
        }

        var savedTab = localStorage.getItem('frontTab');
        !savedTab ? localStorage.setItem('frontTab', 2) : null;
        var maxTabsID = this.refs.contentItems.length - 1;

        if (savedTab) {
          if (savedTab <= maxTabsID) {
            this.state.prevActiveTabId = +savedTab;
            this.state.activeTabId = +savedTab;
          } else {
            localStorage.removeItem('frontTab');
          }
        }

        this.addListeners();
        this.updateContent();
        this.fireAnim();
        this.delHiddenAttr();
        this.initLine();
      }
    }, {
      key: "delHiddenAttr",
      value: function delHiddenAttr() {
        this.refs.contentItems.forEach(function (i) {
          i.removeAttribute('hidden');
        });
        this.updateHeight();
      }
    }, {
      key: "updateHeight",
      value: function updateHeight() {
        var _this = this;

        if (dev) {
          console.log('f: updateHeight');
        }

        if (d.documentElement.clientWidth >= 768) {
          setTimeout(function () {
            var active = _this.refs.activeContent;
            var height = active.offsetHeight;

            if (height === 0) {
              _this.refs.contentContainer.style.height = null;
            } else {
              _this.refs.contentContainer.style.height = height + 'px';
            }

            if (dev) {
              console.log(active);
              console.log(height);
            }
          }, 100);
        }
      }
    }, {
      key: "addListeners",
      value: function addListeners() {
        var _this2 = this;

        if (dev) {
          console.log('f: addListeners');
        }

        this.refs.navItems.forEach(function (item) {
          item.addEventListener('click', _this2.clickHandler.bind(_this2));
        });
      }
    }, {
      key: "clickHandler",
      value: function clickHandler(e) {
        if (dev) {
          console.log('f: clickHandler');
        }

        var newId = e.target.getAttribute('data-tab-id');
        if (!newId) return;
        this.setActiveTabId(newId);

        if (dev) {
          console.log(this.state);
        }
      }
    }, {
      key: "updateContent",
      value: function updateContent() {
        if (dev) {
          console.log('f: updateContent');
        }

        var activeContent = this.refs.activeContent;
        var activeTab = this.refs.activeTab;
        var id = this.state.activeTabId;

        if (activeTab) {
          activeTab.classList.remove('-active');
        }

        if (activeContent) {
          activeContent.classList.remove('-active');
        }

        this.refs.activeTab = this.refs.navItems[id];
        this.refs.activeContent = this.refs.contentItems[id];
        this.updateHeight();
      }
    }, {
      key: "fireAnim",
      value: function fireAnim() {
        if (dev) {
          console.log('f: fireAnim');
        }

        var prevId = this.state.prevActiveTabId;
        var nextId = this.state.activeTabId;
        var duration = 400;
        var nextCnt = this.refs.activeContent;
        var nextTab = this.refs.activeTab;
        var prevCnt = this.refs.contentItems[prevId];
        var prevTab = this.refs.navItems[prevId];
        nextTab.classList.add('-active');
        nextCnt.classList.add('-active');
        if (prevId === nextId) return;
        prevTab.classList.remove('-active');
        setTimeout(function () {
          prevCnt.classList.remove('-active');
        }, duration);

        if (prevId < nextId) {
          prevCnt.classList.add('animOutToLeft');
          nextCnt.classList.add('animInToLeft');
          setTimeout(function () {
            prevCnt.classList.remove('animOutToLeft');
            nextCnt.classList.remove('animInToLeft');
          }, duration);
        }

        if (prevId > nextId) {
          prevCnt.classList.add('animOutToRight');
          nextCnt.classList.add('animInToRight');
          setTimeout(function () {
            prevCnt.classList.remove('animOutToRight');
            nextCnt.classList.remove('animInToRight');
          }, duration);
        }

        this.updateHeight();
      }
    }, {
      key: "setActiveTabId",
      value: function setActiveTabId(id) {
        if (dev) {
          console.log('f: setActiveTabId');
        }

        this.state.prevActiveTabId = this.state.activeTabId;
        this.state.activeTabId = id;
        this.refs.activeTab = this.refs.navItems[id];
        this.refs.activeContent = this.refs.contentItems[id]; // write to localStorage

        localStorage.setItem('frontTab', id); // this.updateContent();

        this.fireAnim();
        this.moveLine();

        if (dev) {
          console.log(this.state);
        }
      }
    }, {
      key: "initLine",
      value: function initLine() {
        var _this3 = this;

        this.button = d.createElement('div');
        this.button.classList.add('frontTabsNav__line');
        this.refs.navContainer.appendChild(this.button);
        this.moveLine();
        setTimeout(function () {
          _this3.moveLine();
        }, 50);
      }
    }, {
      key: "moveLine",
      value: function moveLine() {
        var _this4 = this;

        if (dev) {
          console.log('f: moveLine');
        }

        var tab = this.refs.activeTab;
        var parent = tab.closest('.frontTabsNav');
        var width = tab.offsetWidth * 0.8;
        var offset = getCoords(tab).left - getCoords(parent).left;
        this.button.style.cssText = "        width: 20px;         left: ".concat(offset, "px;         transition-duration: 0.3s;\n        transition-timing-function: linear;");
        setTimeout(function () {
          _this4.button.style.cssText = "        width: ".concat(width, "px;         left: ").concat(offset, "px;         transition-duration: 4s;");
        }, 300);
      }
    }]);

    return FrontTabs;
  }();

  try {
    new FrontTabs();
  } catch (e) {
    console.log(e);
  }
})(document);
"use strict";

(function (d) {
  // const dev = true;
  if (!d.querySelector('.interviewAside__slides')) return;

  if (dev) {
    console.log('INIT interview-sliders');
  }

  var interview = tns({
    container: d.querySelector('.interviewAside__slides'),
    mode: 'carousel',
    loop: false,
    gutter: -95,
    controls: false,
    nav: false,
    navContainer: d.querySelector('.interviewAside__dots'),
    responsive: {
      768: {
        nav: true,
        mode: 'gallery',
        gutter: 0
      }
    }
  });

  try {
    var contents = d.querySelectorAll('.interviewAside__cntItem');

    var changeContent = function changeContent(e) {
      var index = e.index;
      var active = d.querySelector('.interviewAside__cntItem.-active');
      active.classList.remove('-active');
      contents[index].classList.add('-active');
    };

    interview.events.on('indexChanged', changeContent);
  } catch (e) {
    console.log(e);
  }
})(document);
"use strict";

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

(function (d) {
  // const dev = true;
  if (!d.querySelector('.laureates--innerPage')) return;

  if (dev) {
    console.log('INIT laureates-show-fourth');
  }

  var ShowFourth =
  /*#__PURE__*/
  function () {
    function ShowFourth(el) {
      _classCallCheck(this, ShowFourth);

      this.el = el;
      this.inner = this.el.querySelector('.laureates__items');
      this.fourth = null;
      this.state = {
        shifted: false,
        isActive: false,
        offset: null
      };
      this.init();
    }

    _createClass(ShowFourth, [{
      key: "init",
      value: function init() {
        var elWidth = this.el.offsetWidth;
        var innerWidth = this.inner.offsetWidth;
        var diff = innerWidth - elWidth;
        this.fourth = this.el.querySelector('.laureates__item:nth-child(4)');

        if (diff > 0 && this.fourth) {
          if (dev) {
            console.log(elWidth + ':' + innerWidth);
          }

          this.makeActive(diff);
        }
      }
    }, {
      key: "makeActive",
      value: function makeActive(offset) {
        if (dev) {
          console.log('f: makeActive');
        }

        this.state.isActive = true;
        this.state.offset = offset;
        this.fourth.classList.add('laureates__item--fourth');
        this.el.classList.add('-hideRight');
        this.addListeners();

        if (dev) {
          console.log(this.state);
        }
      } // BEGIN listeners and handlers

    }, {
      key: "addListeners",
      value: function addListeners() {
        this.fourth.addEventListener('mouseenter', this.mouseEnterHandler.bind(this));
        this.fourth.addEventListener('mouseleave', this.mouseLeaveHandler.bind(this));
      }
    }, {
      key: "mouseEnterHandler",
      value: function mouseEnterHandler(e) {
        this.show();
      }
    }, {
      key: "mouseLeaveHandler",
      value: function mouseLeaveHandler(e) {
        this.hide();
      } // END listeners and handlers

    }, {
      key: "show",
      value: function show() {
        if (dev) {
          console.log('f: show');
        }

        var offset = -this.state.offset + 'px';
        this.inner.style.transform = "translate3D(".concat(offset, ",0,0)");
        this.el.classList.remove('-hideRight');
        this.el.classList.add('-hideLeft');
      }
    }, {
      key: "hide",
      value: function hide() {
        if (dev) {
          console.log('f: hide');
        }

        this.inner.style.transform = "translate3D(0,0,0)";
        this.el.classList.add('-hideRight');
        this.el.classList.remove('-hideLeft');
      }
    }]);

    return ShowFourth;
  }();

  var items = d.querySelectorAll('.laureates--innerPage');

  try {
    items.forEach(function (i) {
      return new ShowFourth(i);
    });
  } catch (e) {
    console.log(e);
  }
})(document);
"use strict";

(function (d) {
  if (dev) {
    console.log('INIT laureates');
  } // begin slider


  var slidersItems = d.querySelectorAll('.laureates__slider');
  var sliders = [];
  slidersItems.forEach(function (slide) {
    var dots = slide.closest('.laureates__item').querySelector('.laureates__dots');
    var arrows = slide.closest('.laureates__item').querySelector('.laureates__arrows');
    sliders.push(tns({
      container: slide,
      mode: 'gallery',
      loop: false,
      navContainer: dots,
      controlsContainer: arrows
    }));
  });
  /*const changeName = (e) => {
    if (dev) {
      console.log('f: sliderEventHandler');
    }
     const slide = e.slideItems[e.index];
    const name = slide.getAttribute('data-name');
    const nameEl = slide.closest('.laureates__item').querySelector('.laureates__name');
     setTimeout(() => {
      nameEl.innerHTML = name;
    }, 150);
    nameEl.classList.add('laureates__name--anim');
    setTimeout(() => {
      nameEl.classList.remove('laureates__name--anim');
     }, 300);
   };
  for (let i in sliders) {
    sliders[i].events.on('indexChanged', changeName)
  }*/
  // end slider
  // begin hover

  var imgLinks = d.querySelectorAll('.laureates__image a');

  var hoverHandler = function hoverHandler(e) {
    if (dev) {
      console.log('f: hoverHandler');
    }

    var el = e.currentTarget;
    var parent = el.closest('.laureates__item');
    parent.classList.add('laureates__item--imgHovered');

    var mouseLeaveHandler = function mouseLeaveHandler(e) {
      if (dev) {
        console.log('f: hoverHandler');
      }

      parent.classList.remove('laureates__item--imgHovered');
      el.removeEventListener('mouseleave', mouseLeaveHandler);
    };

    el.addEventListener('mouseleave', mouseLeaveHandler);
  };

  imgLinks.forEach(function (i) {
    i.addEventListener('mouseenter', hoverHandler);
  }); // end hover
})(document);
"use strict";

(function (d) {
  // const dev = true;
  if (!window.fsLightbox) return;

  if (dev) {
    console.log('INIT lightbox');
  }

  fsLightbox.data.slideCounter = true;
  var links = d.querySelectorAll('[data-fslightbox]');
  links.forEach(function (i) {
    i.addEventListener('click', function () {
      if (dev) {
        console.log('e: click');
      }

      fsLightbox.show();
    });
  });

  try {
    var moreLinks = d.querySelectorAll('.gallery__counter');
    moreLinks.forEach(function (i) {
      i.addEventListener('click', function (e) {
        e.preventDefault();
        var node = e.target;
        var instance = node.getAttribute('data-fslightbox-gallery');
        fsLightboxInstances[instance].init();
        fsLightboxInstances[instance].show();
      });
    });
  } catch (e) {}
})(document);
"use strict";

(function () {
  if (dev) {
    console.log('INIT menu-links');
  }
  /**
   * Не должно быть перехода по ссылке, если в href есть #
   */


  document.getElementsByClassName('header__menu')[0].addEventListener('click', function (e) {
    if (e.target.getAttribute('href') && e.target.getAttribute('href') === '#') e.preventDefault();
  });
})();
"use strict";

/* Скрипт для открывания меню-гамбургера. */
(function () {
  if (dev) {
    console.log('INIT mob-menu');
  } // проверяем, есть ли само меню и кнопка, если нет, выходим


  var button = document.querySelector('.header__burger');
  var menu = document.querySelector('.mobMenu');

  if (!button && !menu) {
    console.error('Не найдены кнопка .mobMenu__button и мобильное меню .mobMenu.');
    return;
  } else if (!button && menu) {
    console.error('Не найдена кнопка .mobMenu__button.');
    return;
  } else if (button && !menu) {
    console.error('Не найдено мобильное меню .mobMenu.');
    return;
  } // **********************************
  // Открытие/закрытие мобильного меню
  // **********************************


  var opened = false; // вешаем клик на кнопку мобильного меню

  button.addEventListener('click', clickHandler); // вешаем клик на кнопку закрытия мобильного меню

  var closers = document.querySelectorAll('.mobMenu__close, .mobMenu .headerActions__item--search'); // если закрывашки нет, выходим из скрипта

  if (!closers) {
    console.error('Нет кнопки закрытия мобильного меню.');
    return;
  }

  closers.forEach(function (i) {
    i.addEventListener('click', clickHandler);
  }); // вызываем, когда надо открыть меню

  function openMenu() {
    if (dev) {
      console.log('f: openMenu');
    }

    opened = true;
    button.classList.add('mobMenu__button--opened');
    menu.classList.add('mobMenu--opened');
    document.body.classList.add('noScroll');
  } // вызываем, когда надо закрыть меню


  function closeMenu() {
    if (dev) {
      console.log('f: closeMenu');
    }

    opened = false;
    button.classList.remove('mobMenu__button--opened');
    menu.classList.remove('mobMenu--opened');
    document.body.classList.remove('noScroll');
  } // обработчик на клик для кнопки мобильного меню и закрытия


  function clickHandler(e) {
    if (dev) {
      console.log('f: clickHandler');
    }

    if (!opened) {
      openMenu();
    } else {
      closeMenu();
    }
  } // *******************************************
  // Открытие/закрытие подменю в мобильном меню
  // *******************************************
  // вешаем клик на родительские ссылки меню


  var menuParentLinks = menu.querySelectorAll('.menu__item--parent > .menu__link');
  menuParentLinks.forEach(function (link) {
    return link.addEventListener('click', parentLinkClickHandler);
  }); // клик на родительский пункт меню. При первом нажатии перехода по ссылке нет, открывается подменю, при повторном идет переход

  function parentLinkClickHandler(e) {
    if (dev) {
      console.log('parentLinkClickHandler');
    } // родительская ссылка, по которой нажали


    var link = e.target;
    var parent = link.parentElement;

    if (link.wasClicked && link.getAttribute('href').search('#') !== -1) {
      return;
    } else {
      e.preventDefault();
      menuParentLinks.forEach(function (link) {
        return link.wasClicked = false;
      });
      link.wasClicked = true;
    }

    var lastActiveSubMenu = menu.querySelector('.menu__subMenu--opened');

    if (lastActiveSubMenu) {
      parent.classList.remove('menu__item--opened');
      lastActiveSubMenu.classList.remove('menu__subMenu--opened');
    }

    var subMenu = parent.getElementsByClassName('menu__subMenu')[0];
    parent.classList.add('menu__item--opened');
    subMenu.classList.add('menu__subMenu--opened');
  }
})();
"use strict";

/* Скрипт для модальных окон. Чтобы работало, надо для элемента, при клике на который будет появляться модальное окно, добавить атрибут data-modal=".modal--question". Значение атрибута должно быть селектором нужного модального окна. */
// const dev = true;
(function () {
  if (dev) {
    console.log('INIT modal');
  } // выходим, если нет ссылок на модальные окна


  if (!document.querySelector('[data-modal]')) return; // ссылки на модальные окна, в атрибуте должен быть селектор на само модальное окно

  var links = document.querySelectorAll('[data-modal]'); // навешиваем обработчик на клик

  links.forEach(function (link) {
    return link.addEventListener('click', clickHandler);
  }); // нажатие на любой крестик в модальном окне закрывает его

  var closers = document.querySelectorAll('.modal__close, .modal [data-modal-closer]');
  closers.forEach(function (closer) {
    return closer.addEventListener('click', closeModal);
  }); // нажатие на фон в любом модальном окне закрывает его

  var modalCovers = document.querySelectorAll('.modal__bg');
  modalCovers.forEach(function (cover) {
    return cover.addEventListener('click', closeModal);
  }); // закрываем по ESC

  document.addEventListener('keyup', function (e) {
    if (openedModal && e.keyCode === 27) closeModal();
  }); // сюда будем записывать текущее открытое модальное окно

  var openedModal; // открытие модалки через функцию

  window.openModal = function (modalClass) {
    if (openedModal) {
      closeModal();
    } // берем нужное модальное окно


    openedModal = document.querySelector(modalClass); // проверяем, есть ли такое модальное окно

    if (openedModal) {
      // показываем его
      openModal();
    } else {
      console.error('Такого модального окна нет! Проверьте селектор в атрибуте data-modal.');
    }
  }; // обработчик на клик кнопки


  function clickHandler(e) {
    if (dev) {
      console.log('f: clickHandler');
    }

    e.preventDefault(); // кнопка, по которой нажали

    var link = e.currentTarget; // берем нужное модальное окно

    openedModal = document.querySelector(link.getAttribute('data-modal')); // проверяем, есть ли такое модальное окно

    if (openedModal) {
      // показываем его
      openModal();
    } else {
      console.error('Такого модального окна нет! Проверьте селектор в атрибуте data-modal.');
    }
  } // вызываем, чтобы показать модалку


  function openModal() {
    if (dev) {
      console.log('f: openModal');
    }

    openedModal.classList.add('modal--opened');
    var scrollWidth = getScrollWidth();

    if (scrollWidth > 0) {
      document.body.style.marginRight = scrollWidth + 'px';
    }

    document.body.classList.add('noScroll');
  } // вызываем, чтобы скрыть модалку


  function closeModal(e) {
    if (dev) {
      console.log('f: closeModal');
    }

    if (!openedModal) {
      openedModal = document.querySelector('.modal--opened');
    }

    openedModal.classList.remove('modal--opened');
    document.body.classList.remove('noScroll');
    document.body.style.marginRight = ''; // стираем инфо о текущем открытом окне

    openedModal = null;
  } // ширина скролла


  function getScrollWidth() {
    if (dev) {
      console.log('f: getScrollWidth');
    }

    var scrollWidth = window.innerWidth - document.body.clientWidth;
    console.log(scrollWidth);
    return scrollWidth;
  }
})();
"use strict";

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

(function (d) {
  // const dev = true;
  if (!d.querySelector('.rules__slider')) return;

  if (dev) {
    console.log('INIT rules-slider');
  }

  var slider = tns({
    container: d.querySelector('.rules__slider'),
    mode: 'gallery',
    controls: false,
    navContainer: d.querySelector('.rulesPreviews__items'),
    disable: true,
    responsive: {
      768: {
        disable: false
      }
    }
  });

  var RulesScroll =
  /*#__PURE__*/
  function () {
    function RulesScroll() {
      _classCallCheck(this, RulesScroll);

      this.refs = {
        el: d.querySelector('.rulesPreviews')
      };
      this.state = {};

      if (this.refs.el) {
        this.init();
      }
    }

    _createClass(RulesScroll, [{
      key: "init",
      value: function init() {
        if (dev) {
          console.log('init: RulesScroll');
        }
      }
    }]);

    return RulesScroll;
  }();

  try {
    new RulesScroll();
  } catch (e) {
    console.log(e);
  }
})(document);
"use strict";

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

(function (d) {
  // const dev = true;
  if (!d.querySelector('.scrollRow')) return;

  if (dev) {
    console.log('INIT scroll-row');
  }

  var ScrollRow =
  /*#__PURE__*/
  function () {
    function ScrollRow(el) {
      _classCallCheck(this, ScrollRow);

      this.el = el;
      this.inner = el.querySelector('.scrollRow__inner');
      this.innerFistChild = this.inner.firstElementChild;
      this.cursor = d.querySelector('.dragCursor');
      this.arrowPrev = el.querySelector('.scrollRow__arrowPrev');
      this.arrowNext = el.querySelector('.scrollRow__arrowNext');
      this.state = {
        width: 0,
        offset: 0,
        minOffset: 0,
        nodrag: false,
        dragged: false,
        hideLeft: false,
        isActive: true,
        arrows: false
      };
      this.step = 100;
      this.changeState = this.changeState.bind(this);
      this.setInitValues();

      if (!this.cursor) {
        this.createCursor();
      }
    }

    _createClass(ScrollRow, [{
      key: "setInitValues",
      value: function setInitValues() {
        var _this = this;

        d.addEventListener('DOMContentLoaded', function () {
          var innerWidth = _this.inner.offsetWidth;
          var cntWidth = _this.el.offsetWidth;
          var minOffset = cntWidth - innerWidth;

          if (minOffset >= 0) {
            minOffset = 0;
          } else {
            minOffset -= 100;
          }

          _this.state.width = innerWidth;
          _this.state.minOffset = minOffset;
          _this.state.isActive = !(minOffset === 0);

          if (_this.arrowNext && _this.arrowNext) {
            _this.state.arrows = true;
          }

          if (_this.el.classList.contains('scrollRow--nodrag')) {
            _this.state.nodrag = true;
          }

          _this.addListener();

          if (dev) {
            console.log(_this.state);
          }
        });
      } // BEGIN listeners and handlers

    }, {
      key: "addListener",
      value: function addListener() {
        if (!this.state.isActive) return;
        this.el.addEventListener('wheel', function (e) {
          return e.preventDefault();
        });
        this.el.addEventListener('wheel', throttle(this.scrollHandler.bind(this), 20).bind(this));

        if (!this.state.nodrag) {
          if (isTouchDevice()) {
            this.el.addEventListener('touchstart', this.moveHandler.bind(this));
          } else {
            this.el.addEventListener('mouseover', this.mouseOverHandler.bind(this));
            this.el.addEventListener('mousedown', this.moveHandler.bind(this));
          }
        }

        if (this.state.arrows) {
          if (dev) {
            console.log('add arrow listeners');
          }

          this.arrowPrev.addEventListener('click', this.arrowClickHandler.bind(this));
          this.arrowNext.addEventListener('click', this.arrowClickHandler.bind(this));
        }
      }
    }, {
      key: "scrollHandler",
      value: function scrollHandler(e) {
        if (dev) {
          console.log('f: scrollHandler');
          console.log(e);
        }

        var delta = e.deltaY;

        if (delta > 0) {
          this.changeState('forward', Math.abs(delta));
        } else if (delta < 0) {
          this.changeState('backward', Math.abs(delta));
        }

        e.preventDefault();
      }
    }, {
      key: "mouseOverHandler",
      value: function mouseOverHandler(e) {
        var _this2 = this;

        if (dev) {
          console.log('f: mouseOverHandler');
          console.log(e.target);
        }

        if (e.target != this.innerFistChild && e.target != this.inner && !this.state.dragged) {
          this.cursor.style.display = 'none';
          d.body.style.cursor = '';
          return false;
        }

        this.cursor.style.display = 'block';
        d.body.style.cursor = 'none';

        var moveHandler = function moveHandler(e) {
          _this2.cursor.style.top = "".concat(e.clientY, "px");
          _this2.cursor.style.left = "".concat(e.clientX, "px");
        };

        d.addEventListener('mousemove', moveHandler);
        d.addEventListener('mouseout', function (e) {
          d.removeEventListener('mousemove', moveHandler);
          _this2.cursor.style.display = 'none';
          d.body.style.cursor = '';
        });
      }
    }, {
      key: "moveHandler",
      value: function moveHandler(e) {
        if (dev) {
          console.log('f: moveHandler');
        }

        var self = this;
        var initPos;

        if (e.target != self.innerFistChild && e.target != self.inner && e.target !== d.querySelector('.laureates__items')) {
          if (dev) {
            console.log('in cond');
            console.log(e.target);
          }

          return;
        }

        self.changeState('dragged');
        self.inner.style.transitionDuration = '1s';
        self.inner.style.transitionTimingFunction = 'cubic-bezier(0, 0.65, 0.58, 1)';
        moveAt(e);

        function moveAt(e) {
          var pos = e.pageX ? e.pageX : e.changedTouches[0].pageX;
          var offset;
          var coeff = 2;

          if (initPos && initPos < pos) {
            offset = pos - initPos;
            offset = offset * coeff;
            self.changeState('backward', offset);
          }

          if (initPos && initPos > pos) {
            offset = initPos - pos;
            offset = offset * coeff;
            self.changeState('forward', offset);
          }

          initPos = e.pageX ? e.pageX : e.changedTouches[0].pageX;
        }

        if (isTouchDevice()) {
          document.ontouchmove = throttle(function (e) {
            moveAt(e);
          }, 16);

          self.el.ontouchend = function () {
            if (dev) {
              console.log('f: ontouchend');
            } // self.el.classList.remove('-dragged');


            self.inner.style.transitionDuration = '';
            self.inner.style.transitionTimingFunction = '';
            document.ontouchmove = null;
            self.el.ontouchend = null;
            self.changeState('dragged');
          };
        } else {
          document.onmousemove = throttle(function (e) {
            moveAt(e);
          }, 16);

          self.el.onmouseup = function () {
            if (dev) {
              console.log('f: onmouseup');
            } // self.el.classList.remove('-dragged');


            self.inner.style.transitionDuration = '';
            self.inner.style.transitionTimingFunction = '';
            document.onmousemove = null;
            self.el.onmouseup = null;
            self.changeState('dragged');
          };
        }
      }
    }, {
      key: "arrowClickHandler",
      value: function arrowClickHandler(e) {
        if (dev) {
          console.log('f:arrowClickHandler');
        }

        if (e.target.classList.contains('scrollRow__arrowPrev')) {
          if (dev) {
            console.log('click prev');
          }

          this.changeState('backward', 400);
        }

        if (e.target.classList.contains('scrollRow__arrowNext')) {
          if (dev) {
            console.log('click next');
          }

          this.changeState('forward', 400);
        }
      } // END listeners and handlers

    }, {
      key: "createCursor",
      value: function createCursor() {
        if (dev) {
          console.log('f: createCursor');
        }

        this.cursor = d.createElement('div');
        this.cursor.innerHTML = "\n        <svg width=\"178\" height=\"178\" viewBox=\"0 0 178 178\" fill=\"none\" xmlns=\"http://www.w3.org/2000/svg\">\n          <circle class=\"dragCursor__circle\" cx=\"89\" cy=\"89\" r=\"88\" stroke=\"#000\" stroke-width=\"2\"/>\n          <path class=\"dragCursor__leftArrow\" d=\"M53.5275 89L64 79.9306L64 98.0694L53.5275 89Z\" stroke=\"#000\" stroke-width=\"2\"/>\n          <path class=\"dragCursor__rightArrow\" d=\"M124.472 89L114 98.0694L114 79.9306L124.472 89Z\" stroke=\"#000\" stroke-width=\"2\"/>\n          \n        </svg>\n      ";
        this.cursor.className = 'dragCursor';
        d.body.appendChild(this.cursor);
      }
    }, {
      key: "toggleCursorState",
      value: function toggleCursorState() {
        if (dev) {
          console.log('f: toggleCursorState');
        }

        var cursor = this.cursor;
        var circle = cursor.querySelector('.dragCursor__circle');
        var arrowLeft = cursor.querySelector('.dragCursor__leftArrow');
        var arrowRight = cursor.querySelector('.dragCursor__rightArrow');

        var animIn = function animIn() {
          circle.setAttribute('r', 38);
          arrowLeft.setAttribute('d', 'M0 90 L20 73 L20 107 L0 90Z');
          arrowRight.setAttribute('d', 'M178 90 L158 107 L158 73 L178 90Z');
        };

        var animOut = function animOut() {
          circle.setAttribute('r', 88);
          arrowLeft.setAttribute('d', 'M53.5275 89L64 79.9306L64 98.0694L53.5275 89Z');
          arrowRight.setAttribute('d', 'M124.472 89L114 98.0694L114 79.9306L124.472 89Z');
        };

        if (this.state.dragged) animIn();else animOut();
      }
    }, {
      key: "move",
      value: function move() {
        if (dev) {
          console.log('f: move');
        }

        this.inner.style.transform = "translateX(".concat(this.state.offset, "px)");
      }
    }, {
      key: "end",
      value: function end(direction) {
        if (dev) {
          console.log('f: end');
        }

        if (this.state.arrows) {
          if (direction === 'left') {
            this.arrowPrev.classList.add('disabled');
          }

          if (direction === 'right') {
            this.arrowNext.classList.add('disabled');
          }
        }
      }
    }, {
      key: "notEnd",
      value: function notEnd() {
        if (dev) {
          console.log('f: notEnd');
        }

        if (this.state.arrows) {
          this.arrowPrev.classList.remove('disabled');
          this.arrowNext.classList.remove('disabled');
        }
      }
    }, {
      key: "toggleScrollHideClass",
      value: function toggleScrollHideClass() {
        var cls = '-hideLeft';
        var offset = this.state.offset;
        var hideLeft = this.state.hideLeft;

        if (offset !== 0 && !hideLeft || offset === 0 && hideLeft) {
          this.el.classList.toggle(cls);
          this.state.hideLeft = !this.state.hideLeft;
        }
      } // actions ---------------------

    }, {
      key: "changeState",
      value: function changeState(action, payload) {
        switch (action) {
          case 'forward':
            this.notEnd();
            this.actionForward(payload);
            this.move();
            this.toggleScrollHideClass();
            break;

          case 'backward':
            this.notEnd();
            this.actionBackward(payload);
            this.move();
            this.toggleScrollHideClass();
            break;

          case 'dragged':
            this.actionDragged();
            this.el.classList.toggle('-dragged');
            this.toggleCursorState(); // this.cursor.classList.toggle('dragCursor--active');

            break;
        }
      }
    }, {
      key: "actionForward",
      value: function actionForward(offset) {
        if (dev) {
          console.log('f: scrollForward');
        }

        var newOffset;

        if (offset) {
          newOffset = this.state.offset - offset;
        } else {
          newOffset = this.state.offset - this.step;
        }

        if (newOffset <= this.state.minOffset) {
          if (dev) {
            console.log('newOffset <= this.state.minOffset');
            console.log(this.state.minOffset);
          }

          newOffset = this.state.minOffset;
          this.end('right');
        }

        this.state.offset = newOffset;
      }
    }, {
      key: "actionBackward",
      value: function actionBackward(offset) {
        if (dev) {
          console.log('f: scrollBackward');
        }

        var newOffset;

        if (offset) {
          newOffset = this.state.offset + offset;
        } else {
          newOffset = this.state.offset + this.step;
        }

        if (newOffset >= 0) {
          newOffset = 0;
          this.end('left');
        }

        this.state.offset = newOffset;
      }
    }, {
      key: "actionDragged",
      value: function actionDragged() {
        if (dev) {
          console.log('f: actionDragged');
        }

        this.state.dragged = !this.state.dragged;
      }
    }]);

    return ScrollRow;
  }();

  var items = d.querySelectorAll('.scrollRow');

  try {
    items.forEach(function (item) {
      if (window.outerWidth >= 768) {
        new ScrollRow(item);
      }
    });
  } catch (e) {
    console.log(e);
  }
})(document);
"use strict";

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

(function (d) {
  var SearchModel =
  /*#__PURE__*/
  function () {
    function SearchModel() {
      _classCallCheck(this, SearchModel);

      this.data = {};
      this.url = '/wp-json/api/search';
      this.active = 'applicants_texts';
    }

    _createClass(SearchModel, [{
      key: "initActive",
      value: function initActive() {
        var tabsData = this.getTabsData();
        var active = this.active;

        if (!active || tabsData[active] == 0) {
          active = null;

          for (var key in tabsData) {
            if (tabsData[key] !== 0) {
              active = key;
              this.setActive(active);
              return;
            }
          }

          this.setActive(null);
        }
      }
    }, {
      key: "initCurrentPage",
      value: function initCurrentPage() {
        for (var key in this.data) {
          this.data[key].currentPage = 1;
        }
      }
    }, {
      key: "getData",
      value: function getData(searchString, cbFinish, cbStart) {
        var _this = this;

        cbStart();
        this.searchString = searchString;
        var xhr = new XMLHttpRequest();
        var url = "".concat(this.url, "/main?q=").concat(searchString);
        xhr.open('get', url);
        xhr.send();

        xhr.onload = function () {
          if (xhr.status != 200) {
            console.log("\u041E\u0448\u0438\u0431\u043A\u0430 ".concat(xhr.status, ": ").concat(xhr.statusText));
          } else {
            var responseData = JSON.parse(xhr.responseText);
            _this.data = responseData;

            _this.initCurrentPage();

            _this.initActive();

            cbFinish();
          }
        };

        xhr.onerror = function () {
          console.log('Ошибка. Запрос не удался');
        };
      }
    }, {
      key: "updateData",
      value: function updateData(start, finish) {
        var _this2 = this;

        var active = this.data[this.active];
        var currentPage = active.currentPage;
        var pages = active.paged;
        if (currentPage === pages) return;
        if (start) start();
        var xhr = new XMLHttpRequest();
        var searchString = this.searchString;
        var nextPage = currentPage + 1;
        var url = "".concat(this.url, "/").concat(this.active, "?q=").concat(searchString, "&paged=").concat(nextPage);
        xhr.open('get', url);
        xhr.send();

        xhr.onload = function () {
          if (xhr.status != 200) {
            console.log("\u041E\u0448\u0438\u0431\u043A\u0430 ".concat(xhr.status, ": ").concat(xhr.statusText));
          } else {
            var responseData = JSON.parse(xhr.responseText);
            _this2.data[_this2.active].html = _this2.data[_this2.active].html + responseData.html;
            finish();
            active.currentPage = nextPage;
          }
        };

        xhr.onerror = function () {
          console.log('Ошибка. Запрос не удался');
        };
      }
    }, {
      key: "getCurrentItems",
      value: function getCurrentItems(cb) {
        var result = this.empty;

        if (this.active) {
          result = this.data[this.active].html;
        }

        cb(result);
      }
    }, {
      key: "getTabsData",
      value: function getTabsData(cb) {
        var tabsData = {};

        for (var item in this.data) {
          tabsData[item] = this.data[item].count_all;
        }

        if (cb) {
          cb(tabsData);
        }

        return tabsData;
      }
    }, {
      key: "getActive",
      value: function getActive(cb) {
        var active = this.active;
        cb(active);
      }
    }, {
      key: "setActive",
      value: function setActive(value, cb) {
        this.active = value;

        if (cb) {
          cb(value);
        }
      }
    }]);

    return SearchModel;
  }();

  var SearchView =
  /*#__PURE__*/
  function () {
    function SearchView(node) {
      _classCallCheck(this, SearchView);

      this.node = node;
      this.listEl = node.querySelector('.searchResults__list');
      this.tabListenersAdded = false;
      this.scrollListenersAdded = false;
      this.empty = 'По вашему запросу ничего не найдено';
      this.emptyEl = null;
      this.changeTabOnClick = null;
      this.getDataOnScroll = null;
      this.scrollHandler = null;
      this.renderTabs = this.renderTabs.bind(this);
      this.setActive = this.setActive.bind(this);
      this.renderList = this.renderList.bind(this);
      this.preload = this.preload.bind(this);
      this.preloadPart = this.preloadPart.bind(this);
    }

    _createClass(SearchView, [{
      key: "addTabListeners",
      value: function addTabListeners() {
        var tabs = this.node.querySelectorAll('.searchTabs__tab');
        var _iteratorNormalCompletion = true;
        var _didIteratorError = false;
        var _iteratorError = undefined;

        try {
          for (var _iterator = tabs[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
            var tab = _step.value;
            tab.addEventListener('click', this.changeTabOnClick);
          }
        } catch (err) {
          _didIteratorError = true;
          _iteratorError = err;
        } finally {
          try {
            if (!_iteratorNormalCompletion && _iterator["return"] != null) {
              _iterator["return"]();
            }
          } finally {
            if (_didIteratorError) {
              throw _iteratorError;
            }
          }
        }

        this.tabListenersAdded = true;
      }
    }, {
      key: "addScrollListeners",
      value: function addScrollListeners() {
        this.scrollHandler = throttle(this.getDataOnScroll, 500);
        window.addEventListener('scroll', this.scrollHandler);
        this.scrollListenersAdded = true;
      }
    }, {
      key: "removeScrollListeners",
      value: function removeScrollListeners() {
        window.removeEventListener('scroll', this.scrollHandler);
        this.scrollListenersAdded = false;
      }
    }, {
      key: "renderTabs",
      value: function renderTabs(data) {
        var tab;
        var tabNum;

        for (var i in data) {
          var value = data[i];
          tab = this.node.querySelector(".searchTabs__tab--".concat(i));
          tabNum = tab.querySelector('.searchTabs__num');
          tabNum.innerHTML = value;

          if (value == 0) {
            tab.disabled = true;
          } else {
            tab.disabled = false;
          }
        }

        if (!this.tabListenersAdded) {
          this.addTabListeners();
        }
      }
    }, {
      key: "renderList",
      value: function renderList(data) {
        this.hideEmpty();
        var listEl = this.listEl;
        listEl.classList.remove('searchResults__list--preloader');

        if (data) {
          listEl.innerHTML = data;

          if (!this.scrollListenersAdded) {
            this.addScrollListeners();
          }
        } else {
          this.renderEmpty();
        }
      }
    }, {
      key: "setActive",
      value: function setActive(active) {
        var currentActive = this.node.querySelector('.searchTabs__tab--active');

        if (currentActive) {
          currentActive.classList.remove('searchTabs__tab--active');
        }

        if (active) {
          var activeTab = this.node.querySelector(".searchTabs__tab--".concat(active));
          activeTab.classList.add('searchTabs__tab--active');
        }
      }
    }, {
      key: "preload",
      value: function preload() {
        this.hideEmpty();
        var tabNums = this.node.querySelectorAll('.searchTabs__num');
        tabNums.forEach(function (i) {
          i.innerHTML = 0;
        });
        var activeTab = this.node.querySelector('.searchTabs__tab--active');

        if (activeTab) {
          activeTab.classList.remove('searchTabs__tab--active');
        }

        var listEl = this.listEl;
        listEl.innerHTML = '';
        listEl.classList.add('searchResults__list--preloader');
        this.removeScrollListeners();
      }
    }, {
      key: "preloadPart",
      value: function preloadPart() {
        this.hideEmpty();
        var listEl = this.listEl;
        listEl.classList.add('searchResults__list--preloader');
        this.removeScrollListeners();
      }
    }, {
      key: "renderEmpty",
      value: function renderEmpty() {
        var el = this.emptyEl;

        if (!el) {
          var text = this.empty;
          el = d.createElement('div');
          el.className = 'searchResults__empty';
          el.innerHTML = text;
          this.emptyEl = el;
        } // const parent = this.node.querySelector('.searchResults__inner');


        var tabs = this.node.querySelector('.searchResults__tabs');
        tabs.after(el);
        this.removeScrollListeners();
      }
    }, {
      key: "hideEmpty",
      value: function hideEmpty() {
        if (this.emptyEl) {
          this.emptyEl.remove();
        }
      }
    }, {
      key: "getListPosition",
      value: function getListPosition() {
        var listEl = this.listEl;
        var coords = listEl.getBoundingClientRect();
        var fromBottom = coords.y + coords.height - d.documentElement.clientHeight;
        return fromBottom;
      }
    }]);

    return SearchView;
  }();

  var SearchController =
  /*#__PURE__*/
  function () {
    function SearchController(view, model) {
      _classCallCheck(this, SearchController);

      this.view = view;
      this.model = model;
      this.showData = this.showData.bind(this);
      this.showList = this.showList.bind(this);
      this.view.changeTabOnClick = this.changeTabOnClick.bind(this);
      this.view.getDataOnScroll = this.getDataOnScroll.bind(this);
    }

    _createClass(SearchController, [{
      key: "do",
      value: function _do(searchString) {
        this.model.getData(searchString, this.showData, this.view.preload);
      }
    }, {
      key: "showData",
      value: function showData() {
        this.model.getTabsData(this.view.renderTabs);
        this.model.getActive(this.view.setActive);
        this.model.getCurrentItems(this.view.renderList);
      }
    }, {
      key: "showList",
      value: function showList() {
        this.model.getCurrentItems(this.view.renderList);
      }
    }, {
      key: "updateData",
      value: function updateData() {
        this.model.updateData(this.view.preloadPart, this.showList);
      }
    }, {
      key: "searchString",
      value: function searchString() {
        return 'test search string';
      }
    }, {
      key: "changeTabOnClick",
      value: function changeTabOnClick(e) {
        e.preventDefault();
        var target = e.currentTarget;

        if (target.classList.contains('searchTabs__tab--active')) {
          return false;
        } else {
          var active = target.dataset.tabName;
          this.model.setActive(active, this.view.setActive);
          this.model.getCurrentItems(this.view.renderList);
        }
      }
    }, {
      key: "getDataOnScroll",
      value: function getDataOnScroll(e) {
        var offset = this.view.getListPosition();

        if (offset < 150) {
          this.updateData();
        }
      }
    }]);

    return SearchController;
  }();

  var rootEl = d.querySelector('.searchResults');

  if (rootEl) {
    try {
      window.SearchAjax = new SearchController(new SearchView(rootEl), new SearchModel());
    } catch (e) {
      console.log(e);
    }
  }
})(document);
"use strict";

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _get(target, property, receiver) { if (typeof Reflect !== "undefined" && Reflect.get) { _get = Reflect.get; } else { _get = function _get(target, property, receiver) { var base = _superPropBase(target, property); if (!base) return; var desc = Object.getOwnPropertyDescriptor(base, property); if (desc.get) { return desc.get.call(receiver); } return desc.value; }; } return _get(target, property, receiver || target); }

function _superPropBase(object, property) { while (!Object.prototype.hasOwnProperty.call(object, property)) { object = _getPrototypeOf(object); if (object === null) break; } return object; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

(function (d) {
  var dev = true;

  var SearchModal =
  /*#__PURE__*/
  function () {
    function SearchModal() {
      _classCallCheck(this, SearchModal);

      this.refs = {
        btnOpen: d.querySelectorAll('.headerActions__item--search'),
        btnClose: d.querySelector('.searchPopup__closer'),
        popup: d.querySelector('.searchPopup')
      };
      this.close = this.close.bind(this);
      this.open = this.open.bind(this);
      this.init();
    }

    _createClass(SearchModal, [{
      key: "init",
      value: function init() {
        var _this = this;

        this.refs.btnClose.addEventListener('click', this.close);
        this.refs.btnOpen.forEach(function (i) {
          i.addEventListener('click', _this.open);
        });
      }
    }, {
      key: "open",
      value: function open(e) {
        this.refs.popup.classList.add('-opened');
        searchField.focus();
      }
    }, {
      key: "close",
      value: function close(e) {
        e.preventDefault();
        this.refs.popup.classList.remove('-opened');
        searchField.clean(e);
      }
    }]);

    return SearchModal;
  }();

  new SearchModal();

  var BaseField =
  /*#__PURE__*/
  function () {
    function BaseField(el) {
      _classCallCheck(this, BaseField);

      this.state = {
        placeholder: 'Поиск',
        value: null,
        url: null
      };
      this.refs = {
        btnClean: null,
        field: null,
        placeholder: null,
        node: null
      };
      this.onMouseup = this.onMouseup.bind(this);
      this.onBlur = this.onBlur.bind(this);
      this.onKeyup = this.onKeyup.bind(this);
      this.onKeydown = this.onKeydown.bind(this);
      this.onPaste = this.onPaste.bind(this);
      this.clean = this.clean.bind(this);
      this.send = this.send.bind(this);
      this.focus = this.focus.bind(this);

      if (!(el instanceof HTMLElement)) {
        el = d.querySelector(el);
      }

      this.setRefs(el);
      this.init();
    }

    _createClass(BaseField, [{
      key: "init",
      value: function init() {
        this.initPlaceholder();
        this.state.url = this.refs.node.getAttribute('data-search-url');
        var field = this.refs.field; // field.addEventListener('focus', this.onFocus);

        field.addEventListener('mouseup', this.onMouseup);
        field.addEventListener('blur', this.onBlur);
        field.addEventListener('keyup', this.onKeyup);
        field.addEventListener('keydown', this.onKeydown);
        field.addEventListener('paste', this.onPaste);
        this.refs.btnClean.addEventListener('click', this.clean);
      }
    }, {
      key: "setRefs",
      value: function setRefs(node) {
        this.refs = {
          btnClean: node.querySelector('.searchField__clean'),
          field: node.querySelector('.searchField__field'),
          placeholder: node.querySelector('.searchField__placeholder'),
          node: node
        };
      }
    }, {
      key: "clean",
      value: function clean(e) {
        e.preventDefault();
        this.focus();
        this.state.value = null;
        var field = this.refs.field;
        field.innerHTML = null;
        this.refs.placeholder.style = null;
        field.classList.remove('-hasValue');
      }
    }, {
      key: "initPlaceholder",
      value: function initPlaceholder() {
        var placeholder = this.refs.placeholder;
        this.state.placeholder = placeholder.innerHTML || this.state.placeholder;
        placeholder.innerHTML = this.state.placeholder;
      }
    }, {
      key: "focus",
      value: function focus() {
        var _this2 = this;

        setTimeout(function () {
          _this2.refs.field.focus();
        }, 500);
      }
    }, {
      key: "onKeyup",
      value: function onKeyup(e) {
        var value = e.target.innerHTML;
        this.state.value = value;
        var field = this.refs.field;

        if (value) {
          field.classList.add('-hasValue');
        } else {
          field.classList.remove('-hasValue');
        }
      }
    }, {
      key: "onKeydown",
      value: function onKeydown(e) {
        var keyCode = e.key;

        if (keyCode === 'Enter' || keyCode === 'NumpadEnter') {
          e.preventDefault();
          this.send();
          return false;
        }
      }
    }, {
      key: "onMouseup",
      value: function onMouseup(e) {
        var value = this.state.value;
        var field = this.refs.field;
        var placeholder = this.refs.placeholder;

        if (!value) {
          field.innerHTML = '';
          placeholder.style.display = 'none';
        }
      }
    }, {
      key: "onPaste",
      value: function onPaste(e) {
        e.preventDefault();
        var data = e.clipboardData.getData("text/plain");
        e.target.innerHTML = data;
      }
    }, {
      key: "onBlur",
      value: function onBlur(e) {
        var node = e.target;
        var value = node.innerHTML;
        var field = this.refs.field;
        var placeholder = this.refs.placeholder;
        this.state.value = value;

        if (!value) {
          field.innerHTML = null;
          placeholder.style = '';
        }
      }
    }, {
      key: "send",
      value: function send() {
        console.log('send');
        var value = this.state.value;

        if (!value) {
          console.log('in cond');
          return false;
        }

        return true; // метод определен в наследующих классах
      }
    }]);

    return BaseField;
  }();

  var SearchField =
  /*#__PURE__*/
  function (_BaseField) {
    _inherits(SearchField, _BaseField);

    function SearchField(el) {
      _classCallCheck(this, SearchField);

      return _possibleConstructorReturn(this, _getPrototypeOf(SearchField).call(this, el));
    }

    _createClass(SearchField, [{
      key: "send",
      value: function send() {
        if (!_get(_getPrototypeOf(SearchField.prototype), "send", this).call(this)) return;
        var value = this.state.value;
        var url = this.state.url + "?q=".concat(value);
        location.href = url;
      }
    }]);

    return SearchField;
  }(BaseField);

  var PageSearchField =
  /*#__PURE__*/
  function (_BaseField2) {
    _inherits(PageSearchField, _BaseField2);

    function PageSearchField(el) {
      var _this3;

      _classCallCheck(this, PageSearchField);

      _this3 = _possibleConstructorReturn(this, _getPrototypeOf(PageSearchField).call(this, el));
      _this3.state.firstVisit = true;

      _this3.setInitValue();

      return _this3;
    }

    _createClass(PageSearchField, [{
      key: "setInitValue",
      value: function setInitValue() {
        var query = new URLSearchParams(location.search);
        var searchString = query.get('q');
        this.state.value = searchString;

        if (searchString) {
          var field = this.refs.field;
          field.innerHTML = searchString;
          field.classList.add('-hasValue');
          this.send();
        }
      }
    }, {
      key: "send",
      value: function send() {
        if (!_get(_getPrototypeOf(PageSearchField.prototype), "send", this).call(this)) return;

        if (!this.state.firstVisit) {
          this.changeUrl();
        } else {
          this.state.firstVisit = false;
        }

        SearchAjax["do"](this.state.value);
      }
    }, {
      key: "changeUrl",
      value: function changeUrl() {
        var string = this.state.value;
        var url = location.pathname + '?q=' + string;
        var stateObj = {
          search: 'search'
        };
        history.pushState(stateObj, 'hello', url);
      }
    }]);

    return PageSearchField;
  }(BaseField);

  var searchField;
  var pageFieldEl = d.querySelector('.searchField--page');

  try {
    searchField = new SearchField('.searchField--popup');

    if (pageFieldEl) {
      new PageSearchField(pageFieldEl);
    }
  } catch (e) {
    console.log(e);
  }
})(document);
"use strict";

(function (d) {
  // const dev = true;
  if (!d.querySelector('.filter__sort')) return;

  if (dev) {
    console.log('INIT sort-button');
  }

  var sort = function sort() {
    var button = d.querySelector('.filter__sortCurrent');
    var list = d.querySelector('.filter__sortList');
    var links = d.querySelectorAll('.filter__sortButton');
    var openCls = '-active';
    var isOpened = false;

    var bodyClickHandler = function bodyClickHandler(e) {
      if (dev) {
        console.log('f: bodyClickHandler');
      }

      var el = e.target;

      if (el !== button && Array.from(links).indexOf(el) === -1) {
        close();
      }
    };

    var open = function open() {
      if (dev) {
        console.log('f: open');
      }

      list.classList.add(openCls);
      button.classList.add(openCls);
      d.body.addEventListener('click', bodyClickHandler);
      isOpened = true;
    };

    var close = function close() {
      if (dev) {
        console.log('f: close');
      }

      list.classList.remove(openCls);
      button.classList.remove(openCls);
      d.body.removeEventListener('click', bodyClickHandler);
      isOpened = false;
    };

    var buttonClickHandler = function buttonClickHandler(e) {
      if (isOpened) {
        close();
      } else {
        open();
      }

      e.preventDefault();
    };

    button.addEventListener('click', buttonClickHandler);
    links.forEach(function (link) {
      link.addEventListener('click', close);
    });
  };

  var date = function date() {
    var btn = d.querySelector('.filter__dateCurrent');
    var cnt = d.querySelector('.filter__dateList');
    var openCls = '-active';
    var isOpened = false;

    var bodyClickHandler = function bodyClickHandler(e) {
      if (dev) {
        console.log('f: bodyClickHandler');
      }

      var parent = e.target.closest('.filter__date');

      if (!parent) {
        close();
      }
    };

    var open = function open() {
      if (dev) {
        console.log('f: open');
      }

      cnt.classList.add(openCls);
      btn.classList.add(openCls);
      d.body.addEventListener('click', bodyClickHandler);
      isOpened = true;
    };

    var close = function close() {
      if (dev) {
        console.log('f: close');
      }

      cnt.classList.remove(openCls);
      btn.classList.remove(openCls);
      d.body.removeEventListener('click', bodyClickHandler);
      isOpened = false;
    };

    var buttonClickHandler = function buttonClickHandler(e) {
      if (isOpened) {
        close();
      } else {
        open();
      }

      e.preventDefault();
    };

    btn.addEventListener('click', buttonClickHandler);
  };

  try {
    sort();
    date();
  } catch (e) {
    console.log(e);
  }
})(document);
//# sourceMappingURL=../maps/main.js.map
