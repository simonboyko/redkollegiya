
1. index.html - добавился враппер `.frontTabsLayout__navInner`.
==============================================

1. about.html - добавилось больше соцсетей, это для проверки верстки. Никак реагировать не надо.
2. event.html - Добавил фото в галлерею, но у тебя точно так же
==================

1. interview.html - внутри <h1 class="basePage__pageTitle block__pageTitle"> убрал ссылку.
2. event.html - добавился блок с галереей <div class="gallery">

========================

1. interview.html - внутри <h1 class="basePage__pageTitle block__pageTitle"> добавилась ссылка.

====================================================

1. Должны были измениться все файлы разметки, потому что сделал переносы
2. В файле applicants-month.html ушли месяцы (это у меня они по ошибке появились, тебе ничего менять не надо).
3. На страницах applicants-month.html и applicants-year.html для элемента .applicants__card добавился атрибут data-body. Это нужно для всплывающего превью.
4. applicants-month.html и applicants-year.html для .applicants__card добавил target="_blank", у тебя он есть, ничего менять не надо.
5. interview.html - добавились элементы .basePage__readmore и .basePage__inlinedate внутри .basePage__type, больше ничего не менялось.
